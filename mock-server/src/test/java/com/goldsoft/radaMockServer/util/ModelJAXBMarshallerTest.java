package com.goldsoft.radaMockServer.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.goldsoft.radaOpenDataApi.model.chronology.ChronologyInformation;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.*;

public class ModelJAXBMarshallerTest {
    @Test
    public void marshalChronologyInformationToJson() throws JsonProcessingException {
        ChronologyInformation chronologyInformation = new ChronologyInformation();
        chronologyInformation.setPublicationDate(new Date());
        chronologyInformation.setCategory("super category");
        ObjectMapper objectMapper = new ObjectMapper();
        System.out.println(objectMapper.writeValueAsString(createDefaultChronologyInformation()));
//        System.out.println(modelJAXBMarshaller.marshalChronologyInformationToJson(chronologyInformation));
    }

    private ChronologyInformation createDefaultChronologyInformation() {
        ChronologyInformation chronologyInformation = new ChronologyInformation();
        chronologyInformation.setCategory("plenary");
        chronologyInformation.setCreator("Відділ спеціалізованих інформаційно-технічних комплексів Управління комп'ютеризованих систем Апарату Верховної Ради України");
        chronologyInformation.setId("chron_ppz-skl9");
        chronologyInformation.setManager("Миронченко Андрій Сергійович");
        chronologyInformation.setManagerPhone("(044)255-42-50");
        chronologyInformation.setPublisher("Апарат Верховної Ради України");
        chronologyInformation.setWebMaster("lawup@rada.gov.ua");

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        try {
            chronologyInformation.setCreationDate(simpleDateFormat.parse("2019-08-19T10:45:00"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        chronologyInformation.setPublicationDate(new Date());

        return chronologyInformation;
    }
}
