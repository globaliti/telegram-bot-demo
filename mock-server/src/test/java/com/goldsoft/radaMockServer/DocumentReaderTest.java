package com.goldsoft.radaMockServer;

import com.goldsoft.radaMockServer.util.DocumentReader;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

public class DocumentReaderTest {
    @Test
    public void testReader() throws IOException {
        DocumentReader documentReader = new DocumentReader();
        File fileToRead = documentReader.getFileFromResources("chronologyWithUpdates","state2.xml");
        String documentText = documentReader.getFileDataAsText(fileToRead);

        System.out.println(documentText);

        Assert.assertNotNull(documentText);
        Assert.assertTrue(documentText.length() > 0);
    }
}