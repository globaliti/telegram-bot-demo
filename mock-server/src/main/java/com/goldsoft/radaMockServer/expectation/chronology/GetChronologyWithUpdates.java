package com.goldsoft.radaMockServer.expectation.chronology;

import com.goldsoft.radaMockServer.util.DocumentReader;
import org.mockserver.mock.action.ExpectationResponseCallback;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;
import org.mockserver.model.HttpStatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.mockserver.model.Header.header;
import static org.mockserver.model.HttpResponse.response;

@SuppressWarnings({"SpringJavaConstructorAutowiringInspection", "SpringJavaInjectionPointsAutowiringInspection"})
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class GetChronologyWithUpdates implements ExpectationResponseCallback {

    private final String CHRONOLOGY_DIRECTORY_PATH = "chronologyWithUpdates";

    private final String STATE_1_FILE_NAME = "state1.xml";

    private final String STATE_2_FILE_NAME = "state2.xml";

    private final String STATE_3_FILE_NAME = "state3.xml";

    private final String STATE_4_FILE_NAME = "state4.xml";

    @Autowired
    private DocumentReader documentReader;

    private int requestCounter = 0;

    private String beginDocumentStructure = "<?xml version=\"1.0\" encoding=\"windows-1251\"?>\n" +
            "<root>\n" +
            "    <date_agenda>\n" +
            "        <day_agenda>15</day_agenda>\n" +
            "        <month_agenda>10</month_agenda>\n" +
            "        <year_agenda>2019</year_agenda>\n" +
            "    </date_agenda>";

    private String endDocumentStructure = "</root>";

    private final int numberRequestsForStateUpdates;

    public GetChronologyWithUpdates(int numberRequestsForStateUpdates) {
        this.numberRequestsForStateUpdates = numberRequestsForStateUpdates;
    }

    @Override
    public HttpResponse handle(HttpRequest httpRequest) {

        if (requestCounter < makeMaxResponseAmountForStep(1)) {
            return responseForFirstRequest(httpRequest);
        } else if (requestCounter < makeMaxResponseAmountForStep(2)) {
            return responseForSecondRequest(httpRequest);
        } else if (requestCounter < makeMaxResponseAmountForStep(3)) {
            return responseForThirdRequest(httpRequest);
        } else {
            return responseForFourthRequest(httpRequest);
        }
    }

    private int makeMaxResponseAmountForStep(int step) {
        int maxResponseCount = 0;
        for (int i = 0; i < step; i++) {
            maxResponseCount += this.numberRequestsForStateUpdates;
        }
        return maxResponseCount;
    }

    private String getDocumentBodyForState(int state) {
        File chronologyDataFile = null;
        String documentBodyFirstState = null;
        if (state == 1) {
            chronologyDataFile = documentReader.getFileFromResources(
                    CHRONOLOGY_DIRECTORY_PATH, STATE_1_FILE_NAME);
        }

        if (state == 2) {
            chronologyDataFile = documentReader.getFileFromResources(
                    CHRONOLOGY_DIRECTORY_PATH, STATE_2_FILE_NAME);
        }

        if (state == 3) {
            chronologyDataFile = documentReader.getFileFromResources(
                    CHRONOLOGY_DIRECTORY_PATH, STATE_3_FILE_NAME);
        }

        if (state == 4) {
            chronologyDataFile = documentReader.getFileFromResources(
                    CHRONOLOGY_DIRECTORY_PATH, STATE_4_FILE_NAME);
        }

        if (chronologyDataFile != null && chronologyDataFile.exists()) {
            try {
                documentBodyFirstState = documentReader.getFileDataAsText(chronologyDataFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return documentBodyFirstState;
    }

    private HttpResponse responseForFirstRequest(HttpRequest httpRequest) {
        String body = beginDocumentStructure + getDocumentBodyForState(1) + endDocumentStructure;
        requestCounter++;
        return response()
                .withStatusCode(HttpStatusCode.OK_200.code())
                .withHeaders(
                        header("Content-Length", body.getBytes(Charset.forName("windows-1251")).length),
                        header("Content-type", "application/xml; charset=windows-1251")
                )
                .withBody(body);
    }

    private HttpResponse responseForSecondRequest(HttpRequest httpRequest) {
        String body = beginDocumentStructure + getDocumentBodyForState(1) + getDocumentBodyForState(2) + endDocumentStructure;
        requestCounter++;
        return response()
                .withStatusCode(HttpStatusCode.OK_200.code())
                .withHeaders(
                        header("Content-Length", body.getBytes(Charset.forName("windows-1251")).length),
                        header("Content-type", "application/xml; charset=windows-1251")
                )
                .withBody(body);
    }

    private HttpResponse responseForThirdRequest(HttpRequest httpRequest) {
        String body = beginDocumentStructure +
                getDocumentBodyForState(1) +
                getDocumentBodyForState(2) +
                getDocumentBodyForState(3) +
                endDocumentStructure;

        requestCounter++;
        return response()
                .withStatusCode(HttpStatusCode.OK_200.code())
                .withHeaders(
//                        header("Content-Length", body.getBytes(UTF_8).length),
                        header("Content-type", "application/xml; charset=windows-1251")
                )
                .withBody(body);
    }

    private HttpResponse responseForFourthRequest(HttpRequest httpRequest) {
        String body = beginDocumentStructure +
                getDocumentBodyForState(1) +
                getDocumentBodyForState(2) +
                getDocumentBodyForState(3) +
                getDocumentBodyForState(4) +
                endDocumentStructure;
        requestCounter++;
        return response()
                .withStatusCode(HttpStatusCode.OK_200.code())
                .withHeaders(
//                        header("Content-Length", body.getBytes(UTF_8).length),
                        header("Content-type", "application/xml; charset=windows-1251")
                )
                .withBody(body);
    }
}