package com.goldsoft.radaMockServer.expectation.chronology;

import com.goldsoft.radaOpenDataApi.model.enums.Convene;
import com.goldsoft.radaOpenDataApi.util.ChronologyUriBinder;
import org.mockserver.integration.ClientAndServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static org.mockserver.model.HttpRequest.request;

@Component
public class ChronologyExpectationBinder {

    @Autowired
    ChronologyUriBinder chronologyUriBinder;

    @Autowired
    ChronologyExpectationProvider chronologyExpectationProvider;

    public void mockGetChronologyWithUpdates(
            ClientAndServer mockServer,
            Convene convene,
            String chronologyDate,
            int numberRequestsForStateUpdates) {//TODO must throw exception if something went wrong

        mockServer.when(
                request()
                        .withPath(chronologyUriBinder.getChronologyPath(convene, chronologyDate))
                        .withMethod("GET"))
                .respond(chronologyExpectationProvider.getChronologyWithUpdates(numberRequestsForStateUpdates));
    }

    public void mockGetChronologyDatesWithUpdates(
            ClientAndServer mockServer,
            Convene convene,
            String newDate,
            int numberRequestsForStateUpdates) {

        mockServer.when(
                request()
                        .withPath(chronologyUriBinder.getChronologyDatesPath(convene))
                        .withMethod("GET"))
                .respond(chronologyExpectationProvider.getChronologyDatesWithUpdate(newDate, numberRequestsForStateUpdates));
    }

    public void mockGetSectionInformationWithUpdates(
            ClientAndServer mockServer,
            Convene convene,
            int numberRequestsForStateUpdates) {

        mockServer.when(
                request()
                        .withPath(chronologyUriBinder.getSectionInformationJsonPath(convene))
                        .withMethod("GET"))
                .respond(chronologyExpectationProvider.getSectionInformationWIthUpdates(numberRequestsForStateUpdates));
    }
}