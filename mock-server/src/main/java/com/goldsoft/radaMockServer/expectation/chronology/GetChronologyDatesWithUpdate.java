package com.goldsoft.radaMockServer.expectation.chronology;

import org.mockserver.mock.action.ExpectationResponseCallback;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;
import org.mockserver.model.HttpStatusCode;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import static org.mockserver.model.HttpResponse.response;

@SuppressWarnings("SpringJavaConstructorAutowiringInspection")
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class GetChronologyDatesWithUpdate implements ExpectationResponseCallback {

    private String sourceDates = "29.08.2019\n" +
            "30.08.2019\n" +
            "03.09.2019\n" +
            "10.09.2019\n" +
            "11.09.2019\n" +
            "12.09.2019\n" +
            "13.09.2019\n" +
            "18.09.2019\n" +
            "19.09.2019\n" +
            "20.09.2019\n" +
            "02.10.2019\n" +
            "03.10.2019\n" +
            "04.10.2019\n" +
            "15.10.2019\n" +
            "16.10.2019\n" +
            "17.10.2019\n" +
            "18.10.2019\n" +
            "29.10.2019\n" +
            "30.10.2019\n" +
            "31.10.2019\n" +
            "01.11.2019\n" +
            "12.11.2019\n" +
            "13.11.2019\n" +
            "14.11.2019\n" +
            "15.11.2019\n";

    private final String newDate;

    private final int numberRequestsForStateUpdates;

    private int countRequests;

    private String getValidChronologyData(String data){
        if(!data.endsWith("\n")){
            data+="\n";
        }
        return data;
    }
    public GetChronologyDatesWithUpdate(String newDate, int numberRequestsForStateUpdates) {
        this.newDate = getValidChronologyData(newDate);
        this.numberRequestsForStateUpdates = numberRequestsForStateUpdates;
    }

    public GetChronologyDatesWithUpdate(String sourceDates, String newDate, int numberRequestsForStateUpdates) {
        this.sourceDates = sourceDates;
        this.newDate = getValidChronologyData(newDate);
        this.numberRequestsForStateUpdates = numberRequestsForStateUpdates;
    }

    @Override
    public HttpResponse handle(HttpRequest httpRequest) {
            return makeResponse();
    }

    private HttpResponse makeResponse() {
        if (countRequests < numberRequestsForStateUpdates) {
            countRequests++;
            return response()
                    .withStatusCode(HttpStatusCode.OK_200.code())
                    .withHeader("Content-type", "text/plain")
                    .withBody(sourceDates);
        } else {
            return response()
                    .withStatusCode(HttpStatusCode.OK_200.code())
                    .withHeader("Content-type", "text/plain")
                    .withBody(sourceDates + newDate);
        }
    }

    public String getSourceDates() {
        return sourceDates;
    }

    public void setSourceDates(String sourceDates) {
        this.sourceDates = sourceDates;
    }

    public String getNewDate() {
        return newDate;
    }

    public int getNumberRequestsForStateUpdates() {
        return numberRequestsForStateUpdates;
    }

    public int getCountRequests() {
        return countRequests;
    }

    public void setCountRequests(int countRequests) {
        this.countRequests = countRequests;
    }
}