package com.goldsoft.radaMockServer.expectation.chronology;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.goldsoft.radaOpenDataApi.model.chronology.ChronologyInformation;
import org.mockserver.mock.action.ExpectationResponseCallback;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;
import org.mockserver.model.HttpStatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.mockserver.model.Header.header;
import static org.mockserver.model.HttpResponse.response;

@SuppressWarnings({"SpringJavaConstructorAutowiringInspection", "SpringJavaInjectionPointsAutowiringInspection"})
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class GetSectionInformationWithUpdates implements ExpectationResponseCallback {

    private final int numberRequestsForStateUpdates;

    private int requestCounter;

    private final ChronologyInformation chronologyInformation;

    private boolean hadFirstUpdate = false;
    private boolean hadSecondUpdate = false;
    private boolean hadThirdUpdate = false;
    private boolean hadFourthUpdate = false;

    @Autowired
    private ObjectMapper objectMapper;

    public GetSectionInformationWithUpdates(int numberRequestsForStateUpdates) {
        this.numberRequestsForStateUpdates = numberRequestsForStateUpdates;
        chronologyInformation = createDefaultChronologyInformation();
    }

    @Override
    public HttpResponse handle(HttpRequest httpRequest) {

        if (requestCounter < makeMaxResponseAmountForStep(1)) {
            return responseForFirstRequest(httpRequest);
        } else if (requestCounter < makeMaxResponseAmountForStep(2)) {
            return responseForSecondRequest(httpRequest);
        } else if (requestCounter < makeMaxResponseAmountForStep(3)) {
            return responseForThirdRequest(httpRequest);
        } else {
            return responseForFourthRequest(httpRequest);
        }
    }

    private HttpResponse makeResponse(HttpRequest httpRequest) {
        requestCounter++;
        String responseJson = null;
        try {
            responseJson = objectMapper.writeValueAsString(chronologyInformation);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return response()
                .withStatusCode(HttpStatusCode.OK_200.code())
                .withHeaders(
                        header("Content-Length", responseJson.getBytes(UTF_8).length),
                        header("Content-type", "application/json; charset=utf-8")
                )
                .withBody(responseJson);
    }

    private HttpResponse responseForFourthRequest(HttpRequest httpRequest) {
        if (!hadFourthUpdate) {
            updateChronologyInformation(chronologyInformation);
            hadFourthUpdate = true;
        }
        return makeResponse(httpRequest);
    }

    private HttpResponse responseForThirdRequest(HttpRequest httpRequest) {
        if (!hadThirdUpdate) {
            updateChronologyInformation(chronologyInformation);
            hadThirdUpdate = true;
        }
        return makeResponse(httpRequest);
    }

    private HttpResponse responseForSecondRequest(HttpRequest httpRequest) {
        if (!hadSecondUpdate) {
            updateChronologyInformation(chronologyInformation);
            hadSecondUpdate = true;
        }
        return makeResponse(httpRequest);
    }

    private HttpResponse responseForFirstRequest(HttpRequest httpRequest) {
        if (!hadFirstUpdate) {
            updateChronologyInformation(chronologyInformation);
            hadFirstUpdate = true;
        }
        return makeResponse(httpRequest);
    }

    private int makeMaxResponseAmountForStep(int step) {
        int maxResponseCount = 0;
        for (int i = 0; i < step; i++) {
            maxResponseCount += this.numberRequestsForStateUpdates;
        }
        return maxResponseCount;
    }

    private ChronologyInformation createDefaultChronologyInformation() {
        ChronologyInformation chronologyInformation = new ChronologyInformation();
        chronologyInformation.setCategory("plenary");
        chronologyInformation.setCreator("Відділ спеціалізованих інформаційно-технічних комплексів Управління комп'ютеризованих систем Апарату Верховної Ради України");
        chronologyInformation.setId("chron_ppz-skl9");
        chronologyInformation.setManager("Миронченко Андрій Сергійович");
        chronologyInformation.setManagerPhone("(044)255-42-50");
        chronologyInformation.setPublisher("Апарат Верховної Ради України");
        chronologyInformation.setWebMaster("lawup@rada.gov.ua");
        chronologyInformation.setCreationDate(convertDate("2019-08-19T10:45:00"));
        chronologyInformation.setPublicationDate(new Date());

        return chronologyInformation;
    }

    private Date convertDate(String publicationDate) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date convertedPublicationDate = null;
        try {
            convertedPublicationDate = simpleDateFormat.parse(publicationDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return convertedPublicationDate;
    }

    private void updateChronologyInformation(ChronologyInformation chronologyInformation) {
        chronologyInformation.setPublicationDate(new Date());
    }
}