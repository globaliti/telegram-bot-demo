package com.goldsoft.radaMockServer.expectation.chronology;

import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.stereotype.Component;

@Component
public abstract class ChronologyExpectationProvider {
    @Lookup
    public abstract GetChronologyDatesWithUpdate getChronologyDatesWithUpdate(
            String newDate,
            int numberRequestsForStateUpdates);

    @Lookup
    public abstract GetChronologyWithUpdates getChronologyWithUpdates(int numberRequestsForStateUpdates);

    @Lookup
    public abstract GetSectionInformationWithUpdates getSectionInformationWIthUpdates(int numberRequestsForStateUpdates);
}