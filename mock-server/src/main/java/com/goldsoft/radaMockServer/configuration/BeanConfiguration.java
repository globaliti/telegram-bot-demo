package com.goldsoft.radaMockServer.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.mockserver.integration.ClientAndServer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;

import static org.mockserver.integration.ClientAndServer.startClientAndServer;

@Configuration
@PropertySource("classpath:mock-server.properties")
public class BeanConfiguration {

    @Value("${mock-server.port:7575}")
    private int mockServerPort;

    @Bean
    ClientAndServer clientAndServer() {
        return startClientAndServer(mockServerPort);
    }

    @Bean
    RestTemplate restTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("windows-1251")));
        return restTemplate;
    }

    @Bean
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    ObjectMapper objectMapper() {
        return new ObjectMapper();
    }
}
