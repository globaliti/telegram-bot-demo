package com.goldsoft.radaMockServer.bootstrap;

import com.goldsoft.radaMockServer.expectation.chronology.ChronologyExpectationBinder;
import com.goldsoft.radaOpenDataApi.model.enums.Convene;
import org.mockserver.integration.ClientAndServer;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
public class Bootstrap implements InitializingBean {

    @Autowired
    ClientAndServer mockServer;

    @Autowired
    ChronologyExpectationBinder chronologyExpectationBinder;

    @Override
    public void afterPropertiesSet() throws Exception {
        defineMockServerScenarios();
    }

    private void defineMockServerScenarios() {
        chronologyExpectationBinder.mockGetChronologyDatesWithUpdates(
                mockServer,
                Convene.NINTH,
                "17.11.2019",
                2);
        chronologyExpectationBinder.mockGetChronologyWithUpdates(
                mockServer,
                Convene.NINTH,
                "17.11.2019",
                1);
        chronologyExpectationBinder.mockGetSectionInformationWithUpdates(
                mockServer,
                Convene.NINTH,
                2);
    }
}
