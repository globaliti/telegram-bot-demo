package com.goldsoft.radaMockServer;

import com.goldsoft.radaMockServer.expectation.chronology.ChronologyExpectationBinder;
import com.goldsoft.radaOpenDataApi.model.enums.Convene;
import org.mockserver.integration.ClientAndServer;

import static org.mockserver.integration.ClientAndServer.startClientAndServer;

public class Main {

    public static void main(String[] args) {
        ClientAndServer mockServer = startClientAndServer(7070);
        ChronologyExpectationBinder chronologyExpectationBinder = new ChronologyExpectationBinder();

        chronologyExpectationBinder.mockGetChronologyDatesWithUpdates(
                mockServer,
                Convene.NINTH,
                "17.11.2019",
                2);
        chronologyExpectationBinder.mockGetChronologyWithUpdates(
                mockServer,
                Convene.NINTH,
                "17.11.2019",
                2);
    }
}