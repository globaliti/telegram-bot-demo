#UA Rada Information Bot
This bot providing next functionality implemented for Telegram bot:
* Subscription to updating of Factions, MPs, and Chronology. It's also available to telegram channels.
* Get history of voting.
* Get detailed information about certain voting by MP or Faction
* Get of a bill's document submitted for voting question of chronology.
* Get of a bill's document by number.
