package com.goldsoft.radainfobot.service.subscribe;

import com.goldsoft.radainfobot.domain.Subscriber;
import com.goldsoft.radainfobot.domain.enums.SubscribeType;
import com.goldsoft.radainfobot.service.BaseCrudService;

public interface SubscriberService extends BaseCrudService<Subscriber, Long> {
    Subscriber findByChanelName(String chatId);

    @Deprecated
    public Subscriber save(Subscriber entity);
}
