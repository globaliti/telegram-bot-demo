package com.goldsoft.radainfobot.service;

import java.io.Serializable;
import java.util.List;

public interface BaseCrudService<T, PK extends Serializable>  {
    public T save(T entity);

    public void delete(T entity);

    public T findById(PK primaryKey);

    public List<T> findAll();

    public boolean existsById(PK primaryKey);
}
