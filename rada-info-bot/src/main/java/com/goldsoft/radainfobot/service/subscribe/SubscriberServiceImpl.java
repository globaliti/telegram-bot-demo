package com.goldsoft.radainfobot.service.subscribe;

import com.goldsoft.radainfobot.domain.Subscriber;
import com.goldsoft.radainfobot.repository.SubscriberRepository;
import com.goldsoft.radainfobot.service.BaseCrudServiceImpl;
import com.goldsoft.radainfobot.service.subscription.Subscribers;
import com.goldsoft.radainfobot.service.telegram.TelegramChatService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public class SubscriberServiceImpl extends BaseCrudServiceImpl<Subscriber, Long> implements SubscriberService {

    private Subscribers subscribers;

    private final SubscriberRepository subscriberRepository;

    private final TelegramChatService telegramChatService;

    public SubscriberServiceImpl(SubscriberRepository subscriberRepository, TelegramChatService telegramChatService) {
        this.subscriberRepository = subscriberRepository;
        this.telegramChatService = telegramChatService;
    }

    @Override
    public JpaRepository<Subscriber, Long> getRepository() {
        return this.subscriberRepository;
    }

    @Deprecated //TODO delete this
    @Override
    public Subscriber save(Subscriber entity) {

        if (entity.getChatType() == null) {
            entity.setChatType(telegramChatService.getChatType(entity.getChatId()));
        }

//        if (entity.getChatType() == ChatType.CHANEL_CHAT && !isHaveAccessToChanel(entity.getChatId())) {
//            return null;
//        } //TODO move to subscription service

        return super.save(entity);
    }

    @Override
    public Subscriber findByChanelName(String chatId) {
        return subscriberRepository.findByChatId(chatId);
    }
}