package com.goldsoft.radainfobot.service;

import com.goldsoft.radaOpenDataApi.model.chronology.EventQuestion;
import com.goldsoft.radaOpenDataApi.model.chronology.Question;
import com.goldsoft.radainfobot.domain.Vote;
import com.goldsoft.radainfobot.domain.VoteHistory;
import com.goldsoft.radainfobot.domain.VoteQuestion;
//import com.google.gson.internal.$Gson$Preconditions;

import java.util.LinkedList;
import java.util.List;

public interface TemplateMessageEngine {

    String makeMessage(Vote vote);
    String makeMessage(EventQuestion eventQuestion);

    List<String> makeMessagesByVotes(List<Vote> votes);
    List<String> makeMessagesByChronologyEventQuestions(List<EventQuestion> eventQuestions);
    List<String> makeMessagesByChronologyQuestions(List<Question> eventQuestions);

    String newVotesMessage(List<VoteQuestion> voteQuestions);
    String makeMessageForAboutNewVotes(List<Vote> votes);
    @Deprecated
    String makeMessageAboutVotes(List<Vote> votes);
    List<String> makeMessagesAboutVotes(List<Vote> votes);
   // String makeMessageAboutVotes(VoteHistory voteHistory);
}