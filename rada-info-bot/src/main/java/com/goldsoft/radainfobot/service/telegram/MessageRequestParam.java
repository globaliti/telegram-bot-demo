package com.goldsoft.radainfobot.service.telegram;

import java.util.Objects;

public class MessageRequestParam {

    private String chatId;

    private String text;

    private String parseMode;

    private boolean disableWebPagePreview;

    private boolean disableNotification;

    private int replyToMessageId;

    public MessageRequestParam() {
    }

    public MessageRequestParam(String chatId, String text) {
        this.chatId = chatId;
        this.text = text;
    }

    public MessageRequestParam(String chatId, String text, String parseMode, boolean disableWebPagePreview, boolean disableNotification) {
        this.chatId = chatId;
        this.text = text;
        this.parseMode = parseMode;
        this.disableWebPagePreview = disableWebPagePreview;
        this.disableNotification = disableNotification;
    }

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getParseMode() {
        return parseMode;
    }

    public void setParseMode(String parseMode) {
        this.parseMode = parseMode;
    }

    public boolean isDisableWebPagePreview() {
        return disableWebPagePreview;
    }

    public void setDisableWebPagePreview(boolean disableWebPagePreview) {
        this.disableWebPagePreview = disableWebPagePreview;
    }

    public boolean isDisableNotification() {
        return disableNotification;
    }

    public void setDisableNotification(boolean disableNotification) {
        this.disableNotification = disableNotification;
    }

    public int getReplyToMessageId() {
        return replyToMessageId;
    }

    public void setReplyToMessageId(int replyToMessageId) {
        this.replyToMessageId = replyToMessageId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MessageRequestParam that = (MessageRequestParam) o;
        return disableWebPagePreview == that.disableWebPagePreview &&
                disableNotification == that.disableNotification &&
                replyToMessageId == that.replyToMessageId &&
                Objects.equals(chatId, that.chatId) &&
                Objects.equals(text, that.text) &&
                Objects.equals(parseMode, that.parseMode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(chatId, text, parseMode, disableWebPagePreview, disableNotification, replyToMessageId);
    }

    @Override
    public String toString() {
        return "MessageRequestParam{" +
                "chatId='" + chatId + '\'' +
                ", text='" + text + '\'' +
                ", parseMode='" + parseMode + '\'' +
                ", disableWebPagePreview=" + disableWebPagePreview +
                ", disableNotification=" + disableNotification +
                ", replyToMessageId=" + replyToMessageId +
                '}';
    }
}
