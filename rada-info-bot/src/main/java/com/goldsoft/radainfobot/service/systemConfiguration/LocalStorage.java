package com.goldsoft.radainfobot.service.systemConfiguration;

import com.goldsoft.radainfobot.domain.SystemConfiguration;

class LocalStorage {
    private static LocalStorage instance;
    private SystemConfiguration systemConfiguration;

    public static synchronized LocalStorage getInstance() {
        if (instance == null) {
            instance = new LocalStorage();
        }
        return instance;
    }

    private LocalStorage() {
    }

    public synchronized void setSystemConfiguration(SystemConfiguration systemConfiguration) {
        this.systemConfiguration = systemConfiguration;
    }

    public synchronized SystemConfiguration getSystemConfiguration() {
        return systemConfiguration;
    }

}
