package com.goldsoft.radainfobot.service.voteHistory;

import com.goldsoft.radaOpenDataApi.model.chronology.Chronology;
import com.goldsoft.radaOpenDataApi.model.chronology.Question;
import com.goldsoft.radaOpenDataApi.model.enums.Convene;
import com.goldsoft.radainfobot.domain.Vote;
import com.goldsoft.radainfobot.domain.VoteHistory;
import com.goldsoft.radainfobot.domain.VoteQuestion;
import com.goldsoft.radainfobot.service.BaseCrudService;
import org.springframework.lang.Nullable;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public interface VoteHistoryService extends BaseCrudService<VoteHistory, Long> {
    void subscribeToUpdates(String chanel);

    void runScheduleForUpdates(Long milliseconds);

    VoteHistory findByDate(LocalDate date);

    void saveVoteQuestionToVoteHistory(VoteHistory voteHistory, List<Question> questions);

    VoteHistory saveQuestionsToVoteHistory(LocalDate voteHistoryChronologyDate, List<Question> questions);

    VoteHistory saveVoteQuestionsToVoteHistory(LocalDate voteHistoryChronologyDate, List<VoteQuestion> voteQuestions);

    @Nullable
    LocalDateTime getLastVoteDateInVoteHistory(LocalDate voteHistoryDate);

    List<LocalDate> findAllDates();

    List<String> findAllDatesAndGetAsRadaStringFormat();

    List<Vote> extractVotesFromVoteHistory(VoteHistory voteHistory);

    @Nullable
    List<Vote> findVotesFromVoteHistory(LocalDate voteHistoryDate);

    void uploadChronologyForNinthConvene();

    VoteHistory saveChronologyAsVoteHistory(Chronology chronology);

    List<VoteQuestion> getNewVoteQuestionsFromChronology(LocalDate chronologyDate, Convene convene);
}