package com.goldsoft.radainfobot.service.voteQuestion;

import com.goldsoft.radainfobot.domain.Vote;
import com.goldsoft.radainfobot.domain.VoteQuestion;
import com.goldsoft.radainfobot.service.BaseCrudService;

import java.util.List;

public interface VoteQuestionService extends BaseCrudService<VoteQuestion, Long> {
    List<Vote> findAllVotesInVoteQuestions(List<VoteQuestion> voteQuestions);
}
