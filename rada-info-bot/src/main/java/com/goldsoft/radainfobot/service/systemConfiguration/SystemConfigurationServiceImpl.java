package com.goldsoft.radainfobot.service.systemConfiguration;

import com.goldsoft.radainfobot.domain.SystemConfiguration;
import com.goldsoft.radainfobot.repository.SystemConfigurationRepository;
import com.goldsoft.radainfobot.service.BaseCrudServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SystemConfigurationServiceImpl extends BaseCrudServiceImpl<SystemConfiguration, Long> implements SystemConfigurationService {

    @Autowired
    private SystemConfigurationRepository systemConfigurationRepository;

    private LocalStorage localStorage = LocalStorage.getInstance();

    @Override
    public JpaRepository<SystemConfiguration, Long> getRepository() {
        return systemConfigurationRepository;
    }

    @Override
    public SystemConfiguration getConfiguration() {
        if (localStorage.getSystemConfiguration() != null) {
            return localStorage.getSystemConfiguration();
        }

        List<SystemConfiguration> systemConfigurations = systemConfigurationRepository.findAll();
        if (systemConfigurations.size() > 1) {
            throw new RuntimeException("This entity should be only one");
        }
        if (systemConfigurations.isEmpty()) {
            return null;
        }
        return systemConfigurations.get(0);
    }

    @Override
    public SystemConfiguration save(SystemConfiguration entity) {
        SystemConfiguration savedConfiguration = super.save(entity);
        localStorage.setSystemConfiguration(savedConfiguration);
        return savedConfiguration;
    }

    @Override
    public void delete(SystemConfiguration entity) {
        localStorage.setSystemConfiguration(null);
        super.delete(entity);
    }
}
