package com.goldsoft.radainfobot.service;

import java.util.LinkedList;
import java.util.List;

public class ListCutter<T> {

    public List<List<T>> cut(List<T> list, int sizeLimit) {
        List<List<T>> parts = new LinkedList<>();
        int pathCount = calculatePartsQuantity(list.size(), sizeLimit);

        for (int i = 0, firstIndex = 0, lastIndex = sizeLimit; i < pathCount; i++) {
            if (list.size() - (lastIndex) > 0) {
                parts.add(list.subList(firstIndex, lastIndex));
                firstIndex += sizeLimit;
                lastIndex += sizeLimit;
            } else {
                parts.add(list.subList(firstIndex, list.size()));
            }
        }
        return parts;
    }

    protected int calculatePartsQuantity(int listSize, int partSize) {
        return (int) Math.ceil((double) listSize / partSize);
    }
}