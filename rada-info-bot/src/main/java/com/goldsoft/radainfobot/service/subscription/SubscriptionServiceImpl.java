package com.goldsoft.radainfobot.service.subscription;

import com.goldsoft.radainfobot.domain.Subscriber;
import com.goldsoft.radainfobot.domain.enums.ChatType;
import com.goldsoft.radainfobot.domain.enums.SubscribeType;
import com.goldsoft.radainfobot.exception.SubscriptionException;
import com.goldsoft.radainfobot.service.subscribe.SubscriberService;
import com.goldsoft.radainfobot.service.telegram.TelegramChatService;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

@Service
public class SubscriptionServiceImpl implements SubscriptionService {

    private Subscribers subscribers;

    private final TelegramChatService telegramChatService;

    private final SubscriberService subscriberService;

    public SubscriptionServiceImpl(TelegramChatService telegramChatService, SubscriberService subscriberService) {
        this.telegramChatService = telegramChatService;
        this.subscriberService = subscriberService;
    }

    @Override
    public void addSubscriberToPool(Subscriber subscriber) {
        for (SubscribeType subscribeType : subscriber.getSubscribeTypes()) {
            switch (subscribeType) {
                case MP_CHANGING: {
                    Subscribers.MP_CHANGING.addSubscriber(subscriber.getChatId());
                    break;
                }
                case FACTION_CHANGING: {
                    Subscribers.FACTION_CHANGING.addSubscriber(subscriber.getChatId());
                    break;
                }
                case VOTING_HISTORY_CHANGING: {
                    Subscribers.VOTING_HISTORY_CHANGING.addSubscriber(subscriber.getChatId());
                    break;
                }
            }
        }
    }

    @Override
    public void removeSubscriberFromPool(Subscriber subscriber) {
        for (SubscribeType subscribeType : subscriber.getSubscribeTypes()) {
            switch (subscribeType) {
                case MP_CHANGING: {
                    Subscribers.MP_CHANGING.removeSubscriber(subscriber.getChatId());
                    break;
                }
                case FACTION_CHANGING: {
                    Subscribers.FACTION_CHANGING.removeSubscriber(subscriber.getChatId());
                    break;
                }
                case VOTING_HISTORY_CHANGING: {
                    Subscribers.VOTING_HISTORY_CHANGING.removeSubscriber(subscriber.getChatId());
                    break;
                }
            }
        }
    }

    @Override
    public void runFollowing() {
    //TODO run ViteHisThread here
    }

    @Override
    public void stopFollowing() {

    }


    private boolean isHaveAccessToChanel(String chatId) {
        return true; //TODO implement checkAccess
    }

    @Override
    public void addSubscriber(String chatId, SubscribeType[] subscribeTypes) throws SubscriptionException {//TODO abstraction confuse
        Subscriber subscriber = subscriberService.findByChanelName(chatId);
        if (subscriber == null) {
            subscriber = new Subscriber();
            subscriber.setChatId(chatId);

            ChatType chatType = telegramChatService.getChatType(chatId);
            if (chatType == ChatType.CHANNEL && !isHaveAccessToChanel(chatId)) {
                throw new SubscriptionException("Problem with access to " + chatId + " chanel");
            }
            subscriber.setChatType(chatType);
        }

        Set<SubscribeType> subscriberSubscribeTypes = subscriber.getSubscribeTypes();
        if (subscriberSubscribeTypes == null) {
            subscriberSubscribeTypes = new TreeSet<>();
        }
        Collections.addAll(subscriberSubscribeTypes, subscribeTypes);

        subscriber.setSubscribeTypes(subscriberSubscribeTypes);

        Subscriber savedSubscriber = subscriberService.save(subscriber);
        addSubscriberToPool(savedSubscriber);
    }
}