package com.goldsoft.radainfobot.service.subscription;

import com.goldsoft.radainfobot.domain.Subscriber;
import com.goldsoft.radainfobot.domain.enums.SubscribeType;
import com.goldsoft.radainfobot.exception.SubscriptionException;

public interface SubscriptionService {
    void addSubscriberToPool(Subscriber subscriber);

    void addSubscriber(String chatId, SubscribeType[] subscribeTypes) throws SubscriptionException;

    void removeSubscriberFromPool(Subscriber subscriber);

    void runFollowing();

    void stopFollowing();
}
