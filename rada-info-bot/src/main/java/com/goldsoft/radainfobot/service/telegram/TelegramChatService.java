package com.goldsoft.radainfobot.service.telegram;

import com.goldsoft.radainfobot.domain.enums.ChatType;

public interface TelegramChatService {

    void sendMessage(MessageRequestParam messageRequestParam);

    ChatType getChatType(String chatId);

    boolean hasChannelAdministrationRights(String channelId, String userId);
}
