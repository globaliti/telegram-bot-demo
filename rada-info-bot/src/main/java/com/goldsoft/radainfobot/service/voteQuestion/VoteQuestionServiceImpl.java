package com.goldsoft.radainfobot.service.voteQuestion;

import com.goldsoft.radainfobot.domain.Vote;
import com.goldsoft.radainfobot.domain.VoteQuestion;
import com.goldsoft.radainfobot.repository.VoteQuestionRepository;
import com.goldsoft.radainfobot.service.BaseCrudServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
public class VoteQuestionServiceImpl extends BaseCrudServiceImpl<VoteQuestion, Long> implements VoteQuestionService {

    @Autowired
    private VoteQuestionRepository voteQuestionRepository;

    @Override
    public JpaRepository<VoteQuestion, Long> getRepository() {
        return voteQuestionRepository;
    }

    @Override
    public List<Vote> findAllVotesInVoteQuestions(List<VoteQuestion> voteQuestions) {
        List<Vote> votes = new LinkedList<>();
        for (VoteQuestion voteQuestion : voteQuestions) {
            if (voteQuestion.getVotes() != null && !voteQuestion.getVotes().isEmpty()) {
                votes.addAll(voteQuestion.getVotes());
            }
        }
        return votes;
    }
}