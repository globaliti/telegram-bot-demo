package com.goldsoft.radainfobot.service.systemConfiguration;

import com.goldsoft.radainfobot.domain.SystemConfiguration;
import com.goldsoft.radainfobot.service.BaseCrudService;

public interface SystemConfigurationService extends BaseCrudService<SystemConfiguration, Long> {
    SystemConfiguration getConfiguration();
}
