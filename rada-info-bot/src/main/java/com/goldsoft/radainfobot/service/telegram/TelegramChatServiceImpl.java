package com.goldsoft.radainfobot.service.telegram;

import com.goldsoft.radainfobot.controller.TelegramBotController;
import com.goldsoft.radainfobot.domain.enums.ChatType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.objects.ChatMember;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.List;

@Service
public class TelegramChatServiceImpl implements TelegramChatService {

    @Autowired
    private TelegramBotController telegramBotController;

    @Override
    public void sendMessage(MessageRequestParam messageRequestParam) {
        try {
            telegramBotController.sendMessage(messageRequestParam);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }

    }

    @Override
    public ChatType getChatType(String chatId) {
        return telegramBotController.getChatType(chatId);
    }

    @Override
    public boolean hasChannelAdministrationRights(String channelId, String userId) {
        boolean userHasAdminPermissions = false;
        List<ChatMember> chatMembers = null;
        try {
            chatMembers = telegramBotController.getChatAdministrators(channelId);
        } catch (TelegramApiException e) {
            e.printStackTrace();
            return false;
        }
        for (ChatMember chatMember : chatMembers) {
            if (chatMember.getUser().getId().toString().equals(userId)
                    && (chatMember.getStatus().equalsIgnoreCase("administrator")
                    || chatMember.getStatus().equalsIgnoreCase("creator"))) {
                userHasAdminPermissions = true;
            }
        }
        return userHasAdminPermissions;
    }
}