package com.goldsoft.radainfobot.service;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

public abstract class BaseCrudServiceImpl<T,PK extends Serializable> implements BaseCrudService<T,PK> {
    public abstract JpaRepository<T,PK> getRepository();
    @Override
    @Transactional
    public T save(T entity) {
        return getRepository().save(entity);
    }

    @Override
    public void delete(T entity) {
    getRepository().delete(entity);
    }

    @Override
    public T findById(PK primaryKey) {
        Optional<T> optionalT = getRepository().findById(primaryKey);
        return optionalT.get();
    }

    @Override
    public List<T> findAll() {
        return getRepository().findAll();
    }

    @Override
    public boolean existsById(PK primaryKey) {
        return getRepository().existsById(primaryKey);
    }
}
