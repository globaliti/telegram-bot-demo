package com.goldsoft.radainfobot.service;

import com.goldsoft.radaOpenDataApi.model.chronology.EventQuestion;
import com.goldsoft.radaOpenDataApi.model.chronology.Question;
import com.goldsoft.radainfobot.domain.Vote;
import com.goldsoft.radainfobot.domain.VoteHistory;
import com.goldsoft.radainfobot.domain.VoteQuestion;
import com.goldsoft.radainfobot.mapper.ChronologyQuestionMapper;
import com.goldsoft.radainfobot.service.voteHistory.VoteHistoryService;
import com.goldsoft.radainfobot.service.voteQuestion.VoteQuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

@Service
public class TemplateMessageEngineImpl implements TemplateMessageEngine {

    private final String MESSAGE_TEMPLATE = "Голосование за :\n%s \nДата:%s\nЗА = %d||Против = %d";

    @Qualifier("messageTemplateEngine")
    @Autowired
    SpringTemplateEngine messageTemplateEngine;

    @Autowired
    VoteQuestionService voteQuestionService;

    @Autowired
    ChronologyQuestionMapper chronologyQuestionMapper;

    @Autowired
    VoteHistoryService voteHistoryService;

    @Override
    public String makeMessage(Vote vote) {
        return String.format(
                MESSAGE_TEMPLATE,
                vote.getName(),
                vote.getDate().toString(),
                vote.getVoteResult().getVotedFor(),
                vote.getVoteResult().getVotedAgainst());
    }

    @Override
    public String makeMessage(EventQuestion eventQuestion) {
        return String.format(
                MESSAGE_TEMPLATE,
                eventQuestion.getNameEvent(),
                eventQuestion.getDateEvent().toString(),
                eventQuestion.getResultEvent().getFor().intValue(),
                eventQuestion.getResultEvent().getAgainst().intValue());
    }

    @Override
    public List<String> makeMessagesByVotes(List<Vote> votes) {
        List<String> messages = new LinkedList<>();
        for (Vote vote : votes) {
            messages.add(makeMessage(vote));
        }
        return messages;
    }

    @Override
    public List<String> makeMessagesByChronologyEventQuestions(List<EventQuestion> eventQuestions) {
        List<String> messages = new LinkedList<>();
        for (EventQuestion eventQuestion : eventQuestions) {
            messages.add(makeMessage(eventQuestion));
        }
        return messages;
    }

    @Override
    public List<String> makeMessagesByChronologyQuestions(List<Question> questions) {
        List<String> messages = new LinkedList<>();
        for (Question question : questions) {
            for (EventQuestion eventQuestion : question.getEventQuestion()) {
                messages.add(makeMessage(eventQuestion));
            }
        }
        return messages;
    }

    @Override
    public String newVotesMessage(List<VoteQuestion> voteQuestions) {
        return makeMessageForAboutNewVotes(voteQuestionService.findAllVotesInVoteQuestions(voteQuestions));
    }

    @Override
    public String makeMessageForAboutNewVotes(List<Vote> votes) {
        Context context = new Context(Locale.getDefault());
        context.setVariable("votes", votes);
        return messageTemplateEngine.process("new-votes-message", context);
    }

    @Override
    public String makeMessageAboutVotes(List<Vote> votes) {
        Context context = new Context(Locale.getDefault());
        context.setVariable("votes", votes);
        return  messageTemplateEngine.process("votes-message", context);
    }

    @Override
    public List<String> makeMessagesAboutVotes(List<Vote> votes) {
        ListCutter<Vote> voteListCutter = new ListCutter<>();
        List<List<Vote>> parts = voteListCutter.cut(votes,10);
        List<String> messages = new LinkedList<>();
        for(List<Vote> part : parts){
            messages.add(makeMessageAboutVotes(part));
        }
        return messages;
    }
}