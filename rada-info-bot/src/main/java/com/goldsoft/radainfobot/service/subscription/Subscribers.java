package com.goldsoft.radainfobot.service.subscription;

import java.util.Set;
import java.util.TreeSet;

public enum Subscribers {
    VOTING_HISTORY_CHANGING, MP_CHANGING, FACTION_CHANGING;

    private Set<String> chatIdSubscribers = new TreeSet<>();

    public synchronized void addSubscriber(String chatId) {
        this.chatIdSubscribers.add(chatId);
    }

    public synchronized void removeSubscriber(String chatId) {
        this.chatIdSubscribers.remove(chatId);
    }

    public String[] getAllSubscribers() {
        return chatIdSubscribers.toArray(new String[0]);
    }

    private void TEST_clearVariables() {
        chatIdSubscribers = new TreeSet<>();
    }

    public static void TEST_clearVariablesInAllInstances() {
        if (Subscribers.MP_CHANGING.getAllSubscribers().length != 0) {
            Subscribers.MP_CHANGING.TEST_clearVariables();
        }
        if (Subscribers.FACTION_CHANGING.getAllSubscribers().length != 0) {
            Subscribers.FACTION_CHANGING.TEST_clearVariables();
        }
        if (Subscribers.VOTING_HISTORY_CHANGING.getAllSubscribers().length != 0) {
            Subscribers.VOTING_HISTORY_CHANGING.TEST_clearVariables();
        }
    }
}
