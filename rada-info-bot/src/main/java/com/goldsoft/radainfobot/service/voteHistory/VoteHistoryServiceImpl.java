package com.goldsoft.radainfobot.service.voteHistory;

import com.goldsoft.radaOpenDataApi.model.chronology.Chronology;
import com.goldsoft.radaOpenDataApi.model.chronology.EventQuestion;
import com.goldsoft.radaOpenDataApi.model.chronology.Question;
import com.goldsoft.radaOpenDataApi.model.enums.Convene;
import com.goldsoft.radaOpenDataApi.service.ChronologyService;
import com.goldsoft.radaOpenDataApi.util.TimeUtil;
import com.goldsoft.radainfobot.domain.Vote;
import com.goldsoft.radainfobot.domain.VoteHistory;
import com.goldsoft.radainfobot.domain.VoteQuestion;
import com.goldsoft.radainfobot.domain.builder.VoteHistoryBuilder;
import com.goldsoft.radainfobot.domain.enums.SubscribeType;
import com.goldsoft.radainfobot.mapper.ChronologyQuestionMapper;
import com.goldsoft.radainfobot.repository.VoteHistoryRepository;
import com.goldsoft.radainfobot.service.BaseCrudServiceImpl;
import com.goldsoft.radainfobot.service.subscription.SubscriptionService;
import com.goldsoft.radainfobot.service.subscribe.SubscriberService;
import com.goldsoft.radainfobot.service.subscription.Subscribers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@Service
public class VoteHistoryServiceImpl extends BaseCrudServiceImpl<VoteHistory, Long> implements VoteHistoryService {

    private static Subscribers subscribers = Subscribers.VOTING_HISTORY_CHANGING;

    private final Logger logger = LoggerFactory.getLogger(VoteHistoryServiceImpl.class);

    @Autowired
    private VoteHistoryRepository voteHistoryRepository;

    @Autowired
    SubscriberService subscriberService;

    @Autowired
    SubscriptionService subscriptionService;

    @Autowired
    ChronologyService chronologyService;

    @Autowired
    ChronologyQuestionMapper chronologyQuestionMapper;

    @Override
    public JpaRepository<VoteHistory, Long> getRepository() {
        return voteHistoryRepository;
    }

    @Override
    public void subscribeToUpdates(String chanel) {
        subscriptionService.addSubscriber(chanel, new SubscribeType[]{SubscribeType.VOTING_HISTORY_CHANGING});
    }

    @Override
    @Deprecated
    public void runScheduleForUpdates(Long milliseconds) { //TODO delete this

    }

    @Override
    public VoteHistory findByDate(LocalDate date) {
        return voteHistoryRepository.findByDate(date);
    }


    @Override
    public void saveVoteQuestionToVoteHistory(VoteHistory voteHistory, List<Question> questions) {
        //todo Validation vs because I need have date of chronology
        //TODO move logic from saveQuestionsVotingToVotingHistory to this method (voteHistory -> dateOfVoting)
    }

    private VoteQuestion createVoteQuestionByQuestion(Question question) {
        VoteQuestion voteQuestion = null;
        List<EventQuestion> votingEvents = chronologyService.findEventsWithVoting(question);
        if (!votingEvents.isEmpty()) {
            voteQuestion = new VoteQuestion();
            //voteQuestion.setVotes();
        }

        return voteQuestion;
    }

    @Override
    @Transactional
    public VoteHistory save(VoteHistory entity) {
        logger.info("entity to save : " + entity.toString());
        if (entity.getVoteQuestions() != null && !entity.getVoteQuestions().isEmpty()) {
            entity.setVoteQuestions(mergeVoteQuestionWithSameId(entity.getVoteQuestions(), new LinkedList<>()));
        }

        VoteHistory savedHistory = super.save(entity);
        logger.info("saved entity : " + entity.toString());
        return savedHistory;
    }

//    private VoteHistory mergeQuestionsWithSameId(VoteHistory voteHistory) {
//        List<VoteQuestion> mergedVoteQuestions = new LinkedList<>();
//        List<VoteQuestion> sourceQuestions = voteHistory.getVoteQuestions();
//        for(VoteQuestion source: sourceQuestions){
//
//        }
//        return null;
//    }

    private List<VoteQuestion> mergeVoteQuestionWithSameId(List<VoteQuestion> sourceList, List<VoteQuestion> sorted) {
        VoteQuestion checkQuestion = sourceList.get(0);
        sourceList.remove(0);
        List<VoteQuestion> forRemove = new LinkedList<>();

        for (VoteQuestion voteQuestion : sourceList) {
            if (voteQuestion.getId().equals(checkQuestion.getId()) && !voteQuestion.equals(checkQuestion)) {
                forRemove.add(voteQuestion);
                checkQuestion = mergeVoteQuestionVotes(checkQuestion, voteQuestion);
            }
        }

        for (VoteQuestion voteQuestion : forRemove) {
            sourceList.remove(voteQuestion);
        }

        sorted.add(checkQuestion);

        if (sourceList.isEmpty()) {
            return sorted;
        } else {
            return mergeVoteQuestionWithSameId(sourceList, sorted);
        }
    }

    @Override
    public VoteHistory saveQuestionsToVoteHistory(LocalDate voteHistoryChronologyDate, List<Question> questions) {
        VoteHistory voteHistory = findByDate(voteHistoryChronologyDate);
        if (voteHistory == null) {
            voteHistory = new VoteHistory();
            voteHistory.setDate(voteHistoryChronologyDate);
        }
        List<VoteQuestion> newVoteQuestions = new LinkedList<>();
        for (Question question : questions) {
            newVoteQuestions.add(chronologyQuestionMapper.mapping(question));
        }
        List<VoteQuestion> existVoteQuestions = voteHistory.getVoteQuestions();
        voteHistory.setVoteQuestions(margeVoteQuestions(existVoteQuestions, newVoteQuestions));

        return save(voteHistory);
    }

    @Override
    public VoteHistory saveVoteQuestionsToVoteHistory(LocalDate voteHistoryChronologyDate, List<VoteQuestion> voteQuestions) {
        VoteHistory voteHistory = findByDate(voteHistoryChronologyDate);
        if (voteHistory == null) {
            voteHistory = new VoteHistory();
            voteHistory.setDate(voteHistoryChronologyDate);
        }

        List<VoteQuestion> existVoteQuestions = voteHistory.getVoteQuestions();
        voteHistory.setVoteQuestions(margeVoteQuestions(existVoteQuestions, voteQuestions));

        return save(voteHistory);
    }

    List<VoteQuestion> margeVoteQuestions(List<VoteQuestion> sourceQuestions, List<VoteQuestion> newQuestions) {
        List<VoteQuestion> mergedList = new LinkedList<>();

        for (VoteQuestion newQuestion : newQuestions) {
            VoteQuestion equalsSourceVoteQuestion = findVoteQuestionInListById(newQuestion.getId(), sourceQuestions);

            if (equalsSourceVoteQuestion != null) {
                sourceQuestions.remove(equalsSourceVoteQuestion);
                VoteQuestion mergedVoteQuestion = null;
                mergedVoteQuestion = mergeVoteQuestionVotes(equalsSourceVoteQuestion, newQuestion);
                mergedList.add(mergedVoteQuestion);
            } else {
                mergedList.add(newQuestion);
            }
        }

        if (!sourceQuestions.isEmpty()) {
            mergedList.addAll(sourceQuestions);
        }
        return mergedList;
    }

    private VoteQuestion findVoteQuestionInListById(Long id, List<VoteQuestion> voteQuestionList) {
        for (VoteQuestion voteQuestion : voteQuestionList) {
            if (id.equals(voteQuestion.getId())) {
                return voteQuestion;
            }
        }
        return null;
    }

    VoteQuestion mergeVoteQuestionVotes(VoteQuestion sourceVoteQuestion, VoteQuestion newVoteQuestion) {
        List<Vote> mergedVotes = sourceVoteQuestion.getVotes();
        if (mergedVotes == null) {
            mergedVotes = new LinkedList<>();
        }

        if (newVoteQuestion.getVotes() != null) {
            for (Vote newVote : newVoteQuestion.getVotes()) {
                if (!isContainVoteInList(sourceVoteQuestion.getVotes(), newVote)) {
                    mergedVotes.add(newVote);
                }
            }
        }
        if (!mergedVotes.isEmpty()) {
            sourceVoteQuestion.setVotes(mergedVotes);
        }

        return sourceVoteQuestion;
    }

    boolean isContainVoteInList(List<Vote> voteList, Vote vote) {
        boolean result = false;
        if (voteList == null) {
            return false;
        }
        for (Vote voteItem : voteList) {
            if ((voteItem.getDate().equals(vote.getDate())) && (voteItem.getName().equals(vote.getName()))) {
                result = true;
                break;
            }
        }

        return result;
    }

    VoteQuestion getEqualsVoteQuestionInList(List<VoteQuestion> list, VoteQuestion voteQuestion) { //TODO pizda а если войт квестион обновился?
        for (VoteQuestion voteQuestionItem : list) {
            if (voteQuestionItem.getId().equals(voteQuestion.getId())) {
                return voteQuestionItem;
            }
        }
        return null;
    }

    @Override
    @Nullable
    @Transactional
    public LocalDateTime getLastVoteDateInVoteHistory(LocalDate voteHistoryDate) {
        VoteHistory voteHistory = findByDate(voteHistoryDate);
        if (voteHistory == null) {
            return null;
        }
        Vote vote = getLastVoteInVoteHistory(voteHistory);
        if (vote == null) {
            return null;
        }
        return vote.getDate();
    }

    @Nullable
    private Vote getLastVoteInVoteHistory(VoteHistory voteHistory) {
        List<Vote> allVotes = findAllVotes(voteHistory);
        if (allVotes.isEmpty()) {
            return null;
        }
        return findLastVote(allVotes);
    }

    private List<Vote> findAllVotes(VoteHistory voteHistory) {
        List<Vote> votes = new LinkedList<>();
        for (VoteQuestion voteQuestion : voteHistory.getVoteQuestions()) {
            if (voteQuestion.getVotes() != null && !voteQuestion.getVotes().isEmpty()) {
                votes.addAll(voteQuestion.getVotes());
            }
        }
        return votes;
    }

    @Nullable
    public VoteQuestion findLastVoteQuestionWithVotes(List<VoteQuestion> voteQuestions) {
        List<VoteQuestion> voteQuestionsWithVotes = findVoteQuestionsWithVotes(voteQuestions);
        if (voteQuestionsWithVotes.isEmpty()) {
            return null;
        }
        return findLastVoteQuestion(voteQuestions);
    }

    private List<VoteQuestion> findVoteQuestionsWithVotes(List<VoteQuestion> voteQuestions) {
        List<VoteQuestion> voteQuestionsWithVotes = new LinkedList<>();
        for (VoteQuestion voteQuestion : voteQuestions) {
            if (voteQuestion.getVotes() != null && !voteQuestion.getVotes().isEmpty()) {
                voteQuestionsWithVotes.add(voteQuestion);
            }
        }
        return voteQuestionsWithVotes;
    }

    //    @Nullable
    private VoteQuestion findLastVoteQuestion(List<VoteQuestion> voteQuestions) {
        if (voteQuestions == null || voteQuestions.isEmpty()) {
            return null;
        }

        VoteQuestion lastVoteQuestion = voteQuestions.get(0);
        for (VoteQuestion voteQuestion : voteQuestions) {
            if (lastVoteQuestion.getDate().isBefore(voteQuestion.getDate())) {
                lastVoteQuestion = voteQuestion;
            }
        }

        return lastVoteQuestion;
    }

    private Vote findLastVote(List<Vote> votes) {
        if (votes == null || votes.isEmpty()) {
            return null;
        }

        Vote lastVote = votes.get(0);
        for (Vote vote : votes) {
            if (lastVote.getDate().isBefore(vote.getDate())) {
                lastVote = vote;
            }
        }
        return lastVote;
    }

    public void setChronologyQuestionMapper(ChronologyQuestionMapper chronologyQuestionMapper) {
        this.chronologyQuestionMapper = chronologyQuestionMapper;
    }

    @Override
    public List<LocalDate> findAllDates() {
        return voteHistoryRepository.findAllDates();
    }

    @Override
    public List<String> findAllDatesAndGetAsRadaStringFormat() {
        List<LocalDate> voteHistoryDates = findAllDates();
        List<String> voteHistoryInRadaStringFormat = new LinkedList<>();
        for (LocalDate date : voteHistoryDates) {
            voteHistoryInRadaStringFormat.add(TimeUtil.getChronologyDateString(date));
        }
        return voteHistoryInRadaStringFormat;
    }

    @Override
    public List<Vote> extractVotesFromVoteHistory(VoteHistory voteHistory) {
        List<Vote> votes = new LinkedList<>();
        for (VoteQuestion voteQuestion : voteHistory.getVoteQuestions()) {
            if (!voteQuestion.getVotes().isEmpty()) {
                votes.addAll(voteQuestion.getVotes());
            }
        }
        return votes;
    }

    @Override
    @Transactional
    @Nullable
    public List<Vote> findVotesFromVoteHistory(LocalDate voteHistoryDate) {
        VoteHistory voteHistory = findByDate(voteHistoryDate);
        if (voteHistory != null) {
            return extractVotesFromVoteHistory(voteHistory);
        }
        return null;
    }

    @Override
    public void uploadChronologyForNinthConvene() {
        List<LocalDate> chronologyDates = chronologyService.getChronologyLocalDates(Convene.NINTH);

        for (LocalDate chronologyDate : chronologyDates) {
            Chronology chronology = chronologyService.getChronology(Convene.NINTH, chronologyDate);
            if (chronology != null) {
                saveChronologyAsVoteHistory(chronology);
            }
        }

    }

    @Override
    public VoteHistory saveChronologyAsVoteHistory(Chronology chronology) {
        VoteHistoryBuilder voteHistoryBuilder = VoteHistoryBuilder.voteHistory()
                .setDate(TimeUtil.getChronologyLocalDate(chronology.getDateAgenda()));

        for (Question question : chronology.getQuestion()) {
            voteHistoryBuilder.addVoteQuestion(chronologyQuestionMapper.mapping(question));
        }

        return save(voteHistoryBuilder.build());
    }

    @Transactional
    @Override
   public List<VoteQuestion> getNewVoteQuestionsFromChronology(LocalDate chronologyDate, Convene convene) {
        List<VoteQuestion> voteQuestions = null;

        LocalDateTime lastVoteEntryInVoteHistory = getLastVoteDateInVoteHistory(chronologyDate);
        if (lastVoteEntryInVoteHistory != null) {
            Chronology chronology = chronologyService.getChronology(convene, chronologyDate);
            List<Question> questions = chronologyService.findQuestionsWithNewVotesAfterDate(
                    chronology,
                    lastVoteEntryInVoteHistory);
            voteQuestions = chronologyQuestionMapper.mappingAll(questions);
        } else {
            Chronology chronology = chronologyService.getChronology(convene, chronologyDate);
            voteQuestions = chronologyQuestionMapper.mappingAll(chronology.getQuestion());
        }

        return voteQuestions;
    }
}