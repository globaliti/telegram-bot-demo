package com.goldsoft.radainfobot.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

@Configuration
public class RadaBotProfilesConfiguration {
    @Configuration
    @Profile("test")
    @PropertySource("classpath:telegram-bot-test.properties")
    static class Test{}

    @Configuration
    @Profile("default")
    @PropertySource("classpath:telegram-bot.properties")
    static class Defaults
    { }
    @Configuration
    @Profile("prod")
    @PropertySource("classpath:telegram-bot.properties")
    static class Production
    { }
}