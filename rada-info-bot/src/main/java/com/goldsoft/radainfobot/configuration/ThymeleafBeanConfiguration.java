package com.goldsoft.radainfobot.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Description;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.templateresolver.ServletContextTemplateResolver;

@Configuration
public class ThymeleafBeanConfiguration {
    @Bean(name = "messageClassLoaderTemplateResolver")
    ClassLoaderTemplateResolver messageClassLoaderTemplateResolver(){
        ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();

        templateResolver.setPrefix("message-templates/");
        templateResolver.setCacheable(false);
        templateResolver.setSuffix(".txt");
        templateResolver.setTemplateMode("TEXT");
      //  templateResolver.setCharacterEncoding("UTF-8");

        return templateResolver;
    }

    @Bean
    @Description("Thymeleaf template engine for messages templates")
    public SpringTemplateEngine messageTemplateEngine() {

        SpringTemplateEngine templateEngine = new SpringTemplateEngine();
        templateEngine.setTemplateResolver(messageClassLoaderTemplateResolver());

        return templateEngine;
    }

}
