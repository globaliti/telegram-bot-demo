package com.goldsoft.radainfobot.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import org.telegram.telegrambots.meta.TelegramBotsApi;

@Configuration
public class BasicBeanConfiguration {
    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }

    @Bean
    public TelegramBotsApi createTelegramBotsApi(){
        return new TelegramBotsApi();
    }
}
