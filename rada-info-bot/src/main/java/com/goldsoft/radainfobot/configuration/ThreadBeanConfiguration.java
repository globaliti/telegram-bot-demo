package com.goldsoft.radainfobot.configuration;

import com.goldsoft.radaOpenDataApi.model.enums.Convene;
import com.goldsoft.radainfobot.threading.ChronologyUpdateChecker;
import com.goldsoft.radainfobot.threading.FollowingChronologyTask;
import com.goldsoft.radainfobot.threading.FrequencyUpdate;
import com.goldsoft.radainfobot.threading.voteHistory.NoUpdatesCounter;
import com.goldsoft.radainfobot.threading.voteHistory.VoteHistoryFollowingTask;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

@Configuration
public class ThreadBeanConfiguration {

    @Bean
    @Scope(value = "prototype")
    @SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection"})
    public VoteHistoryFollowingTask voteHistoryFollowingThread(LocalDate chronologyDate,
                                                               Convene convene,
                                                               NoUpdatesCounter noUpdatesCounter) {
        return new VoteHistoryFollowingTask(chronologyDate, convene, noUpdatesCounter);
    }

    @Bean
    @Scope(value = "prototype")
    @SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection"})
    public VoteHistoryFollowingTask voteHistoryFollowingThread(Date chronologyDate,
                                                               Convene convene,
                                                               NoUpdatesCounter noUpdatesCounter) {
        return new VoteHistoryFollowingTask(chronologyDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate(),
                convene,
                noUpdatesCounter);
    }
    @Bean
    @Scope(value = "prototype")
    @SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection"})
    public ChronologyUpdateChecker chronologyUpdateChecker(FrequencyUpdate frequencyUpdate, Convene convene) {
        return new ChronologyUpdateChecker(frequencyUpdate, convene);
    }

    @Bean
    @Scope(value = "prototype")
    @SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection"})
    public FollowingChronologyTask followingChronologyTask(FrequencyUpdate frequencyUpdate, Convene convene) {
        return new FollowingChronologyTask(frequencyUpdate, convene);
    }


//    @Bean
//    @Scope(value = "prototype")
//    @SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection"})
//    public TestThread testThread(String name){
//        return new TestThread(name);
//    }
//
//    @Bean
//    @Scope(value = "prototype")
//    @SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection"})
//    public TestTrigger testTrigger(String name){
//        return new TestTrigger(name);
//    }
//
//    @Bean
//    public TriggerContext triggerContext(){
//        return new SimpleTriggerContext();
//    }
}
