package com.goldsoft.radainfobot.domain;

public interface CloneableModel<Model> {
    Model cloneModel();
}
