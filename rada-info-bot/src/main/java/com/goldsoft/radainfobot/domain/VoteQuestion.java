package com.goldsoft.radainfobot.domain;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

@Entity(name = "VoteQuestion")
@Table(name = "vote_question")
public final class VoteQuestion implements CloneableModel<VoteQuestion> {
    @Id
    private Long id;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Vote> votes;

    @Column(name = "date", nullable = false)
    private LocalDateTime date;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Vote> getVotes() {
        return votes;
    }

    public void setVotes(List<Vote> votes) {
        this.votes = votes;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public VoteQuestion() {
    }

    public VoteQuestion(Long id) {
        this.id = id;
    }

    public VoteQuestion(Long id, List<Vote> votes, LocalDateTime date) {
        this.id = id;
        this.votes = votes;
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VoteQuestion that = (VoteQuestion) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(votes, that.votes) &&
                Objects.equals(date, that.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, votes, date);
    }

    @Override
    public String toString() {
        return "VoteQuestion{" +
                "id=" + id +
                ", votes=" + votes +
                ", date=" + date +
                '}';
    }

    @Override
    public VoteQuestion cloneModel() {
        VoteQuestion voteQuestion = new VoteQuestion();
        voteQuestion.setId(id);
        voteQuestion.setDate(date);

        if (votes != null && !votes.isEmpty()) {
            List<Vote> newVotes = new LinkedList<>();
            for (Vote vote : votes) {
                newVotes.add(vote.cloneModel());
            }
            voteQuestion.setVotes(newVotes);
        }
        return voteQuestion;
    }
}