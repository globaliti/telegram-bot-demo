package com.goldsoft.radainfobot.domain.enums;

public enum TypeEvent {
    VOTING(0), SPEECH(8), REGISTRATION(1);
    private int eventValue;

    TypeEvent(int eventValue) {
        this.eventValue = eventValue;
    }

    public int getEventValue() {
        return eventValue;
    }
}
