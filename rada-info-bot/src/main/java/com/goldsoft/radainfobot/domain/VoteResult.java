package com.goldsoft.radainfobot.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity(name = "VoteResult")
@Table(name = "vote_result")
public class VoteResult implements CloneableModel<VoteResult> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "voted_for", nullable = false)
    private Short votedFor;

    @Column(name = "voted_against", nullable = false)
    private Short votedAgainst;

    @Column(name = "abstain", nullable = false)
    private Short abstain;

    @Column(name = "not_voted", nullable = false)
    private Short notVoted;

    @Column(name = "total", nullable = false)
    private Short total;

    @Column(name = "presence", nullable = false)
    private Short presence;

    @Column(name = "absent", nullable = false)
    private Short absent;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Short getVotedFor() {
        return votedFor;
    }

    public void setVotedFor(Short votedFor) {
        this.votedFor = votedFor;
    }

    public Short getVotedAgainst() {
        return votedAgainst;
    }

    public void setVotedAgainst(Short votedAgainst) {
        this.votedAgainst = votedAgainst;
    }

    public Short getAbstain() {
        return abstain;
    }

    public void setAbstain(Short abstain) {
        this.abstain = abstain;
    }

    public Short getNotVoted() {
        return notVoted;
    }

    public void setNotVoted(Short notVoted) {
        this.notVoted = notVoted;
    }

    public Short getTotal() {
        return total;
    }

    public void setTotal(Short total) {
        this.total = total;
    }

    public Short getPresence() {
        return presence;
    }

    public void setPresence(Short presence) {
        this.presence = presence;
    }

    public Short getAbsent() {
        return absent;
    }

    public void setAbsent(Short absent) {
        this.absent = absent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VoteResult that = (VoteResult) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(votedFor, that.votedFor) &&
                Objects.equals(votedAgainst, that.votedAgainst) &&
                Objects.equals(abstain, that.abstain) &&
                Objects.equals(notVoted, that.notVoted) &&
                Objects.equals(total, that.total) &&
                Objects.equals(presence, that.presence) &&
                Objects.equals(absent, that.absent);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, votedFor, votedAgainst, abstain, notVoted, total, presence, absent);
    }

    @Override
    public String toString() {
        return "VoteResult{" +
                "id=" + id +
                ", votedFor=" + votedFor +
                ", votedAgainst=" + votedAgainst +
                ", abstain=" + abstain +
                ", notVoted=" + notVoted +
                ", total=" + total +
                ", presence=" + presence +
                ", absent=" + absent +
                '}';
    }

    @Override
    public VoteResult cloneModel() {
        VoteResult voteResult = new VoteResult();
        voteResult.setId(id);
        voteResult.setVotedFor(votedFor);
        voteResult.setVotedAgainst(votedAgainst);
        voteResult.setAbstain(abstain);
        voteResult.setNotVoted(notVoted);
        voteResult.setTotal(total);
        voteResult.setPresence(presence);
        voteResult.setAbsent(absent);
        return voteResult;
    }
}