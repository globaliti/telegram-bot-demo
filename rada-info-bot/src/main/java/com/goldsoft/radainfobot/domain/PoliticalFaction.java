package com.goldsoft.radainfobot.domain;

import javax.persistence.*;
import java.util.List;

@Entity(name = "PoliticalFaction")
@Table(name = "political_faction")
public final class PoliticalFaction {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Short id;

    @Column(name = "name", nullable = false)
    private String name;

    @OneToMany
    private List<ParliamentMember> members;

    public Short getId() {
        return id;
    }

    public void setId(Short id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ParliamentMember> getMembers() {
        return members;
    }

    public void setMembers(List<ParliamentMember> members) {
        this.members = members;
    }
}
