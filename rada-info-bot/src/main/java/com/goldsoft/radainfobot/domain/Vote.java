package com.goldsoft.radainfobot.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Map;
import java.util.Objects;

@Entity(name = "Vote")
@Table(name = "vote")
public final class Vote implements CloneableModel<Vote> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToOne(cascade = CascadeType.ALL)
    @NotNull
    private VoteResult voteResult;

    @Column(nullable = false)
    private LocalDateTime date;

    @Column(nullable = false)
    @Size(max = 100000)
    private String name;

    //     k - id
    @OneToMany(cascade = CascadeType.ALL)
//    @NotNull
    private Map<ParliamentMember, VoteResult> resultMps;

    @OneToMany(cascade = CascadeType.ALL)
//    @NotNull
    private Map<String, VoteResult> resultFactions;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public VoteResult getVoteResult() {
        return voteResult;
    }

    public void setVoteResult(VoteResult voteResult) {
        this.voteResult = voteResult;
    }

    public Map<ParliamentMember, VoteResult> getResultMps() {
        return resultMps;
    }

    public void setResultMps(Map<ParliamentMember, VoteResult> resultMps) {
        this.resultMps = resultMps;
    }

    public Map<String, VoteResult> getResultFactions() {
        return resultFactions;
    }

    public void setResultFactions(Map<String, VoteResult> resultFactions) {
        this.resultFactions = resultFactions;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vote vote = (Vote) o;
        return Objects.equals(id, vote.id) &&
                Objects.equals(voteResult, vote.voteResult) &&
                Objects.equals(date, vote.date) &&
                Objects.equals(name, vote.name) &&
                Objects.equals(resultMps, vote.resultMps) &&
                Objects.equals(resultFactions, vote.resultFactions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, voteResult, date, name, resultMps, resultFactions);
    }

    @Override
    public String toString() {
        return "Vote{" +
                "id=" + id +
                ", voteResult=" + voteResult +
                ", date=" + date +
                ", name='" + name + '\'' +
                ", resultMps=" + resultMps +
                ", resultFactions=" + resultFactions +
                '}';
    }

    @Override
    public Vote cloneModel() {
        Vote vote = new Vote();
        vote.setId(id);
        vote.setName(name);
        vote.setDate(date);
        if (voteResult != null) {
            vote.setVoteResult(voteResult.cloneModel());
        }
        return vote;
    }
}