package com.goldsoft.radainfobot.domain.builder;

import com.goldsoft.radainfobot.domain.Vote;
import com.goldsoft.radainfobot.domain.VoteQuestion;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class VoteQuestionBuilder {
    private Long id;

    private List<Vote> votes = new LinkedList<>();

    private LocalDateTime date;

    public static VoteQuestionBuilder voteQuestion(Long id) {
        return new VoteQuestionBuilder().setId(id);
    }

    public VoteQuestionBuilder setId(Long id) {
        this.id = id;
        return this;
    }

    public VoteQuestionBuilder addVote(Vote vote) {
        this.votes.add(vote);
        return this;
    }

    public VoteQuestionBuilder resetVotes() {
        votes = new LinkedList<>();
        return this;
    }

    public VoteQuestionBuilder setDate(LocalDateTime date) {
        this.date = date;
        return this;
    }

    public VoteQuestion build() {
        VoteQuestion voteQuestion = new VoteQuestion();
        voteQuestion.setId(this.id);
        List<Vote> newVotes = new LinkedList<>();
        for (Vote vote : votes) {
            newVotes.add(vote.cloneModel());
        }
        voteQuestion.setVotes(newVotes);
        voteQuestion.setDate(date);
        return voteQuestion;
    }
}