package com.goldsoft.radainfobot.domain.builder;

import com.goldsoft.radainfobot.domain.VoteResult;

public class VoteResultBuilder {
    private Long id;

    private Short votedFor;

    private Short votedAgainst;

    private Short abstain;

    private Short notVoted;

    private Short total;

    private Short presence;

    private Short absent;

    public static VoteResultBuilder voteResult(){
        return new VoteResultBuilder();
    }

    public VoteResultBuilder setId(Long id) {
        this.id = id;
        return this;
    }

    public VoteResultBuilder setVotedFor(Short votedFor) {
        this.votedFor = votedFor;
        return this;
    }

    public VoteResultBuilder setVotedAgainst(Short votedAgainst) {
        this.votedAgainst = votedAgainst;
        return this;
    }

    public VoteResultBuilder setAbstain(Short abstain) {
        this.abstain = abstain;
        return this;
    }

    public VoteResultBuilder setNotVoted(Short notVoted) {
        this.notVoted = notVoted;
        return this;
    }

    public VoteResultBuilder setTotal(Short total) {
        this.total = total;
        return this;
    }

    public VoteResultBuilder setPresence(Short presence) {
        this.presence = presence;
        return this;
    }

    public VoteResultBuilder setAbsent(Short absent) {
        this.absent = absent;
        return this;
    }

    public VoteResult build() {
        VoteResult voteResult = new VoteResult();
        voteResult.setId(this.id);
        voteResult.setAbsent(this.absent);
        voteResult.setTotal(this.total);
        voteResult.setVotedAgainst(this.votedAgainst);
        voteResult.setVotedFor(this.votedFor);
        voteResult.setPresence(this.presence);
        voteResult.setNotVoted(this.notVoted);
        voteResult.setAbstain(this.abstain);
        return voteResult;
    }
}