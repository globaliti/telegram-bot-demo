package com.goldsoft.radainfobot.domain.builder;

import com.goldsoft.radainfobot.domain.VoteHistory;
import com.goldsoft.radainfobot.domain.VoteQuestion;

import java.time.LocalDate;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class VoteHistoryBuilder {

    private Long id;

    private List<VoteQuestion> voteQuestions = new LinkedList<>();

    private LocalDate date;

    public static VoteHistoryBuilder voteHistory() {
        return new VoteHistoryBuilder();
    }

    public VoteHistoryBuilder setId(Long id) {
        this.id = id;
        return this;
    }

    public VoteHistoryBuilder addVoteQuestion(VoteQuestion voteQuestion) {
        voteQuestions.add(voteQuestion);
        return this;
    }

    public VoteHistoryBuilder resetVoteQuestions() {
        voteQuestions = new LinkedList<>();
        return this;
    }

//    public VoteHistoryBuilder setVoteQuestions(List<VoteQuestion> voteQuestions) {
//        this.voteQuestions = voteQuestions;
//        return this;
//    }

    public VoteHistoryBuilder setDate(LocalDate date) {
        this.date = date;
        return this;
    }

    public VoteHistory build() {
        VoteHistory voteHistory = new VoteHistory();
        voteHistory.setId(id);
        voteHistory.setDate(date);
        if (voteQuestions != null && !voteQuestions.isEmpty()) {
            List<VoteQuestion> newVoteQuestions = new LinkedList<>();
            for (VoteQuestion voteQuestion : voteQuestions) {
                newVoteQuestions.add(voteQuestion.cloneModel());
            }
            voteHistory.setVoteQuestions(newVoteQuestions);
        }
        return voteHistory;
    }
}