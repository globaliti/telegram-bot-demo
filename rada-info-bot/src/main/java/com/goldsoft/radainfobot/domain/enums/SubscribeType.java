package com.goldsoft.radainfobot.domain.enums;

public enum SubscribeType {
    VOTING_HISTORY_CHANGING("Изменения в истории голосования"),
    MP_CHANGING("Изменения членов парламента"),
    FACTION_CHANGING("Изменения в структуре партий");

    private final String RUSSIAN_NAME;

    SubscribeType(final String russianName) {
        this.RUSSIAN_NAME = russianName;
    }

    public String getRussianStringValue(){
        return this.RUSSIAN_NAME;
    }
}
