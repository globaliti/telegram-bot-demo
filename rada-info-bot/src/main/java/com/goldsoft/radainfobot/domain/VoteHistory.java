package com.goldsoft.radainfobot.domain;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "vote_history")
public final class VoteHistory implements CloneableModel<VoteHistory> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToMany(cascade = CascadeType.ALL)
    private List<VoteQuestion> voteQuestions = new LinkedList<>();

    @Column(name = "date", nullable = false, unique = true, updatable = false)
    private LocalDate date;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<VoteQuestion> getVoteQuestions() {
        return voteQuestions;
    }

    public void setVoteQuestions(List<VoteQuestion> voteQuestions) {
        this.voteQuestions = voteQuestions;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VoteHistory that = (VoteHistory) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(voteQuestions, that.voteQuestions) &&
                Objects.equals(date, that.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, voteQuestions, date);
    }

    @Override
    public String toString() {
        return "VoteHistory{" +
                "id=" + id +
                ", voteQuestions=" + voteQuestions +
                ", date=" + date +
                '}';
    }

    @Override
    public VoteHistory cloneModel() {
        VoteHistory voteHistory = new VoteHistory();
        voteHistory.setId(id);
        if (voteQuestions != null && !voteQuestions.isEmpty()) {
            List<VoteQuestion> newVoteQuestions = new LinkedList<>();
            for (VoteQuestion voteQuestion : voteQuestions) {
                newVoteQuestions.add(voteQuestion.cloneModel());
            }
            voteHistory.setVoteQuestions(newVoteQuestions);
        }
        voteHistory.setDate(date);
        return voteHistory;
    }
}