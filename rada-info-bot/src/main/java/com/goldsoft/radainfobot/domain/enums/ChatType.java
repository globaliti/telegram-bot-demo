package com.goldsoft.radainfobot.domain.enums;

public enum ChatType {
    PRIVATE, CHANNEL, SUPER_GROUP, GROUP;
}
