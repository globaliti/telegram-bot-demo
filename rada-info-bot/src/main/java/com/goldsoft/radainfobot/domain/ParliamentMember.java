package com.goldsoft.radainfobot.domain;

import com.goldsoft.radainfobot.domain.enums.Sex;

import javax.persistence.*;
import java.util.Objects;

@Entity(name = "ParliamentMember")
@Table(name = "parliament_member")
public final class ParliamentMember implements CloneableModel<ParliamentMember> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Short id;

    @Column(name = "name", nullable = false)
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(name = "sex", nullable = false)
    private Sex sex;

    @ManyToOne
    private PoliticalFaction politicalFaction;

    public Short getId() {
        return id;
    }

    public void setId(Short id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public PoliticalFaction getPoliticalFaction() {
        return politicalFaction;
    }

    public void setPoliticalFaction(PoliticalFaction politicalFaction) {
        this.politicalFaction = politicalFaction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParliamentMember that = (ParliamentMember) o;
        return id.equals(that.id) &&
                name.equals(that.name) &&
                sex == that.sex;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, sex);
    }

    @Override
    public String toString() {
        return "ParliamentMember{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", sex=" + sex +
                ", politicalFaction=" + politicalFaction +
                '}';
    }

    @Override
    public ParliamentMember cloneModel() {
        ParliamentMember parliamentMember = new ParliamentMember();
        parliamentMember.setId(id);
        parliamentMember.setName(name);
        parliamentMember.setSex(sex);
        return null;
    }
}