package com.goldsoft.radainfobot.domain.enums;

public enum Sex {
    FEMALE(1), MALE(2);
    private int sexValue;

    private Sex(int sexValue) {
        this.sexValue = sexValue;
    }

    public int getSexValue() {
        return sexValue;
    }
}
