package com.goldsoft.radainfobot.domain.builder;

import com.goldsoft.radainfobot.domain.ParliamentMember;
import com.goldsoft.radainfobot.domain.Vote;
import com.goldsoft.radainfobot.domain.VoteResult;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class VoteBuilder {

    private Long id;

    private VoteResult voteResult;

    private LocalDateTime date;

    private String name;

    private Map<ParliamentMember, VoteResult> resultMps = new HashMap<>();

    private Map<String, VoteResult> resultFactions = new HashMap<>();

    public static VoteBuilder vote() {
        return new VoteBuilder();
    }

    public VoteBuilder setId(Long id) {
        this.id = id;
        return this;
    }

    public VoteBuilder setVoteResult(VoteResult voteResult) {
        this.voteResult = voteResult;
        return this;
    }

    public VoteBuilder setDate(LocalDateTime date) {
        this.date = date;
        return this;
    }

    public VoteBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public VoteBuilder addResultMps(ParliamentMember parliamentMember, VoteResult voteResult) {
        resultMps.put(parliamentMember, voteResult);
        return this;
    }

    public VoteBuilder resetResultMps() {
        resultMps = new HashMap<>();
        return this;
    }

    public VoteBuilder resetResultFactions() {
        resultFactions = new HashMap<>();
        return this;
    }

    public VoteBuilder addResultFactions(String factionName, VoteResult voteResult) {
        resultFactions.put(factionName, voteResult);
        return this;
    }

    public Vote build() {
        Vote vote = new Vote();
        vote.setId(id);
        vote.setDate(date);
        vote.setName(name);
        if (voteResult != null) {
            vote.setVoteResult(voteResult.cloneModel());
        }
        if (resultFactions != null && !resultFactions.isEmpty()) {
            Map<String, VoteResult> newMap = new HashMap<>();
            for (String key : resultFactions.keySet()) {
                newMap.put(key, resultFactions.get(key).cloneModel());
            }
            vote.setResultFactions(newMap);
        }
        if (resultMps != null && !resultMps.isEmpty()) {
            Map<ParliamentMember, VoteResult> newMap = new HashMap<>();
            for (ParliamentMember parliamentMemberKey : resultMps.keySet()) {
                newMap.put(parliamentMemberKey.cloneModel(), (VoteResult) resultMps.get(parliamentMemberKey).cloneModel());
            }
            vote.setResultMps(newMap);
        }
        vote.setResultMps(resultMps);
        return vote;
    }
}
