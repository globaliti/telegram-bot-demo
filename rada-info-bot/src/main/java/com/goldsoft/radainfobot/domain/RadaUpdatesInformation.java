package com.goldsoft.radainfobot.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Entity
public class RadaUpdatesInformation implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Date chronologyNinthConveneUpdateTime;
    private Date mpsNinthConveneUpdateTime;
    private Date agendasNinthConveneUpdateTime;
    private Date factionNinthConveneUpdateTime; //TODO check one time before chronology checking

    //TODO stooping time, if file will be does'nt updates for 2 hours(for example), stop checking for updates
}
