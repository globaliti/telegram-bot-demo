package com.goldsoft.radainfobot.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

@Entity
public class SystemConfiguration {
    @Id
    private final Long id = 1L; //Must be only one in system

    @Column(nullable = false)
    private Long chronologyFrequencyUpdate;

    @Column(nullable = false)
    private Long voteHistoryFrequencyUpdate;

    @Column(nullable = false)
    private Long chronologyCheckingFrequencyUpdate;

    public Long getChronologyFrequencyUpdate() {
        return chronologyFrequencyUpdate;
    }

    public void setChronologyFrequencyUpdate(Long chronologyFrequencyUpdate) {
        this.chronologyFrequencyUpdate = chronologyFrequencyUpdate;
    }

    public Long getVoteHistoryFrequencyUpdate() {
        return voteHistoryFrequencyUpdate;
    }

    public void setVoteHistoryFrequencyUpdate(Long voteHistoryFrequencyUpdate) {
        this.voteHistoryFrequencyUpdate = voteHistoryFrequencyUpdate;
    }

    public Long getChronologyCheckingFrequencyUpdate() {
        return chronologyCheckingFrequencyUpdate;
    }

    public void setChronologyCheckingFrequencyUpdate(Long chronologyCheckingFrequencyUpdate) {
        this.chronologyCheckingFrequencyUpdate = chronologyCheckingFrequencyUpdate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SystemConfiguration that = (SystemConfiguration) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(chronologyFrequencyUpdate, that.chronologyFrequencyUpdate) &&
                Objects.equals(voteHistoryFrequencyUpdate, that.voteHistoryFrequencyUpdate) &&
                Objects.equals(chronologyCheckingFrequencyUpdate, that.chronologyCheckingFrequencyUpdate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, chronologyFrequencyUpdate, voteHistoryFrequencyUpdate, chronologyCheckingFrequencyUpdate);
    }

    @Override
    public String toString() {
        return "SystemConfiguration{" +
                "id=" + id +
                ", chronologyFrequencyUpdate=" + chronologyFrequencyUpdate +
                ", voteHistoryFrequencyUpdate=" + voteHistoryFrequencyUpdate +
                ", chronologyCheckingFrequencyUpdate=" + chronologyCheckingFrequencyUpdate +
                '}';
    }
}
