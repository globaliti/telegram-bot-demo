package com.goldsoft.radainfobot.domain;

import com.goldsoft.radainfobot.domain.enums.ChatType;
import com.goldsoft.radainfobot.domain.enums.SubscribeType;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Entity(name = "Subscriber")
@Table(name = "subscriber")
public class Subscriber {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "chat_id", nullable = false, unique = true)
    private String chatId;

    @ElementCollection(targetClass = SubscribeType.class)
    @Enumerated(EnumType.STRING)
    @CollectionTable(name="subscribe_types")
    @Column(name="subscribe_types")
    private Set<SubscribeType> subscribeTypes;

    @Enumerated(EnumType.STRING)
    @Column(name = "chat_type", nullable = true)
    private ChatType chatType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<SubscribeType> getSubscribeTypes() {
        return subscribeTypes;
    }

    public void setSubscribeTypes(Set<SubscribeType> subscribeTypes) {
        this.subscribeTypes = subscribeTypes;
    }

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public ChatType getChatType() {
        return chatType;
    }

    public void setChatType(ChatType chatType) {
        this.chatType = chatType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Subscriber that = (Subscriber) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(chatId, that.chatId) &&
                Objects.equals(subscribeTypes, that.subscribeTypes) &&
                chatType == that.chatType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, chatId, subscribeTypes, chatType);
    }

    @Override
    public String toString() {
        return "Subscriber{" +
                "id=" + id +
                ", chatId='" + chatId + '\'' +
                ", subscribeTypes=" + subscribeTypes +
                ", chatType=" + chatType +
                '}';
    }
}
