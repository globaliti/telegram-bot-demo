package com.goldsoft.radainfobot.mapper;

import java.util.List;

public interface Mapper<A, E> { //A- apiEntity E - entity
    E mapping(A apiEntity);

    A unMapping(E entity);

    List<A> unMappingAll(List<E> entities);

    List<E> mappingAll(List<A> apiEntities);
}