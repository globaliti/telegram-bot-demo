package com.goldsoft.radainfobot.mapper;

import com.goldsoft.radaOpenDataApi.model.chronology.EventQuestion;
import com.goldsoft.radaOpenDataApi.model.chronology.Question;
import com.goldsoft.radaOpenDataApi.model.chronology.ResultEvent;
import com.goldsoft.radaOpenDataApi.util.TimeUtil;
import com.goldsoft.radainfobot.domain.Vote;
import com.goldsoft.radainfobot.domain.VoteQuestion;
import com.goldsoft.radainfobot.domain.VoteResult;
import com.goldsoft.radainfobot.domain.enums.TypeEvent;
import com.goldsoft.radainfobot.service.voteQuestion.VoteQuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class ChronologyQuestionMapper implements Mapper<Question, VoteQuestion> {

    @Override
    public VoteQuestion mapping(Question entityApi) {
        VoteQuestion voteQuestion = new VoteQuestion();
        List<Vote> votes = new LinkedList<>();
        for (EventQuestion eventQuestion : entityApi.getEventQuestion()) {
            if (eventQuestion.getTypeEvent().intValue() == TypeEvent.VOTING.getEventValue()) {
                votes.add(mappingVote(eventQuestion));
            }
        }
        voteQuestion.setVotes(votes);
        voteQuestion.setId(entityApi.getIdQuestion().longValue());
        voteQuestion.setDate(TimeUtil.getLocalDateTime(entityApi.getDateQuestion()));

        return voteQuestion;
    }

    @Override
    public Question unMapping(VoteQuestion entity) {
        return null;
    }

    private Vote mappingVote(EventQuestion eventQuestion) {
        Vote vote = null;
        if (eventQuestion.getTypeEvent().intValue() == TypeEvent.VOTING.getEventValue()) {
            vote = new Vote();
            vote.setDate(TimeUtil.getLocalDateTime(eventQuestion.getDateEvent()));
            vote.setName(eventQuestion.getNameEvent());
            vote.setVoteResult(mappingVoteResult(eventQuestion.getResultEvent()));
        }

        return vote;
    }

    private VoteResult mappingVoteResult(ResultEvent resultEvent) {
        VoteResult voteResult = new VoteResult();

        voteResult.setAbsent(resultEvent.getAbsent().shortValueExact());
        voteResult.setAbstain(resultEvent.getAbstain().shortValueExact());
        voteResult.setNotVoted(resultEvent.getNotVoting().shortValueExact());
        voteResult.setPresence(resultEvent.getPresence().shortValueExact());
        voteResult.setTotal(resultEvent.getTotal().shortValueExact());
        voteResult.setVotedAgainst(resultEvent.getAgainst().shortValueExact());
        voteResult.setVotedFor(resultEvent.getFor().shortValueExact());

        return voteResult;
    }

    @Override
    public List<Question> unMappingAll(List<VoteQuestion> entities) {
        List<Question> questions = new LinkedList<>();
        for (VoteQuestion voteQuestion : entities) {
            questions.add(unMapping(voteQuestion));
        }
        return questions;
    }

    @Override
    public List<VoteQuestion> mappingAll(List<Question> apiEntities) {
        List<VoteQuestion> voteQuestions = new LinkedList<>();
        for (Question question : apiEntities) {
            voteQuestions.add(mapping(question));
        }
        return voteQuestions;
    }
}