package com.goldsoft.radainfobot.bootstrap;

import com.goldsoft.radaOpenDataApi.model.enums.Convene;
import com.goldsoft.radainfobot.client.TelegramBotClientImpl;
import com.goldsoft.radainfobot.domain.Subscriber;
import com.goldsoft.radainfobot.domain.SystemConfiguration;
import com.goldsoft.radainfobot.service.subscribe.SubscriberService;
import com.goldsoft.radainfobot.service.subscription.SubscriptionService;
import com.goldsoft.radainfobot.service.systemConfiguration.SystemConfigurationService;
import com.goldsoft.radainfobot.service.voteHistory.VoteHistoryService;
import com.goldsoft.radainfobot.threading.FollowingThreadsManager;
import com.goldsoft.radainfobot.threading.FrequencyUpdate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiRequestException;

import java.util.List;

@Service
@Profile({"prod", "default", "test"})
public class BootstrapService implements InitializingBean {

    Logger logger = LoggerFactory.getLogger(BootstrapService.class);

    @Autowired
    VoteHistoryService voteHistoryService;

    @Autowired
    private SubscriberService subscriberService;

    @Autowired
    private SubscriptionService subscriptionService;

    @Autowired
    private FollowingThreadsManager followingThreadsManager;

    @Autowired
    private SystemConfigurationService systemConfigurationService;

    @Autowired
    private TelegramBotsApi telegramBotsApi;

    @Autowired
    private TelegramBotClientImpl telegramBotClientImpl;

    @Override
    public void afterPropertiesSet() throws Exception {
        logger.info("Load subscribers...");
        loadSubscribers();
        logger.info("Create system configuration if not exists...");
        createSystemConfigurationIfNotExist();
        logger.info("Registration bots...");
        registrationBots();
        logger.info("Start following chronology updates for ninth convene...");
        followingNinthConvene();
        uploadAllVoteHistoryForConvene();
    }

   public void loadSubscribers() {
        List<Subscriber> subscribers = subscriberService.findAll();
        for (Subscriber subscriber : subscribers) {
            subscriptionService.addSubscriberToPool(subscriber);
        }
    }

    private void followingNinthConvene() {
        FrequencyUpdate frequencyUpdate = new FrequencyUpdate(0, 0, 1, 0);
        followingThreadsManager.followingChronologies(frequencyUpdate, Convene.NINTH);
    }

    private void loadChronologiesForNinthConvene() {

    }//TODO implement this

    private void createSystemConfigurationIfNotExist() { //TODO load this values from property
        if (systemConfigurationService.getConfiguration() == null) {
            SystemConfiguration systemConfiguration = new SystemConfiguration();
            systemConfiguration.setChronologyCheckingFrequencyUpdate(3600000L);
            systemConfiguration.setChronologyFrequencyUpdate(3600000L);
            systemConfiguration.setVoteHistoryFrequencyUpdate(60000L);
            SystemConfiguration savedConfiguration = systemConfigurationService.save(systemConfiguration);
            logger.info("Created default system configuration :" + savedConfiguration);
        }
    }

    private void registrationBots() {
        try {
            telegramBotsApi.registerBot(telegramBotClientImpl);
        } catch (TelegramApiRequestException e) {
            e.printStackTrace();
        }
    }

    private void uploadAllVoteHistoryForConvene() {
        if (voteHistoryService.findAll().isEmpty()) {
            voteHistoryService.uploadChronologyForNinthConvene();
        }
    }
}