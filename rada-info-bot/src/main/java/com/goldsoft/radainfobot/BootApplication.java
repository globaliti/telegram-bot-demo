package com.goldsoft.radainfobot;

import com.goldsoft.radainfobot.configuration.ThreadBeanConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.ContextStartedEvent;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.telegram.telegrambots.ApiContextInitializer;

//@SpringBootApplication(scanBasePackages = {"com.goldsoft.radaOpenDataApi.*","com.goldsoft.radainfobot.service*","com.goldsoft.radainfobot.configuration"})
@SpringBootApplication(scanBasePackages = {"com.goldsoft.radaOpenDataApi.*", "com.goldsoft.radainfobot.*"})
@EnableScheduling
public class BootApplication {
    public static void main(String[] args) {
        SpringApplication.run(BootApplication.class, args);
    }

    static {
        ApiContextInitializer.init();
    }
}