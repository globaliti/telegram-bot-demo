package com.goldsoft.radainfobot.threading;

public class VoteHistoryFollowingException extends RuntimeException {
    public VoteHistoryFollowingException() {
        super();
    }

    public VoteHistoryFollowingException(String message) {
        super(message);
    }

    public VoteHistoryFollowingException(String message, Throwable cause) {
        super(message, cause);
    }

    public VoteHistoryFollowingException(Throwable cause) {
        super(cause);
    }

    protected VoteHistoryFollowingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}