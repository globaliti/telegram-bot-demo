package com.goldsoft.radainfobot.threading;

import com.goldsoft.radaOpenDataApi.model.enums.Convene;
import com.goldsoft.radainfobot.threading.voteHistory.VoteHistoryFollowingTask;
import com.goldsoft.radainfobot.threading.voteHistory.VoteHistoryTrigger;

public interface FollowingThreadsManager {

    /**
     * Run schedule task that check new updates for Chronology.
     * If found new chronology, run thread for following changes.
     */
    void followingChronologies(FrequencyUpdate frequencyUpdate, Convene convene);

    void shutdown();

    void runVoteHistoryFollowing(VoteHistoryFollowingTask voteHistoryFollowingTask, VoteHistoryTrigger voteHistoryTrigger);
}