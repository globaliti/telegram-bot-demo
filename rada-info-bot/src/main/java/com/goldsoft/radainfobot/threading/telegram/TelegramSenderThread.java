package com.goldsoft.radainfobot.threading.telegram;

import com.goldsoft.radainfobot.service.telegram.MessageRequestParam;
import com.goldsoft.radainfobot.service.telegram.TelegramChatService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class TelegramSenderThread implements Runnable {//TODO meybe rename to TelegramParalelSender

    Logger logger = LoggerFactory.getLogger(TelegramSenderThread.class);

    private TelegramSenderConcurrentModel telegramSenderConcurrentModel;

    public TelegramSenderThread() {
    }

    public TelegramSenderThread(TelegramSenderConcurrentModel telegramSenderConcurrentModel) {
        this.telegramSenderConcurrentModel = telegramSenderConcurrentModel;
    }

    @Autowired
    private TelegramChatService telegramChatService;

    @Override
    public void run() {
        String receiver = telegramSenderConcurrentModel.getRecipients().poll();
        while (receiver != null && !Thread.currentThread().isInterrupted()) {
            for (String message : telegramSenderConcurrentModel.getMessagesForSend()) {
                MessageRequestParam messageRequestParam = new MessageRequestParam(receiver, message);
                telegramChatService.sendMessage(messageRequestParam);
            }
            logger.info("Message was send to " + receiver + " in thread " + Thread.currentThread().getName());
            receiver = telegramSenderConcurrentModel.getRecipients().poll();
        }
    }
}