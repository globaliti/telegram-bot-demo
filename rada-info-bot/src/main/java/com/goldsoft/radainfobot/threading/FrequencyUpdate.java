package com.goldsoft.radainfobot.threading;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class FrequencyUpdate {
    private int days;
    private int hours;
    private int minutes;
    private int seconds;
    private long milliseconds;

    public FrequencyUpdate(Long milliseconds) {
        initFieldsByMilliseconds(milliseconds);
        calculateMilliseconds();
    }

    public FrequencyUpdate(int days, int hours, int minutes, int seconds) {
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = seconds;
        this.days = days;
        calculateMilliseconds();

    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public int getSeconds() {
        return seconds;
    }

    public void setSeconds(int seconds) {
        this.seconds = seconds;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    private void calculateMilliseconds() {
        long sum = 0;
        if (days != 0) {
            sum += days * 24 * 60 * 60 * 1000;
        }
        if (hours != 0) {
            sum += hours * 60 * 60 * 1000;
        }
        if (minutes != 0) {
            sum += minutes * 60 * 1000;
        }
        if (seconds != 0) {
            sum += seconds * 1000;
        }
        milliseconds = sum;
    }

    private void initFieldsByMilliseconds(long milliseconds) {
        TimeUnit timeUnit = TimeUnit.MILLISECONDS;
        this.days = (int) timeUnit.toDays(milliseconds);
        this.hours = (int) (timeUnit.toHours(milliseconds) % 24L);
        this.minutes = (int) (timeUnit.toMinutes(milliseconds) % 60L);
        this.seconds = (int) (timeUnit.toSeconds(milliseconds) % 60L);
    }

    public long getMilliseconds() {
        return milliseconds;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FrequencyUpdate that = (FrequencyUpdate) o;
        return days == that.days &&
                hours == that.hours &&
                minutes == that.minutes &&
                seconds == that.seconds &&
                milliseconds == that.milliseconds;
    }

    @Override
    public int hashCode() {
        return Objects.hash(days, hours, minutes, seconds, milliseconds);
    }
}
