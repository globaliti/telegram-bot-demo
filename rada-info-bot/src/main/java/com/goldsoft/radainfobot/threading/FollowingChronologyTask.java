package com.goldsoft.radainfobot.threading;

import com.goldsoft.radaOpenDataApi.model.enums.Convene;
import com.goldsoft.radainfobot.service.systemConfiguration.SystemConfigurationService;
import com.goldsoft.radainfobot.threading.voteHistory.NoUpdatesCounter;
import com.goldsoft.radainfobot.threading.voteHistory.VoteHistoryFollowingTask;
import com.goldsoft.radainfobot.threading.voteHistory.VoteHistoryTrigger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class FollowingChronologyTask implements Runnable {

    Logger logger = LoggerFactory.getLogger(FollowingChronologyTask.class);

    @Autowired
    FollowingThreadsManager followingThreadsManager;

    @Autowired
    BeanFactory beanFactory;

    @Autowired
    private SystemConfigurationService systemConfigurationService;

    FrequencyUpdate frequencyUpdate;

    Convene convene;

    public FollowingChronologyTask(FrequencyUpdate frequencyUpdate, Convene convene) {
        this.frequencyUpdate = frequencyUpdate;
        this.convene = convene;
    }

    @Override
    public void run() {
        logger.info("FollowingChronologyTask run in " + Thread.currentThread().getName());
        while (!Thread.currentThread().isInterrupted()) {
            List<Date> newChronologyDates = findNewDates();
            createVoteHistoryFollowing(newChronologyDates);
        }
    }

    protected void setFollowingThreadsManager(FollowingThreadsManager followingThreadsManager) {
        this.followingThreadsManager = followingThreadsManager;
    }

    List<Date> findNewDates() {
        List<Date> newChronologyDates = null;
        ChronologyUpdateChecker chronologyUpdateChecker = beanFactory.getBean(
                ChronologyUpdateChecker.class,
                frequencyUpdate,
                convene);
        FutureTask<List<Date>> futureTask = new FutureTask<>(chronologyUpdateChecker);
        Thread thread = new Thread(futureTask);
        thread.start();

        try {
            logger.info(Thread.currentThread().getName() + " is waiting for get dates");
            newChronologyDates = futureTask.get();
            logger.info(Thread.currentThread().getName() + " is got the dates :" + newChronologyDates);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return newChronologyDates;
    }

    void createVoteHistoryFollowing(List<Date> chronologyDates) {
        if (chronologyDates != null) { //TODO add logic, первым должжна быть ранняя дата, и только после завершения следования ранней даты, следующая
            for (Date newChronologyDate : chronologyDates) {//TODO to LocalDate
                NoUpdatesCounter noUpdatesCounter = new NoUpdatesCounter(4);
                VoteHistoryFollowingTask voteHistoryFollowingTask = beanFactory.getBean(
                        VoteHistoryFollowingTask.class,
                        newChronologyDate,
                        convene,
                        noUpdatesCounter);
                FrequencyUpdate chronologyFollowingFrequencyUpdate = new FrequencyUpdate(systemConfigurationService.getConfiguration().getVoteHistoryFrequencyUpdate());
                VoteHistoryTrigger voteHistoryTrigger = new VoteHistoryTrigger(noUpdatesCounter, chronologyFollowingFrequencyUpdate); //TODO Task's no update counter
                followingThreadsManager.runVoteHistoryFollowing(voteHistoryFollowingTask, voteHistoryTrigger);
            }
        }
    }
}