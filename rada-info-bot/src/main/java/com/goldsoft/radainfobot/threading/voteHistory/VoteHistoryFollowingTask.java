package com.goldsoft.radainfobot.threading.voteHistory;

import com.goldsoft.radaOpenDataApi.model.chronology.Chronology;
import com.goldsoft.radaOpenDataApi.model.chronology.Question;
import com.goldsoft.radaOpenDataApi.model.enums.Convene;
import com.goldsoft.radaOpenDataApi.service.ChronologyService;
import com.goldsoft.radainfobot.domain.Vote;
import com.goldsoft.radainfobot.domain.VoteQuestion;
import com.goldsoft.radainfobot.mapper.ChronologyQuestionMapper;
import com.goldsoft.radainfobot.service.TemplateMessageEngine;
import com.goldsoft.radainfobot.service.subscription.Subscribers;
import com.goldsoft.radainfobot.service.voteHistory.VoteHistoryService;
import com.goldsoft.radainfobot.service.voteQuestion.VoteQuestionService;
import com.goldsoft.radainfobot.threading.FrequencyUpdate;
import com.goldsoft.radainfobot.threading.telegram.TelegramSenderConcurrentModel;
import com.goldsoft.radainfobot.threading.telegram.TelegramSenderThread;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.task.TaskExecutor;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

//@Component
//@Scope(value = "prototype")
public class VoteHistoryFollowingTask implements Runnable {

    private Subscribers subscribers = Subscribers.VOTING_HISTORY_CHANGING;

    @Autowired
    private ChronologyService chronologyService;

    @Autowired
    private VoteHistoryService voteHistoryService;

    @Autowired
    ChronologyQuestionMapper chronologyQuestionMapper;

    @Autowired
    private VoteQuestionService voteQuestionService;

    @Qualifier("applicationTaskExecutor")
    @Autowired
    private TaskExecutor taskExecutor;

    @Autowired
    private TemplateMessageEngine templateMessageEngine;

    @Autowired
    private BeanFactory beanFactory;

    private Date lastChronologyPublicationDate;

    private LocalDate chronologyDate;

    private Convene convene;

    private final ThreadPoolExecutor threadPoolExecutor;

    private NoUpdatesCounter noUpdatesCounter;


    public VoteHistoryFollowingTask(LocalDate chronologyDate, Convene convene, NoUpdatesCounter noUpdatesCounter) {
        this.chronologyDate = chronologyDate;
        this.convene = convene;
        this.noUpdatesCounter = noUpdatesCounter;
        threadPoolExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
    }

    @Override
    @Transactional
    public void run() {
        if (chronologyService.isChronologyPublicationDateHasUpdate(lastChronologyPublicationDate, convene)) {
            List<VoteQuestion> newVoteQuestions = voteHistoryService.getNewVoteQuestionsFromChronology(chronologyDate, convene);
            if (!newVoteQuestions.isEmpty()) {
                voteHistoryService.saveVoteQuestionsToVoteHistory(chronologyDate, newVoteQuestions);
                sendMessagesAboutNewVotesToSubscribers(subscribers, newVoteQuestions);
                noUpdatesCounter.resetCounter();
            } else {
                noUpdatesCounter.increment();
            }
        } else {
            noUpdatesCounter.increment();
        }
    }

    public void sendMessagesAboutNewVotesToSubscribers(Subscribers subscribers, List<VoteQuestion> voteQuestions) {
        List<String> receivers = Arrays.asList(subscribers.getAllSubscribers());
        String message = templateMessageEngine.newVotesMessage(voteQuestions);
        List<String> messages = new LinkedList<>();
        messages.add(message);

        TelegramSenderConcurrentModel telegramSenderConcurrentModel = new TelegramSenderConcurrentModel(receivers, messages);
        int coreCount = Runtime.getRuntime().availableProcessors();

        for (int i = 0; i < coreCount; i++) {
            TelegramSenderThread telegramSenderThread = beanFactory.getBean(
                    TelegramSenderThread.class,
                    telegramSenderConcurrentModel);
            threadPoolExecutor.execute(telegramSenderThread);
        }
    }

    public void TEST_setChronologyPublicationDate(Date chronologyPublicationDate) {
        this.lastChronologyPublicationDate = chronologyPublicationDate;
    }

    public Date TEST_getChronologyPublicationDate() {
        return this.lastChronologyPublicationDate;
    }
}