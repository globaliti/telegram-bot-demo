package com.goldsoft.radainfobot.threading.voteHistory;

public class NoUpdatesCounter {
    private int noUpdatesAmount;

    private final int maximumAmountOfNoUpdates;

    public NoUpdatesCounter(int maximumAmountOfNoUpdates) {
        this.maximumAmountOfNoUpdates = maximumAmountOfNoUpdates;
    }

    synchronized int getNoUpdatesAmount() {
        return noUpdatesAmount;
    }

    synchronized void increment() {
        this.noUpdatesAmount++;
    }

    synchronized void resetCounter() {
        this.noUpdatesAmount = 0;
    }

    public int getMaximumAmountOfNoUpdates() {
        return maximumAmountOfNoUpdates;
    }
}
