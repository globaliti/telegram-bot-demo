package com.goldsoft.radainfobot.threading;

import com.goldsoft.radaOpenDataApi.model.enums.Convene;
import com.goldsoft.radainfobot.threading.telegram.TelegramSenderThread;
import com.goldsoft.radainfobot.threading.voteHistory.VoteHistoryFollowingTask;
import com.goldsoft.radainfobot.threading.voteHistory.VoteHistoryTrigger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.stereotype.Service;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

@Service
public class FollowingThreadsManagerImpl implements FollowingThreadsManager {

    private final ThreadPoolExecutor threadPool;

    private boolean shutdown; //TODO maybe stop following

    Logger logger = LoggerFactory.getLogger(FollowingThreadsManagerImpl.class);

    @Autowired
    private BeanFactory beanFactory;

    @Autowired
    TaskScheduler taskScheduler;

    public FollowingThreadsManagerImpl() {
        threadPool = (ThreadPoolExecutor) Executors.newFixedThreadPool(2 * Runtime.getRuntime().availableProcessors());
    }

    @Override
    public void followingChronologies(FrequencyUpdate frequencyUpdate, Convene convene) {
        FollowingChronologyTask followingChronologyTask = beanFactory.getBean(
                FollowingChronologyTask.class,
                frequencyUpdate,
                convene);
        threadPool.execute(followingChronologyTask);
    }

    @Override
    public void shutdown() {
        shutdown = true;
        threadPool.shutdown();
    }

    @Override
    public void runVoteHistoryFollowing(VoteHistoryFollowingTask voteHistoryFollowingTask, VoteHistoryTrigger voteHistoryTrigger) {
        taskScheduler.schedule(voteHistoryFollowingTask,voteHistoryTrigger);
        logger.info("VoteHistoryFollowingTask is added to scheduler service");
    }
}