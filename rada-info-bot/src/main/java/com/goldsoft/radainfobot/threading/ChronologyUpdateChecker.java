package com.goldsoft.radainfobot.threading;

import com.goldsoft.radaOpenDataApi.model.enums.Convene;
import com.goldsoft.radaOpenDataApi.service.ChronologyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Return the new chronology date
 */
public class ChronologyUpdateChecker implements Callable<List<Date>> {
    @Autowired
    private ChronologyService chronologyService;

    private FrequencyUpdate frequencyUpdate;

    private Convene convene;

    private List<Date> oldDates;

    public ChronologyUpdateChecker(FrequencyUpdate frequencyUpdate, Convene convene) {
        this.frequencyUpdate = frequencyUpdate;
        this.convene = convene;
    }

    @Override
    @Nullable
    public List<Date> call() throws Exception {
        oldDates = chronologyService.getChronologyDates(convene);
        return checking();
    }

    void setChronologyService(ChronologyService chronologyService) {
        this.chronologyService = chronologyService;
    }

    @Nullable
    List<Date> checking() {
        List<Date> newChronologyDates = null;

        while (!Thread.currentThread().isInterrupted()) {
            List<Date> lastDates = chronologyService.getChronologyDates(convene);
            if (lastDates.size() > oldDates.size()) {
                lastDates.removeAll(oldDates);
                newChronologyDates = new LinkedList<>(lastDates);
                break;
            }

            try {
                Thread.sleep(frequencyUpdate.getMilliseconds());
            } catch (InterruptedException e) {
                e.printStackTrace();
                Thread.currentThread().interrupt();
            }
        }

        return newChronologyDates;
    }

    void TEST_setOldDates(List<Date> dates) {
        this.oldDates = dates;
    }
}