package com.goldsoft.radainfobot.threading.voteHistory;

import com.goldsoft.radainfobot.threading.FrequencyUpdate;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.TriggerContext;

import java.util.Date;

public class VoteHistoryTrigger implements Trigger {//TODO change to more informative name

    private FrequencyUpdate frequencyUpdate;

    private NoUpdatesCounter noUpdatesCounter;

    public VoteHistoryTrigger(NoUpdatesCounter noUpdatesCounter, FrequencyUpdate frequencyUpdate) {
        this.frequencyUpdate = frequencyUpdate;
        this.noUpdatesCounter = noUpdatesCounter;
    }

    @Override
    public Date nextExecutionTime(TriggerContext triggerContext) {
        if (noUpdatesCounter.getNoUpdatesAmount() > noUpdatesCounter.getMaximumAmountOfNoUpdates()) {
            return null;
        }

        Date executionTime = null;
        if (triggerContext.lastScheduledExecutionTime() == null) {
            executionTime = new Date();
        } else {
            executionTime = new Date(triggerContext.lastScheduledExecutionTime().getTime() + frequencyUpdate.getMilliseconds());
        }
        return executionTime;
    }
}