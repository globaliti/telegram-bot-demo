package com.goldsoft.radainfobot.threading.telegram;

import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

public class TelegramSenderConcurrentModel {
    private ConcurrentLinkedQueue<String> recipients;

    private ConcurrentLinkedQueue<String> messagesForSend;

    public TelegramSenderConcurrentModel() {
    }

    public TelegramSenderConcurrentModel(List<String> recipients, List<String> messagesForSend) {
        this.recipients = new ConcurrentLinkedQueue<String>(recipients);
        this.messagesForSend = new ConcurrentLinkedQueue<String>(messagesForSend);
    }

    public ConcurrentLinkedQueue<String> getRecipients() {
        return recipients;
    }

    public void setRecipients(ConcurrentLinkedQueue<String> recipients) {
        this.recipients = recipients;
    }

    public ConcurrentLinkedQueue<String> getMessagesForSend() {
        return messagesForSend;
    }

    public void setMessagesForSend(ConcurrentLinkedQueue<String> messagesForSend) {
        this.messagesForSend = messagesForSend;
    }
}