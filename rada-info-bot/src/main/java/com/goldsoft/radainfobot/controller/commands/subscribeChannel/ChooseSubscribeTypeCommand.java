package com.goldsoft.radainfobot.controller.commands.subscribeChannel;

import com.goldsoft.radainfobot.client.BotMenu;
import com.goldsoft.radainfobot.client.TelegramBotClient;
import com.goldsoft.radainfobot.controller.commands.CommandState;
import com.goldsoft.radainfobot.controller.commands.DeepCommand;
import com.goldsoft.radainfobot.controller.builder.InlineKeyboardMarkupBuilder;
import com.goldsoft.radainfobot.domain.Subscriber;
import com.goldsoft.radainfobot.domain.enums.ChatType;
import com.goldsoft.radainfobot.domain.enums.SubscribeType;
import com.goldsoft.radainfobot.service.subscribe.SubscriberService;
import com.goldsoft.radainfobot.service.subscription.SubscriptionService;
import org.apache.shiro.session.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.AnswerCallbackQuery;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageReplyMarkup;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.*;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ChooseSubscribeTypeCommand extends DeepCommand {

    private final Logger logger = LoggerFactory.getLogger(ChooseSubscribeTypeCommand.class);

    private final String SUBSCRIBE_BUTTON_MESSAGE = "Подписаться";
    private final String CANCEL_BUTTON_MESSAGE = "Отмена";
    private final String SUCCESSFUL_SUBSCRIBE_MESSAGE = "Подписка успешно оформленна";
    private final String FAIL_SUBSCRIBE_MESSAGE = "Произошла ошибка при операции подписки, возможно данный чат уже " +
            "был добавлен, или у вас нету прав для совершенния данного действия.";

    private final String TURN_OFF_STATE = "[ВЫКЛ]";
    private final String TURN_ON_STATE = "[ВКЛ]";

    @Autowired
    BotMenu botMenu;

    private String channelId;

    private Subscriber subscriber = new Subscriber();

    private Set<SubscribeType> subscribeTypes = new HashSet<>();

    @Autowired
    private InlineKeyboardMarkupBuilder inlineKeyboardMarkupBuilder;

    @Autowired
    private TelegramBotClient telegramBotClient;

    @Autowired
    private SubscriberService subscriberService;

    @Autowired
    private SubscriptionService subscriptionService;

    @SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection"})
    public ChooseSubscribeTypeCommand(String channelId) {
        this.channelId = channelId;
        subscriber.setChatId(channelId);
    }

    @Override
    protected void onFinish(Update update, Optional<Session> optionalSession) {
        removeCommandFromSession(optionalSession);
    }

    @Override
    protected void onGetFeedback(Update update, Optional<Session> optionalSession) {
        CallbackQuery callbackQuery = update.getCallbackQuery();
        try {
            if (callbackQuery != null) {
                addSubscribeType(callbackQuery.getData());
                sendAnswerCallbackQuery(callbackQuery.getId(), null);
                editMessageReplyMarkup(
                        update.getCallbackQuery().getMessage().getChatId(),
                        callbackQuery.getMessage().getMessageId(),
                        inlineKeyboardMarkupBuilder.build());
            } else if (update.hasMessage()) {
                if (SUBSCRIBE_BUTTON_MESSAGE.equals(update.getMessage().getText())) {
                    createSubscriber();
                    sendSuccessfulSubscribeMessage(update.getMessage().getChatId());
                    botMenu.showMainMenu(update.getMessage().getChatId().toString(), true);
                    setCommandState(CommandState.FINISHED);
                    removeCommandFromSession(optionalSession);
                } else if (CANCEL_BUTTON_MESSAGE.equals(update.getMessage().getText())){
                    sendCancelSubscribeMessage(update.getMessage().getChatId());
                    botMenu.showMainMenu(update.getMessage().getChatId().toString(), true);
                    setCommandState(CommandState.REJECTED);
                    removeCommandFromSession(optionalSession);
                }
            }
        } catch (TelegramApiException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace(); //TODO exception
            try {
                Long chatId = null;
                if (callbackQuery != null) {
                    chatId = callbackQuery.getMessage().getChatId();
                }
                if (update.hasMessage()) {
                    chatId = update.getMessage().getChatId();
                }
                sendFailSubscribeMessage(chatId);
                botMenu.showMainMenu(chatId.toString(), true);
                removeCommandFromSession(optionalSession);
                setCommandState(CommandState.REJECTED);
            } catch (TelegramApiException ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    protected void onPerform(Update update, Optional<Session> optionalSession) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(update.getMessage().getChatId());

        for (SubscribeType subscribeType : SubscribeType.values()) {
            inlineKeyboardMarkupBuilder.addKeyboardButtonToRow(
                    TURN_OFF_STATE + " " + subscribeType.getRussianStringValue(),
                    subscribeType.name());
            inlineKeyboardMarkupBuilder.createNewRow();
        }

        InlineKeyboardMarkup inlineKeyboardMarkup = inlineKeyboardMarkupBuilder.build();
        sendMessage.setReplyMarkup(inlineKeyboardMarkup);
        sendMessage.setText("Доступные подписки:");
        try {
            telegramBotClient.execute(sendMessage);
            sendSubscribeButtons(update.getMessage().getChatId());
            setCommandState(CommandState.WAITING_FEEDBACK);
        } catch (TelegramApiException e) {
            e.printStackTrace();
            removeCommandFromSession(optionalSession);
            setCommandState(CommandState.REJECTED);
        }
    }

    private void editMessageReplyMarkup(
            Long chatId,
            Integer messageId,
            InlineKeyboardMarkup inlineKeyboardMarkup) throws TelegramApiException {

        EditMessageReplyMarkup editMessageReplyMarkup = new EditMessageReplyMarkup();
        editMessageReplyMarkup.setChatId(chatId);
        editMessageReplyMarkup.setMessageId(messageId);
        editMessageReplyMarkup.setReplyMarkup(inlineKeyboardMarkup);
        telegramBotClient.execute(editMessageReplyMarkup);
    }

    private void createSubscriber() {
        subscriber.setSubscribeTypes(subscribeTypes);
        subscriber.setChatType(ChatType.CHANNEL);//TODO only channels might be ?
        subscriberService.save(subscriber);
        subscriptionService.addSubscriberToPool(subscriber);
        logger.info("Created new subscriber :" + subscriber.toString());
    }

    private void addSubscribeType(String callbackData) {
        if (SubscribeType.FACTION_CHANGING.name().equals(callbackData)) {
            addSubscriptionIfNotContainOrDelete(SubscribeType.FACTION_CHANGING);
        }
        if (SubscribeType.MP_CHANGING.name().equals(callbackData)) {
            addSubscriptionIfNotContainOrDelete(SubscribeType.MP_CHANGING);
        }
        if (SubscribeType.VOTING_HISTORY_CHANGING.name().equals(callbackData)) {
            addSubscriptionIfNotContainOrDelete(SubscribeType.VOTING_HISTORY_CHANGING);
        }
    }

    private void addSubscriptionIfNotContainOrDelete(SubscribeType subscribeType) {
        if (subscribeTypes.contains(subscribeType)) {
            subscribeTypes.remove(subscribeType);
            inlineKeyboardMarkupBuilder.replaceButtonDataByCallbackData(
                    subscribeType.name(),
                    TURN_OFF_STATE + " " + subscribeType.getRussianStringValue(),
                    subscribeType.name());
        } else {
            subscribeTypes.add(subscribeType);
            inlineKeyboardMarkupBuilder.replaceButtonDataByCallbackData(
                    subscribeType.name(),
                    TURN_ON_STATE + subscribeType.getRussianStringValue(),
                    subscribeType.name());
        }
    }

    private void sendSubscribeButtons(Long chatId) throws TelegramApiException {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);
        sendMessage.setText("Выберете тип подписки и нажмите кнопку подписаться");
        sendMessage.disableNotification();
        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
        List<KeyboardRow> keyboard = new ArrayList<>();
        KeyboardRow row = new KeyboardRow();
        KeyboardButton subscribeButton = new KeyboardButton();
        subscribeButton.setText(SUBSCRIBE_BUTTON_MESSAGE);
        row.add(subscribeButton);

        KeyboardButton resetButton = new KeyboardButton();
        resetButton.setText(CANCEL_BUTTON_MESSAGE);
        row.add(resetButton);
        keyboard.add(row);
        keyboardMarkup.setKeyboard(keyboard);
        sendMessage.setReplyMarkup(keyboardMarkup);
        keyboardMarkup.setOneTimeKeyboard(true);
        keyboardMarkup.setResizeKeyboard(true);
        telegramBotClient.execute(sendMessage);
    }

    private void sendSuccessfulSubscribeMessage(Long chatId) throws TelegramApiException {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);
        sendMessage.setText(SUCCESSFUL_SUBSCRIBE_MESSAGE);
        telegramBotClient.execute(sendMessage);
    }

    private void sendFailSubscribeMessage(Long chatId) throws TelegramApiException {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);
        sendMessage.setText(FAIL_SUBSCRIBE_MESSAGE);
        telegramBotClient.execute(sendMessage);
    }

    private void sendAnswerCallbackQuery(String queryId, String text) throws TelegramApiException {
        AnswerCallbackQuery answerCallbackQuery = new AnswerCallbackQuery();
        answerCallbackQuery.setCallbackQueryId(queryId);
        if (text != null) {
            answerCallbackQuery.setText(text);
        }
        telegramBotClient.execute(answerCallbackQuery);
    }

    private void sendCancelSubscribeMessage(Long chatId) throws TelegramApiException {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setText("Операция отменена");
        sendMessage.setChatId(chatId);
        telegramBotClient.execute(sendMessage);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChooseSubscribeTypeCommand that = (ChooseSubscribeTypeCommand) o;
        return Objects.equals(channelId, that.channelId) &&
                Objects.equals(subscribeTypes, that.subscribeTypes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(channelId, subscribeTypes);
    }
}