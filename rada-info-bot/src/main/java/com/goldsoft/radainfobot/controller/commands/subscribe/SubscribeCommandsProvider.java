package com.goldsoft.radainfobot.controller.commands.subscribe;

import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.stereotype.Component;

@Component
public class SubscribeCommandsProvider {
    @Lookup
    public ChatChooseSubscribeTypeCommand getChatChooseSubscribeTypeCommand(String chatId) {
        return null;
    }

    @Lookup
    public SubscribeChatCommand getSubscribeChatCommand() {
        return null;
    }
}
