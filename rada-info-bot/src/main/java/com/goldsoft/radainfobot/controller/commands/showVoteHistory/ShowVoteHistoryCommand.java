package com.goldsoft.radainfobot.controller.commands.showVoteHistory;

import com.goldsoft.radaOpenDataApi.util.TimeUtil;
import com.goldsoft.radainfobot.client.TelegramBotClient;
import com.goldsoft.radainfobot.controller.commands.CommandState;
import com.goldsoft.radainfobot.controller.commands.DeepCommand;
import com.goldsoft.radainfobot.controller.builder.InlineKeyboardMarkupBuilder;
import com.goldsoft.radainfobot.domain.Vote;
import com.goldsoft.radainfobot.service.TemplateMessageEngine;
import com.goldsoft.radainfobot.service.voteHistory.VoteHistoryService;
import org.apache.shiro.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ShowVoteHistoryCommand extends DeepCommand {
    private String MESSAGE_TEXT = "Введите дату";

    @Autowired
    private TelegramBotClient telegramBotClient;

    @Autowired
    private VoteHistoryService voteHistoryService;

    @Autowired
    private TemplateMessageEngine templateMessageEngine;

    @Override
    protected void onFinish(Update update, Optional<Session> optionalSession) {
        removeCommandFromSession(optionalSession);
    }

    @Override
    protected void onGetFeedback(Update update, Optional<Session> optionalSession) {
        CallbackQuery callbackQuery = update.getCallbackQuery();
        if (callbackQuery != null) {
            LocalDate date = TimeUtil.getChronologyLocalDate(update.getCallbackQuery().getData());
            if (date != null) {
                try {
                    sendVotesResults(callbackQuery.getMessage().getChatId(), date);
                } catch (TelegramApiException e) {
                    removeCommandFromSession(optionalSession);
                    setCommandState(CommandState.REJECTED);
                    e.printStackTrace();
                }

                removeCommandFromSession(optionalSession);
                setCommandState(CommandState.FINISHED);
            }
        }
    }

    private void sendVotesResults(Long chatId, LocalDate voteHistoryDate) throws TelegramApiException {
        SendMessage sendMessage = new SendMessage();
        List<Vote> votes = voteHistoryService.findVotesFromVoteHistory(voteHistoryDate);
        List<String> messages = templateMessageEngine.makeMessagesAboutVotes(votes);
        for (String message : messages) {
            sendMessage.setText(message);
            sendMessage.setChatId(chatId);
            telegramBotClient.execute(sendMessage);
        }
    }

    @Override
    protected void onPerform(Update update, Optional<Session> optionalSession) {
        List<String> voteHistoriesDates = voteHistoryService.findAllDatesAndGetAsRadaStringFormat();
        InlineKeyboardMarkupBuilder keyboardMarkupBuilder = new InlineKeyboardMarkupBuilder();
        for (String date : voteHistoriesDates) {
            keyboardMarkupBuilder.addKeyboardButtonToRow(date, date);
            keyboardMarkupBuilder.createNewRow();
        }
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(update.getMessage().getChatId());
        sendMessage.setText(MESSAGE_TEXT);
        sendMessage.setReplyMarkup(keyboardMarkupBuilder.build());

        try {
            telegramBotClient.execute(sendMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
            setCommandState(CommandState.REJECTED);
            removeCommandFromSession(optionalSession);
        }
        setCommandToSession(optionalSession);
        setCommandState(CommandState.WAITING_FEEDBACK);
    }
}