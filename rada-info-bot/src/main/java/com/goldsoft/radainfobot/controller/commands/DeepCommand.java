package com.goldsoft.radainfobot.controller.commands;

import com.goldsoft.radainfobot.client.TelegramBotClient;
import org.apache.shiro.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.util.Optional;

public abstract class DeepCommand implements Command{
    private DeepCommand nextCommand;
    private CommandState commandState = CommandState.PERFORM;

    public static final String DEEP_COMMAND_ATTRIBUTE_NAME = "deepCommand";

    @Override
    public final void executeCommand(Update update, Optional<Session> optionalSession) {
        switch (commandState) {
            case PERFORM:
                onPerform(update, optionalSession);
                break;
            case WAITING_FEEDBACK:
                onGetFeedback(update, optionalSession);
                break;
            case FINISHED:
                onFinish(update, optionalSession);
                break;
            case REJECTED:
                removeCommandFromSession(optionalSession);
                break;
            default:
                throw new CommandDefineException("Command state is not define.");
        }
    }

    protected abstract void onFinish(Update update, Optional<Session> optionalSession);

    protected abstract void onGetFeedback(Update update, Optional<Session> optionalSession);

    protected abstract void onPerform(Update update, Optional<Session> optionalSession);

    public void setNextCommand(DeepCommand nextCommand) {
        this.nextCommand = nextCommand;
    }

    protected final void executeNextCommand(Update update, Optional<Session> optionalSession) throws CommandDefineException {
        if (this.nextCommand != null) {
            nextCommand.executeCommand(update, optionalSession);
        } else {
            throw new CommandDefineException("Next command is not defined.");
        }
    }

    protected void setCommandToSession(Optional<Session> optionalSession) {
        optionalSession.ifPresent(session -> session.setAttribute(DEEP_COMMAND_ATTRIBUTE_NAME, this));

    }

    protected void removeCommandFromSession(Optional<Session> optionalSession) {
        optionalSession.ifPresent(session -> session.removeAttribute(DEEP_COMMAND_ATTRIBUTE_NAME));
    }

    /**
     * Return message that must be send before executing command. This message may have instruction
     * for preparing data or notify about next action that user or bot gonna do.
     *
     * @return BotApiMethod or null, if this command have'nt required message.
     */
    public boolean isFinished() {
        return CommandState.FINISHED.equals(this.commandState);
    }

    public boolean isWaitingFeedback() {
        return CommandState.WAITING_FEEDBACK.equals(this.commandState);
    }

    public boolean isPerform() {
        return CommandState.PERFORM.equals(this.commandState);
    }

    public boolean isRejected() {
        return CommandState.REJECTED.equals(this.commandState);
    }

    protected void setCommandState(CommandState commandState) {
        this.commandState = commandState;
    }
}