package com.goldsoft.radainfobot.controller.commands.subscribeChannel;

import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.stereotype.Component;

@Component
public class SubscribeChannelCommandsProvider {
    @Lookup
    public ChooseSubscribeTypeCommand getChooseSubscribeType(String chatId) {
        return null;
    }

    @Lookup
    public SubscribeChannelCommand getSubscribeChannelCommand() {
        return null;
    }

}