package com.goldsoft.radainfobot.controller.commands.subscribe;

import com.goldsoft.radainfobot.client.TelegramBotClient;
import com.goldsoft.radainfobot.controller.commands.CommandState;
import com.goldsoft.radainfobot.controller.commands.DeepCommand;
import com.goldsoft.radainfobot.domain.Subscriber;
import com.goldsoft.radainfobot.service.subscribe.SubscriberService;
import org.apache.shiro.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.Optional;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class SubscribeChatCommand extends DeepCommand {

    private final String FAIL_SUBSCRIBE_MESSAGE = "Произошла ошибка при подписке";
    @Autowired
    private SubscriberService subscriberService;

    @Autowired
    private SubscribeCommandsProvider subscribeCommandsProvider;

    @Autowired
    private TelegramBotClient telegramBotClient;


    @Override
    protected void onFinish(Update update, Optional<Session> optionalSession) {
        executeNextCommand(update, optionalSession);
    }

    @Override
    protected void onGetFeedback(Update update, Optional<Session> optionalSession) {

    }

    @Override
    protected void onPerform(Update update, Optional<Session> optionalSession) {
        setCommandToSession(optionalSession);

        if (update.hasMessage()) {
            Long chatId = update.getMessage().getChatId();
            Subscriber subscriber = subscriberService.findByChanelName(chatId.toString());
            if (subscriber != null) {
                try {
                    sendFailSubscribeMessage(chatId, "Вы уже подписаны.");
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
                setCommandState(CommandState.REJECTED);
                removeCommandFromSession(optionalSession);
            } else {
                setNextCommand(subscribeCommandsProvider.getChatChooseSubscribeTypeCommand(chatId.toString()));
                setCommandState(CommandState.FINISHED);
                executeNextCommand(update, optionalSession);
            }
        }
    }

    private void sendFailSubscribeMessage(Long chatId, String description) throws TelegramApiException {
        SendMessage sendMessage = new SendMessage();
        String messageText = FAIL_SUBSCRIBE_MESSAGE;
        if (description != null) {
            messageText += "\nПричина : " + description;
        }
        sendMessage.setText(messageText);
        sendMessage.setChatId(chatId);
        telegramBotClient.execute(sendMessage);
    }
}
