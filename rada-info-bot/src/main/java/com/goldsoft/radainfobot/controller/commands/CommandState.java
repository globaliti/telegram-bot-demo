package com.goldsoft.radainfobot.controller.commands;

public enum CommandState {
    FINISHED, WAITING_FEEDBACK, PERFORM, REJECTED;
}
