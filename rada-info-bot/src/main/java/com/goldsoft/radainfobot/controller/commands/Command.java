package com.goldsoft.radainfobot.controller.commands;

import org.apache.shiro.session.Session;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.util.Optional;

public interface Command {
    void executeCommand(Update update, Optional<Session> optionalSession);
}
