package com.goldsoft.radainfobot.controller.commands.start;

import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.stereotype.Component;

@Component
public abstract class StartCommandProvider {

    @Lookup
    public abstract StartCommand startCommand();
}