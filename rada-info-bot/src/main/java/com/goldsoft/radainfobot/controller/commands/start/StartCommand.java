package com.goldsoft.radainfobot.controller.commands.start;

import com.goldsoft.radainfobot.client.BotMenu;
import com.goldsoft.radainfobot.client.TelegramBotClient;
import com.goldsoft.radainfobot.controller.commands.Command;
import org.apache.shiro.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.Optional;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class StartCommand implements Command {

    private final String GREETING_TEXT = "Добро пожаловать в телеграм бот для отображения данных о работе верховной рады украины";

    @Autowired
    TelegramBotClient telegramBotClient;

    @Autowired
    BotMenu botMenu;

    @Override
    public void executeCommand(Update update, Optional<Session> optionalSession) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(update.getMessage().getChatId());
        sendMessage.setText(GREETING_TEXT);
        try {
            telegramBotClient.execute(sendMessage);
            botMenu.showMainMenu(update.getMessage().getChatId().toString(),true);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
}