package com.goldsoft.radainfobot.controller.commands.help;

import com.goldsoft.radainfobot.client.TelegramBotClient;
import com.goldsoft.radainfobot.controller.commands.Command;
import org.apache.shiro.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.Optional;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class HelpCommand implements Command {

    private final String HELP_MESSAGE_TEXT = "Данный бот был разработан независемым разработчиком в 2019г. Контактные данные : email:sistimas001@gmail.com";

    @Autowired
    private TelegramBotClient telegramBotClient;

    @Override
    public void executeCommand(Update update, Optional<Session> optionalSession) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(update.getMessage().getChatId());
        sendMessage.setText(HELP_MESSAGE_TEXT);
        try {
            telegramBotClient.execute(sendMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
}