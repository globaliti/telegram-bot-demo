package com.goldsoft.radainfobot.controller.commands.showVoteHistory;

import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.stereotype.Component;

@Component
public abstract class ShowVoteHistoryCommandProvider {
    @Lookup
    abstract public ShowVoteHistoryCommand showVoteHistoryCommand();
}