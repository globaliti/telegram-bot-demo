package com.goldsoft.radainfobot.controller.commands.subscribeChannel;

import com.goldsoft.radainfobot.client.BotMenu;
import com.goldsoft.radainfobot.controller.TelegramBotController;
import com.goldsoft.radainfobot.client.TelegramBotClient;
import com.goldsoft.radainfobot.controller.commands.CommandState;
import com.goldsoft.radainfobot.controller.commands.DeepCommand;
import com.goldsoft.radainfobot.service.subscribe.SubscriberService;
import com.goldsoft.radainfobot.service.telegram.TelegramChatService;
import org.apache.shiro.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.Optional;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class SubscribeChannelCommand extends DeepCommand {

    @Autowired
    private SubscribeChannelCommandsProvider subscribeChannelCommandsProvider;

    @Autowired
    private TelegramChatService telegramChatService;

    @Autowired
    private TelegramBotClient telegramBotClient;

    @Autowired
    private SubscriberService subscriberService;

    public SubscribeChannelCommand() {
    }

    @Autowired
    BotMenu botMenu;

    @Override
    protected void onFinish(Update update, Optional<Session> optionalSession) {
        executeNextCommand(update, optionalSession);
    }

    @Override
    protected void onGetFeedback(Update update, Optional<Session> optionalSession) {
        if (!update.hasMessage() || !update.getMessage().hasText()) {
            return;
        }
        SendMessage sendMessage = new SendMessage();
        try {
            if (telegramChatService.hasChannelAdministrationRights(
                    update.getMessage().getText(),
                    update.getMessage().getChatId().toString())) {
                if (subscriberService.findByChanelName(update.getMessage().getText()) != null) {
                    sendMessage.setChatId(update.getMessage().getChatId());
                    sendMessage.setText("Данный канал уже подписан!");
                    telegramBotClient.execute(sendMessage);
                    setCommandState(CommandState.REJECTED);
                    removeCommandFromSession(optionalSession);
                } else {
                    sendMessage.setChatId(update.getMessage().getChatId());
                    sendMessage.setText("Доступ верифицирован!");
                    setNextCommand(subscribeChannelCommandsProvider.getChooseSubscribeType(update.getMessage().getText()));
                    setCommandState(CommandState.FINISHED);
                    telegramBotClient.execute(sendMessage);
                    executeNextCommand(update, optionalSession);
                }
            } else {
                sendMessage.setChatId(update.getMessage().getChatId());
                sendMessage.setText("Доступ не верифицирован! Убедитесь что вы являетесь администратором данного канала и предоставили доступ администратора данному боту, и выполните данную комманду снова.");
                removeCommandFromSession(optionalSession);
                setCommandState(CommandState.REJECTED);
                removeCommandFromSession(optionalSession);
                telegramBotClient.execute(sendMessage);
            }
        } catch (TelegramApiException e) {
            e.printStackTrace();
            setCommandState(CommandState.REJECTED);
            removeCommandFromSession(optionalSession);
        }
    }

    @Override
    protected void onPerform(Update update, Optional<Session> optionalSession) {
        setCommandToSession(optionalSession);

        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(update.getMessage().getChatId());
        sendMessage.setText("Добавте данного бота в администраторы канала, и введите ссылку на телеграм канал, внимание," +
                " для выполнения данной функции вы" +
                " должны состоять в группе администраторов данного канала");

        try {
            telegramBotClient.execute(sendMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
            setCommandState(CommandState.REJECTED);
            removeCommandFromSession(optionalSession);
        }
        setCommandState(CommandState.WAITING_FEEDBACK);
    }
}