package com.goldsoft.radainfobot.controller.builder;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.util.LinkedList;
import java.util.List;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class InlineKeyboardMarkupBuilder {
    private List<List<InlineKeyboardButton>> inlineKeyboard = new LinkedList<>();
    private List<InlineKeyboardButton> keyboardRow = new LinkedList<>();

    public InlineKeyboardMarkupBuilder addKeyboardButtonToRow(String text, String callbackData) {
        if (checkExistButtonByCallbackData(callbackData)) {
            replaceButtonDataByCallbackData(callbackData, text, callbackData);
            return this;
        }

        InlineKeyboardButton inlineKeyboardButton = new InlineKeyboardButton();
        inlineKeyboardButton.setText(text);
        inlineKeyboardButton.setCallbackData(callbackData);
        keyboardRow.add(inlineKeyboardButton);
        return this;
    }

    public InlineKeyboardMarkup build() {
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        if (!keyboardRow.isEmpty()) {
            inlineKeyboard.add(keyboardRow);
        }

        if (!inlineKeyboard.isEmpty()) {
            inlineKeyboardMarkup.setKeyboard(inlineKeyboard);
        }

        return inlineKeyboardMarkup;
    }

    public InlineKeyboardMarkupBuilder createNewRow() {
        if (!keyboardRow.isEmpty()) {
            this.inlineKeyboard.add(keyboardRow);
            keyboardRow = new LinkedList<>();
        }
        return this;
    }

    public void replaceButtonDataByCallbackData(String callbackData,
                                                String newButtonText,
                                                String newButtonCallbackData) {
        for (List<InlineKeyboardButton> inlineKeyboardButtons : inlineKeyboard) {
            replaceButtonDataInRowByCallbackData(
                    callbackData,
                    newButtonText,
                    newButtonCallbackData,
                    inlineKeyboardButtons);
        }

        if (!keyboardRow.isEmpty()) {
            replaceButtonDataInRowByCallbackData(
                    callbackData,
                    newButtonText,
                    newButtonCallbackData,
                    keyboardRow);
        }


    }

    private void replaceButtonDataInRowByCallbackData(String callbackData,
                                                      String newButtonText,
                                                      String newButtonCallbackData,
                                                      List<InlineKeyboardButton> keyboardRow) {
        for (InlineKeyboardButton inlineKeyboardButton : keyboardRow) {
            if (callbackData.equals(inlineKeyboardButton.getCallbackData())) {
                inlineKeyboardButton.setText(newButtonText);
                inlineKeyboardButton.setCallbackData(newButtonCallbackData);
            }
        }
    }

    private boolean checkExistButtonByCallbackData(String callbackData) {
        boolean result = false;
        for (InlineKeyboardButton inlineKeyboardButton : keyboardRow) {
            if (callbackData.equals(inlineKeyboardButton.getCallbackData())) {
                result = true;
                break;
            }
        }
        return result;
    }
}