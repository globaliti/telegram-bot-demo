package com.goldsoft.radainfobot.controller;

import com.goldsoft.radainfobot.client.TelegramBotClient;
import com.goldsoft.radainfobot.domain.enums.ChatType;
import com.goldsoft.radainfobot.service.telegram.MessageRequestParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.groupadministration.GetChat;
import org.telegram.telegrambots.meta.api.methods.groupadministration.GetChatAdministrators;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.ChatMember;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.List;

@Component
public class TelegramBotControllerImpl implements TelegramBotController {

    @Autowired
    private TelegramBotClient telegramBotClient;

    @Override
    public void sendMessage(MessageRequestParam messageRequestParam) throws TelegramApiException {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setParseMode(messageRequestParam.getParseMode());
        sendMessage.setText(messageRequestParam.getText());
        sendMessage.setChatId(messageRequestParam.getChatId());
        sendMessage.setReplyToMessageId(messageRequestParam.getReplyToMessageId());

        telegramBotClient.execute(sendMessage);

    }

    @Override
    public ChatType getChatType(String chatId) {
        ChatType chatType = null;
        GetChat getChat = new GetChat();
        getChat.setChatId(chatId);
        Chat chat = null;
        try {
            chat = telegramBotClient.execute(getChat);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }

        if (chat.isChannelChat() != null && chat.isChannelChat()) {
            chatType = ChatType.CHANNEL;
        }
        if (chat.isGroupChat() != null && chat.isGroupChat()) {
            chatType = ChatType.GROUP;
        }
        if (chat.isSuperGroupChat() != null && chat.isSuperGroupChat()) {
            chatType = ChatType.SUPER_GROUP;
        }
        if (chat.isUserChat() != null && chat.isUserChat()) {
            chatType = ChatType.PRIVATE;
        }

        return chatType;
    }

    @Override
    public List<ChatMember> getChatAdministrators(String chatId) throws TelegramApiException {
        GetChatAdministrators getChatAdministrators = new GetChatAdministrators();
        getChatAdministrators.setChatId(chatId);
        return telegramBotClient.execute(getChatAdministrators);
    }

}