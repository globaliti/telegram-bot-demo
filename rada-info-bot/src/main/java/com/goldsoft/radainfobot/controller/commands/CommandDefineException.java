package com.goldsoft.radainfobot.controller.commands;

public class CommandDefineException extends RuntimeException {
    public CommandDefineException() {
        super();
    }

    public CommandDefineException(String message) {
        super(message);
    }

    public CommandDefineException(String message, Throwable cause) {
        super(message, cause);
    }

    public CommandDefineException(Throwable cause) {
        super(cause);
    }

    protected CommandDefineException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
