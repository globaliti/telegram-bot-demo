package com.goldsoft.radainfobot.controller;

import com.goldsoft.radainfobot.domain.enums.ChatType;
import com.goldsoft.radainfobot.service.telegram.MessageRequestParam;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.objects.ChatMember;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.List;

public interface TelegramBotController {
    void sendMessage(MessageRequestParam messageRequestParam) throws TelegramApiException;
    ChatType getChatType(String chatId);
    List<ChatMember> getChatAdministrators(String chatId) throws TelegramApiException;
}