package com.goldsoft.radainfobot.controller.commands.help;

import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.stereotype.Component;

@Component
public abstract class HelpCommandProvider {

    @Lookup
    public abstract HelpCommand helpCommand();
}