package com.goldsoft.radainfobot.repository;

import com.goldsoft.radainfobot.domain.VoteHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Repository
public interface VoteHistoryRepository extends JpaRepository<VoteHistory,Long> {
    VoteHistory findByDate(LocalDate date);

    @Query("SELECT v.date FROM VoteHistory v")
    List<LocalDate> findAllDates();
}