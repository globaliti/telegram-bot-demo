package com.goldsoft.radainfobot.repository;

import com.goldsoft.radainfobot.domain.VoteQuestion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VoteQuestionRepository extends JpaRepository<VoteQuestion, Long> {
}
