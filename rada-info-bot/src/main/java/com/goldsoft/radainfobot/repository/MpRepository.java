package com.goldsoft.radainfobot.repository;

import com.goldsoft.radainfobot.domain.ParliamentMember;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MpRepository extends JpaRepository<ParliamentMember,Long> {
}