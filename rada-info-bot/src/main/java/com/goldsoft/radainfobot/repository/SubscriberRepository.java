package com.goldsoft.radainfobot.repository;

import com.goldsoft.radainfobot.domain.Subscriber;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SubscriberRepository extends JpaRepository<Subscriber, Long> {
    Subscriber findByChatId(String chatId);
}