package com.goldsoft.radainfobot.client;

import com.goldsoft.radainfobot.controller.commands.help.HelpCommandProvider;
import com.goldsoft.radainfobot.controller.commands.showVoteHistory.ShowVoteHistoryCommand;
import com.goldsoft.radainfobot.controller.commands.showVoteHistory.ShowVoteHistoryCommandProvider;
import com.goldsoft.radainfobot.controller.commands.start.StartCommandProvider;
import com.goldsoft.radainfobot.controller.commands.subscribe.SubscribeChatCommand;
import com.goldsoft.radainfobot.controller.commands.subscribe.SubscribeCommandsProvider;
import com.goldsoft.radainfobot.controller.commands.subscribeChannel.SubscribeChannelCommand;
import com.goldsoft.radainfobot.controller.commands.subscribeChannel.SubscribeChannelCommandsProvider;
import com.goldsoft.radainfobot.domain.enums.SubscribeType;
import org.apache.shiro.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.util.Optional;
import java.util.Set;

@Component
public class DefaultCommandHandler implements CommandHandler {

    @Autowired
    private SubscribeChannelCommandsProvider subscribeChannelCommandsProvider;

    @Autowired
    private SubscribeCommandsProvider subscribeCommandsProvider;

    @Autowired
    private ShowVoteHistoryCommandProvider showVoteHistoryCommandProvider;

    @Autowired
    private HelpCommandProvider helpCommandProvider;

    @Autowired
    private StartCommandProvider startCommandProvider;

    @Override
    public void subscribe(Update update, Optional<Session> optional) {
        SubscribeChatCommand subscribeChatCommand = subscribeCommandsProvider.getSubscribeChatCommand();
        subscribeChatCommand.executeCommand(update, optional);
    }

    @Override
    public void subscribeChannel(Update update, Optional<Session> optional) {
        SubscribeChannelCommand subscribeChannelCommand = subscribeChannelCommandsProvider.getSubscribeChannelCommand();
        subscribeChannelCommand.executeCommand(update, optional);
    }

    @Override
    public void showVoteHistory(Update update, Optional<Session> optional) {
        ShowVoteHistoryCommand showVoteHistoryCommand = showVoteHistoryCommandProvider.showVoteHistoryCommand();
        showVoteHistoryCommand.executeCommand(update, optional);
    }

    @Override
    public void help(Update update, Optional<Session> optional) {
        helpCommandProvider.helpCommand().executeCommand(update, optional);
    }

    @Override
    public void start(Update update, Optional<Session> optional) {
        startCommandProvider.startCommand().executeCommand(update, optional);
    }
}