package com.goldsoft.radainfobot.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;

@Component
public class BotMenuImpl implements BotMenu {

    @Autowired
    TelegramBotClient telegramBotClient;

    @Override
    public void showMainMenu(String chatId, boolean disableNotification) throws TelegramApiException {
        SendMessage sendMessage = new SendMessage();

        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
        List<KeyboardRow> keyboard = new ArrayList<>();
        KeyboardRow row = new KeyboardRow();

        row.add(BotKeyWords.SUBSCRIBE);
        row.add(BotKeyWords.SUBSCRIBE_CHANNEL);
        keyboard.add(row);

        row = new KeyboardRow();
        row.add(BotKeyWords.SHOW_VOTE_HISTORY);
        row.add(BotKeyWords.HELP);
        keyboard.add(row);

        keyboardMarkup.setKeyboard(keyboard);
        keyboardMarkup.setResizeKeyboard(true);

        sendMessage.setText("Главное меню");
        sendMessage.setChatId(chatId);
        sendMessage.setReplyMarkup(keyboardMarkup);
        if (disableNotification) {
            sendMessage.disableNotification();
        }

        telegramBotClient.execute(sendMessage);
    }
}