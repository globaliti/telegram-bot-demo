package com.goldsoft.radainfobot.client;

public final class BotCommands {
    public static final String SUBSCRIBE = "/subscribe";
    public static final String SUBSCRIBE_CHANNEL = "/subscribechannel";
    public static final String SHOW_VOTE_HISTORY = "/showvotehistory";
    public static final String HELP = "/help";
    public static final String START = "/start";

    private BotCommands() {
    }
}
