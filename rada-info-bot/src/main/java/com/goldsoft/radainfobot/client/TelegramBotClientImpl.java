package com.goldsoft.radainfobot.client;

import com.goldsoft.radainfobot.controller.commands.DeepCommand;
import org.apache.shiro.session.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.groupadministration.GetChatAdministrators;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.ChatMember;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.List;
import java.util.Optional;

@Component
public class TelegramBotClientImpl extends TelegramBotClient {

    @Value("${bootToken}")
    private String bootToken;
    @Value("${botUserName}")
    private String botUserName;

    private Logger logger = LoggerFactory.getLogger(TelegramBotClientImpl.class);

    private CommandHandler commandHandler;

    @Autowired
    public TelegramBotClientImpl(CommandHandler commandHandler) {
        this.commandHandler = commandHandler;
        logger.info("TelegramBotSessionClient was created !");
    }

    @Override
    public void onUpdateReceived(Update update, Optional<Session> optionalSession) {
        logger.info("TelegramBotSessionClient receive the update !");

        if (hasDeepCommandInSession(optionalSession)) {
            DeepCommand deepCommand = getDeepCommandFromSession(optionalSession);
            deepCommand.executeCommand(update, optionalSession);
        } else {
            if (update.getMessage().isCommand()) {
                executeCommand(update, optionalSession);
            } else {
                if (update.hasMessage() && update.getMessage().hasText()) {
                    executeCommandByKeyWord(update, optionalSession);
                }

            }
        }
    }

    @Override
    public String getBotToken() {
        return bootToken;
    }

    @Override
    public String getBotUsername() {
        return botUserName;
    }

    private void executeCommand(Update update, Optional<Session> optionalSession) {
        String command = update.getMessage().getText();
        switch (command) {
            case BotCommands.SUBSCRIBE: {
                commandHandler.subscribe(update, optionalSession);
                break;
            }
            case BotCommands.SUBSCRIBE_CHANNEL: {
                commandHandler.subscribeChannel(update, optionalSession);
                break;
            }
            case BotCommands.SHOW_VOTE_HISTORY: {
                commandHandler.showVoteHistory(update, optionalSession);
                break;
            }
            case BotCommands.HELP: {
                commandHandler.help(update, optionalSession);
                break;
            }
            case BotCommands.START: {
                commandHandler.start(update, optionalSession);
                break;
            }
            default:
                break;
        }
    }

    private void executeCommandByKeyWord(Update update, Optional<Session> optionalSession) {
        String command = update.getMessage().getText();
        switch (command) {
            case BotKeyWords.SUBSCRIBE: {
                commandHandler.subscribe(update, optionalSession);
                break;
            }
            case BotKeyWords.SUBSCRIBE_CHANNEL: {
                commandHandler.subscribeChannel(update, optionalSession);
                break;
            }
            case BotKeyWords.SHOW_VOTE_HISTORY: {
                commandHandler.showVoteHistory(update, optionalSession);
                break;
            }
            case BotKeyWords.HELP: {
                commandHandler.help(update, optionalSession);
                break;
            }
            default:
                break;
        }
    }

    private DeepCommand getDeepCommandFromSession(Optional<Session> optionalSession) {
        DeepCommand result = null;
        if (optionalSession.isPresent() && optionalSession.get().getAttribute(DeepCommand.DEEP_COMMAND_ATTRIBUTE_NAME) != null) {
            result = (DeepCommand) optionalSession.get().getAttribute(DeepCommand.DEEP_COMMAND_ATTRIBUTE_NAME);
        }
        return result;
    }

    private boolean hasDeepCommandInSession(Optional<Session> optionalSession) {
        return getDeepCommandFromSession(optionalSession) != null;
    }
}