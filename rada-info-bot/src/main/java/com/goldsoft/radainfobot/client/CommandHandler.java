package com.goldsoft.radainfobot.client;

import com.goldsoft.radainfobot.domain.enums.SubscribeType;
import org.apache.shiro.session.Session;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.util.Optional;
import java.util.Set;

public interface CommandHandler {

    void subscribe(Update update, Optional<Session> optional);

    void subscribeChannel(Update update, Optional<Session> optional);

    void showVoteHistory(Update update, Optional<Session> optional);

    void help(Update update, Optional<Session> optional);

    void start(Update update, Optional<Session> optional);
}