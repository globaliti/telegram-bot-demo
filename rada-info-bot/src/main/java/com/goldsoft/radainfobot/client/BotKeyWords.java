package com.goldsoft.radainfobot.client;

public final class BotKeyWords {
    public static final String SUBSCRIBE = "Подписаться";
    public static final String SUBSCRIBE_CHANNEL = "Подписать канал";
    public static final String SHOW_VOTE_HISTORY = "Хронология голосования";
    public static final String HELP = "Помощь";

    private BotKeyWords() {
    }
}