package com.goldsoft.radainfobot.client;

import org.telegram.telegrambots.meta.exceptions.TelegramApiException;


public interface BotMenu {
    void showMainMenu(String chatId, boolean disableNotification) throws TelegramApiException;
}
