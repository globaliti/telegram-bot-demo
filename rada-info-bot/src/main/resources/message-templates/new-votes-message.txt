Результати голосовань:
 [# th:if="${questionId}!=null"]
     Номер питання : [(${questionId})]
  [/]

[# th:each="vote : ${votes}"]
[(${vote.name})]
За : [(${vote.voteResult.votedFor})]
Против :[(${vote.voteResult.votedAgainst})]
Утрималось :[(${vote.voteResult.abstain})]
Не голосувало :[(${vote.voteResult.notVoted})]
[/]