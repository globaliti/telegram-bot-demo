package com.goldsoft.radainfobot.threading;

import org.junit.Assert;
import org.junit.Test;

public class FrequencyUpdateTest {

    @Test
    public void checkMillisecondAmountTest() {
        FrequencyUpdate frequencyUpdateWithZeroValues = new FrequencyUpdate(0, 0, 0, 0);
        FrequencyUpdate frequencyUpdateWithSeconds = new FrequencyUpdate(0, 0, 0, 1);
        FrequencyUpdate frequencyUpdateWithMinutes = new FrequencyUpdate(0, 0, 1, 0);
        FrequencyUpdate frequencyUpdateWithHours = new FrequencyUpdate(0, 1, 0, 0);
        FrequencyUpdate frequencyUpdateWithDays = new FrequencyUpdate(1, 0, 0, 0);

        Assert.assertEquals(0, frequencyUpdateWithZeroValues.getMilliseconds());
        Assert.assertEquals(1000, frequencyUpdateWithSeconds.getMilliseconds());
        Assert.assertEquals(60000, frequencyUpdateWithMinutes.getMilliseconds());
        Assert.assertEquals(3600000, frequencyUpdateWithHours.getMilliseconds());
        Assert.assertEquals(86400000, frequencyUpdateWithDays.getMilliseconds());

        FrequencyUpdate frequencyUpdateWithZeroMilliseconds = new FrequencyUpdate(0L);
        FrequencyUpdate frequencyUpdateWithSecondsInMilliseconds = new FrequencyUpdate(1000L);
        FrequencyUpdate frequencyUpdateWithMinutesInMilliseconds = new FrequencyUpdate(60000L);
        FrequencyUpdate frequencyUpdateWithHoursInMilliseconds = new FrequencyUpdate(3600000L);
        FrequencyUpdate frequencyUpdateWithDaysInMilliseconds = new FrequencyUpdate(86400000L);

        Assert.assertEquals(0, frequencyUpdateWithZeroMilliseconds.getMilliseconds());
        Assert.assertEquals(1000, frequencyUpdateWithSecondsInMilliseconds.getMilliseconds());
        Assert.assertEquals(60000, frequencyUpdateWithMinutesInMilliseconds.getMilliseconds());
        Assert.assertEquals(3600000, frequencyUpdateWithHoursInMilliseconds.getMilliseconds());
        Assert.assertEquals(86400000, frequencyUpdateWithDaysInMilliseconds.getMilliseconds());

        Assert.assertEquals(frequencyUpdateWithZeroValues, frequencyUpdateWithZeroMilliseconds);
        Assert.assertEquals(frequencyUpdateWithSeconds, frequencyUpdateWithSecondsInMilliseconds);
        Assert.assertEquals(frequencyUpdateWithMinutes, frequencyUpdateWithMinutesInMilliseconds);
        Assert.assertEquals(frequencyUpdateWithHours, frequencyUpdateWithHoursInMilliseconds);
        Assert.assertEquals(frequencyUpdateWithDays, frequencyUpdateWithDaysInMilliseconds);
    }
}