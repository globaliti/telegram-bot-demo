package com.goldsoft.radainfobot.builder.telegram;

import org.mockito.Mockito;
import org.telegram.telegrambots.meta.api.objects.Message;

import static org.mockito.BDDMockito.given;

public class MessageMockBuilder {
    private String messageText;
    private boolean command;
    private Long chatId;
    private Integer messageId;

    public static MessageMockBuilder message() {
        return new MessageMockBuilder();
    }

    public MessageMockBuilder setMessageText(String messageText) {
        this.messageText = messageText;
        return this;
    }

    public MessageMockBuilder setCommand(boolean command) {
        this.command = command;
        return this;
    }

    public MessageMockBuilder setMessageId(Integer messageId) {
        this.messageId = messageId;
        return this;
    }

    public MessageMockBuilder setChatId(Long chatId) {
        this.chatId = chatId;
        return this;
    }

    public Message build() {
        Message message = Mockito.mock(Message.class);
        given(message.getChatId()).willReturn(chatId);
        if (messageText != null) {
            given(message.getText()).willReturn(messageText);
            given(message.hasText()).willReturn(true);
        }
        if (command) {
            given(message.isCommand()).willReturn(command);
        }
        if (messageId != null) {
            given(message.getMessageId()).willReturn(messageId);
        }
        return message;
    }
}