package com.goldsoft.radainfobot.builder.telegram;

import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Message;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

public class CallbackQueryMockBuilder {
    private String callbackData;
    private Message message;

    public static CallbackQueryMockBuilder callbackQuery() {
        return new CallbackQueryMockBuilder();
    }

    public CallbackQueryMockBuilder setMessage(Message message) {
        this.message = message;
        return this;
    }

    public CallbackQueryMockBuilder setCallbackData(String callbackData) {
        this.callbackData = callbackData;
        return this;
    }

    public CallbackQuery build() {
        CallbackQuery callbackQuery = mock(CallbackQuery.class);
        if (callbackData != null) {
            given(callbackQuery.getData()).willReturn(callbackData);
        }
        if (message != null) {
            given(callbackQuery.getMessage()).willReturn(message);
        }
        return callbackQuery;
    }
}