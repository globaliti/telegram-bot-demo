package com.goldsoft.radainfobot.builder.telegram;

import com.goldsoft.radainfobot.controller.commands.DeepCommand;
import org.apache.shiro.session.Session;

import java.util.Optional;

import static org.mockito.Mockito.mock;

public class SessionMockBuilder {
    private DeepCommand deepCommand;

    public SessionMockBuilder setDeepCommandInAttribute(DeepCommand deepCommand) {
        this.deepCommand = deepCommand;
        return this;
    }

    public static SessionMockBuilder session(){
        return new SessionMockBuilder();
    }

    public Session build(){
        Session session = mock(Session.class);
        if(deepCommand!=null){}
        return session;
    }
    public Optional<Session> buildOptional(){
        return Optional.of(build());
    }
}
