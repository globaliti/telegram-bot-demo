package com.goldsoft.radainfobot.builder;

import org.apache.shiro.session.Session;
import org.mockito.Mockito;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

public class TelegramMockModelBuilder {
    private static final long DEFAULT_CHAT_ID = 10L;
    private Long chatId = DEFAULT_CHAT_ID;
//    private String messageText;
    private String callbackData;
    private Message message;

    public TelegramMockModelBuilder setCallbackData(String callbackData) {
        this.callbackData = callbackData;
        return this;
    }

    public TelegramMockModelBuilder setMessage(MessageBuilder messageBuilder) {
        this.message = messageBuilder.build();
        return this;
    }

    //    public TelegramMockModelBuilder setMessageText(String messageText) {
//        this.messageText = messageText;
//        return this;
//    }
//
    public TelegramMockModelBuilder setChatId(Long chatId) {
        this.chatId = chatId;
        return this;
    }

//    private Message mockMessage() {
//        Message message = Mockito.mock(Message.class);
//        given(message.getChatId()).willReturn(chatId);
//        given(message.getText()).willReturn(messageText);
//        given(message.hasText()).willReturn(true);
//        return message;
//    }

//    public Update getMockedUpdate() {
//        Update update = Mockito.mock(Update.class);
//
//
//        return update;
//    }

    public Update buildUpdate() {
        Update update = Mockito.mock(Update.class);
        if (message != null) {
            given(update.hasMessage()).willReturn(true);
//            Message message = mockMessage();
            given(update.getMessage()).willReturn(message);
        }
        if (callbackData != null) {
            CallbackQuery callbackQuery = mock(CallbackQuery.class);
            given(callbackQuery.getData()).willReturn(callbackData);
            given(update.getCallbackQuery()).willReturn(callbackQuery);
        }

        return update;

    }

    public Optional<Session> getMockedSession() {
        Session session = Mockito.mock(Session.class);
        Optional<Session> optionalSession = Optional.of(session);
        return optionalSession;
    }

    public Long getChatId() {
        return chatId;
    }
//
//    public String getMessageText() {
//        return messageText;
//    }

   public static class MessageBuilder {
        private String messageText;
        private boolean command;
        private Long chatId = DEFAULT_CHAT_ID;

        public String getMessageText() {
            return messageText;
        }

        public MessageBuilder setMessageText(String messageText) {
            this.messageText = messageText;
            return this;
        }

        public boolean isCommand() {
            return command;
        }

        public MessageBuilder setCommand(boolean command) {
            this.command = command;
            return this;
        }

        public Long getChatId() {
            return chatId;
        }

        public MessageBuilder setChatId(Long chatId) {
            this.chatId = chatId;
            return this;
        }

        public Message build(){
            Message message = Mockito.mock(Message.class);
            given(message.getChatId()).willReturn(chatId);
            given(message.getText()).willReturn(messageText);
            given(message.hasText()).willReturn(true);
            given(message.isCommand()).willReturn(command);
            return message;
        }
    }
}