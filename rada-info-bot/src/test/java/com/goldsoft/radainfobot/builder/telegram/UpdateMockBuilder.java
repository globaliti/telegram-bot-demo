package com.goldsoft.radainfobot.builder.telegram;

import org.mockito.Mockito;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

public class UpdateMockBuilder {
    private Message message;
    private CallbackQuery callbackQuery;
    public static UpdateMockBuilder update(){
       return new UpdateMockBuilder();
    }

    public UpdateMockBuilder setMessage(Message message) {
        this.message = message;
        return this;
    }

    public UpdateMockBuilder setCallbackQuery(CallbackQuery callbackQuery) {
        this.callbackQuery = callbackQuery;
        return this;
    }

    public Update build(){
        Update update = Mockito.mock(Update.class);
        if (message != null) {
            given(update.hasMessage()).willReturn(true);
            given(update.getMessage()).willReturn(message);
        }
        if (callbackQuery != null) {
            given(update.getCallbackQuery()).willReturn(callbackQuery);
        }
        return update;
    }
}