package com.goldsoft.radainfobot.controller;

import com.goldsoft.radainfobot.client.BotMenu;
import com.goldsoft.radainfobot.service.subscription.SubscriptionService;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;

@TestConfiguration
@ComponentScan(basePackages = "com.goldsoft.radainfobot.controller")
public class ControllerLayerTestConfiguration {
    @MockBean
    BotMenu botMenu;
}