package com.goldsoft.radainfobot.controller.commands.subscribeChannel;

import com.goldsoft.radainfobot.builder.telegram.MessageMockBuilder;
import com.goldsoft.radainfobot.client.TelegramBotClient;
import com.goldsoft.radainfobot.controller.ControllerLayerTestConfiguration;
import com.goldsoft.radainfobot.controller.commands.CommandState;
import com.goldsoft.radainfobot.controller.commands.DeepCommandReflectionSetter;
import com.goldsoft.radainfobot.builder.TelegramMockModelBuilder;
import com.goldsoft.radainfobot.domain.Subscriber;
import com.goldsoft.radainfobot.domain.enums.ChatType;
import com.goldsoft.radainfobot.domain.enums.SubscribeType;
import com.goldsoft.radainfobot.service.subscribe.SubscriberService;
import com.goldsoft.radainfobot.service.subscription.SubscriptionService;
import org.apache.shiro.session.Session;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static com.goldsoft.radainfobot.builder.telegram.CallbackQueryMockBuilder.callbackQuery;
import static com.goldsoft.radainfobot.builder.telegram.MessageMockBuilder.message;
import static com.goldsoft.radainfobot.builder.telegram.SessionMockBuilder.session;
import static com.goldsoft.radainfobot.builder.telegram.UpdateMockBuilder.update;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ControllerLayerTestConfiguration.class)
public class ChatChooseSubscribeTypeCommandTest {

    @MockBean
    private TelegramBotClient telegramBotClient;

    @MockBean
    SubscriptionService subscriptionService;

    @MockBean
    private SubscriberService subscriberService;

    private TelegramMockModelBuilder telegramMockModelBuilder;

    @Before
    public void setUp() throws Exception {
        telegramMockModelBuilder = new TelegramMockModelBuilder();
    }

    @Autowired
    private SubscribeChannelCommandsProvider subscribeChannelCommandsProvider;

    @Test
    public void onGetFeedback_passTwoSubscribeTypesAndSubscribeCommand_saveSubscriber() throws NoSuchFieldException {
        String channelId = "@testChannel";
        Long chatId = 10L;
        ChooseSubscribeTypeCommand chooseSubscribeTypeCommand = subscribeChannelCommandsProvider.getChooseSubscribeType(
                channelId);
        DeepCommandReflectionSetter.setCommandState(chooseSubscribeTypeCommand, CommandState.WAITING_FEEDBACK);
        chooseSubscribeTypeCommand = spy(chooseSubscribeTypeCommand);

        Update updateWithPMChanging = update()
                .setCallbackQuery(callbackQuery()
                        .setMessage(MessageMockBuilder.message()
                                .setChatId(chatId)
                                .setMessageId(12)
                                .build())
                        .setCallbackData(SubscribeType.MP_CHANGING.name())
                        .build())
                .build();
        Optional<Session> optionalSession = telegramMockModelBuilder.getMockedSession();

        chooseSubscribeTypeCommand.onGetFeedback(updateWithPMChanging, optionalSession);

        Update updateWithVotingHistoryChanging = update()
                .setCallbackQuery(callbackQuery()
                        .setMessage(MessageMockBuilder.message()
                                .setChatId(chatId)
                                .setMessageId(12)
                                .build())
                        .setCallbackData(SubscribeType.VOTING_HISTORY_CHANGING.name())
                        .build())
                .build();

        chooseSubscribeTypeCommand.onGetFeedback(updateWithVotingHistoryChanging, optionalSession);
        Update updateWithEndCommand = update()
                .setMessage(message()
                        .setMessageText("Подписаться")
                        .setCommand(true)
                        .setChatId(chatId)
                        .build())
                .build();
        chooseSubscribeTypeCommand.onGetFeedback(updateWithEndCommand, optionalSession);

        Assert.assertTrue(chooseSubscribeTypeCommand.isFinished());

        Subscriber expectedSubscriber = new Subscriber();
        expectedSubscriber.setChatType(ChatType.CHANNEL);
        Set<SubscribeType> subscribeTypes = new HashSet<>();
        subscribeTypes.add(SubscribeType.MP_CHANGING);
        subscribeTypes.add(SubscribeType.VOTING_HISTORY_CHANGING);
        expectedSubscriber.setSubscribeTypes(subscribeTypes);
        expectedSubscriber.setChatId(channelId);

        verify(subscriberService).save(eq(expectedSubscriber));
    }

    @Test
    public void onPerform_execution_setCommandStateToWaitingFeedback() {
        Long chatId = 10L;
        ChooseSubscribeTypeCommand chooseSubscribeTypeCommand = subscribeChannelCommandsProvider.getChooseSubscribeType(
                chatId.toString());
        Update update = update().setMessage(message().setChatId(chatId).setMessageText("testMessage").build()).build();
        Optional<Session> optionalSession = session().buildOptional();

        chooseSubscribeTypeCommand.onPerform(update, optionalSession);

        Assert.assertTrue(chooseSubscribeTypeCommand.isWaitingFeedback());
    }
}