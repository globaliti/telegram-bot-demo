package com.goldsoft.radainfobot.controller.commands.subscribeChannel;

import com.goldsoft.radainfobot.client.TelegramBotClient;
import com.goldsoft.radainfobot.controller.ControllerLayerTestConfiguration;
import com.goldsoft.radainfobot.controller.commands.CommandState;
import com.goldsoft.radainfobot.controller.commands.DeepCommandReflectionSetter;
import com.goldsoft.radainfobot.service.subscribe.SubscriberService;
import com.goldsoft.radainfobot.service.subscription.SubscriptionService;
import com.goldsoft.radainfobot.service.telegram.TelegramChatService;
import org.apache.shiro.session.Session;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.lang.reflect.Field;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ControllerLayerTestConfiguration.class)
public class SubscribeChannelCommandTest {

    @Autowired
    SubscribeChannelCommandsProvider subscribeChannelCommandsProvider;

    @MockBean
    private TelegramChatService telegramChatService;

    @MockBean
    SubscriptionService subscriptionService;

    @MockBean
    private TelegramBotClient telegramBotClient;

    @MockBean
    private SubscriberService subscriberService;

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void onPerform_executeAfterCreation_changeStateToWaiting() {
        SubscribeChannelCommand subscribeChannelCommand = subscribeChannelCommandsProvider.getSubscribeChannelCommand();
        Update update = Mockito.mock(Update.class);
        Message message = Mockito.mock(Message.class);

        given(message.getChatId()).willReturn(10L);
        given(update.getMessage()).willReturn(message);

        Session session = Mockito.mock(Session.class);
        Optional<Session> optionalSession = Optional.of(session);

        subscribeChannelCommand.executeCommand(update, optionalSession);
        Assert.assertTrue(subscribeChannelCommand.isWaitingFeedback());
    }

    @Test
    public void onGetUpdates_putChannelWithMockChecking_changeStateToFinish() throws NoSuchFieldException {
        SubscribeChannelCommand subscribeChannelCommand = subscribeChannelCommandsProvider.getSubscribeChannelCommand();
        Update update = Mockito.mock(Update.class);
        Message message = Mockito.mock(Message.class);
        DeepCommandReflectionSetter.setCommandState(subscribeChannelCommand, CommandState.WAITING_FEEDBACK);

        subscribeChannelCommand = spy(subscribeChannelCommand);

        Long chatId = 10L;
        String channelAddress = "@channel";
        given(message.getChatId()).willReturn(chatId);
        given(message.getText()).willReturn(channelAddress);
        given(message.hasText()).willReturn(true);
        given(update.hasMessage()).willReturn(true);
        given(update.getMessage()).willReturn(message);
        given(telegramChatService.hasChannelAdministrationRights(eq(channelAddress),eq(chatId.toString()))).willReturn(true);

        Session session = Mockito.mock(Session.class);
        Optional<Session> optionalSession = Optional.of(session);


        Assert.assertTrue(subscribeChannelCommand.isWaitingFeedback());

        subscribeChannelCommand.executeCommand(update,optionalSession);

        Assert.assertTrue(subscribeChannelCommand.isFinished());
        verify(subscribeChannelCommand).setNextCommand(new ChooseSubscribeTypeCommand(channelAddress));
    }
}