package com.goldsoft.radainfobot.controller.commands;

import com.goldsoft.radainfobot.controller.commands.CommandState;
import com.goldsoft.radainfobot.controller.commands.DeepCommand;
import org.mockito.internal.util.reflection.FieldSetter;

public final class DeepCommandReflectionSetter {
    public static void setCommandState(DeepCommand deepCommand, CommandState commandState) throws NoSuchFieldException {
            FieldSetter.setField(
                    deepCommand,
                    deepCommand.getClass().getSuperclass().getDeclaredField("commandState"),
                    commandState);
    }
}
