package com.goldsoft.radainfobot.service;

import org.junit.Assert;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

public class ListCutterTest {

    @Test
    public void calculateQuantity() {
        ListCutter<Object> cutter = new ListCutter<Object>();
        Assert.assertEquals(11, cutter.calculatePartsQuantity(55, 5));
        Assert.assertEquals(10, cutter.calculatePartsQuantity(50, 5));
        Assert.assertEquals(12, cutter.calculatePartsQuantity(56, 5));
        Assert.assertEquals(0, cutter.calculatePartsQuantity(0, 5));

    }

    @Test
    public void cut() {
        List<String> strings = new LinkedList<>();
        for (int i = 0; i < 50; i++) {
            strings.add("" + i);
        }

        ListCutter<String> cutter = new ListCutter<String>();
        List<List<String>> parts = cutter.cut(strings,5);

        Assert.assertEquals(10, parts.size());
        Assert.assertEquals(5, cutter.cut(strings, 10).size());


        for (int i = 0; i < 5; i++) {
            Assert.assertEquals(5, parts.get(i).size());
        }

        strings = new LinkedList<>();
        for (int i = 0; i < 56; i++) {
            strings.add("" + i);
        }

        Assert.assertEquals(12, cutter.cut(strings,5).size());
        Assert.assertEquals(1, cutter.cut(strings,5).get(11).size());
        Assert.assertEquals(6, cutter.cut(strings, 10).size());
        Assert.assertEquals(6, cutter.cut(strings, 10).get(5).size());
    }
}