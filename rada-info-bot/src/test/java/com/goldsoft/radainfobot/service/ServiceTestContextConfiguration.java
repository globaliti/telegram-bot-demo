package com.goldsoft.radainfobot.service;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.ComponentScan;

@TestConfiguration
@ComponentScan(basePackages = "com.goldsoft.radainfobot.service**")
public class ServiceTestContextConfiguration {
}
