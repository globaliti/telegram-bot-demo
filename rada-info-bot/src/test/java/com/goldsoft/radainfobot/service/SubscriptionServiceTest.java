package com.goldsoft.radainfobot.service;

import com.goldsoft.radainfobot.domain.Subscriber;
import com.goldsoft.radainfobot.domain.enums.ChatType;
import com.goldsoft.radainfobot.domain.enums.SubscribeType;
import com.goldsoft.radainfobot.repository.SubscriberRepository;
import com.goldsoft.radainfobot.service.subscribe.SubscriberService;
import com.goldsoft.radainfobot.service.subscription.Subscribers;
import com.goldsoft.radainfobot.service.subscription.SubscriptionService;
import com.goldsoft.radainfobot.service.subscription.SubscriptionServiceImpl;
import com.goldsoft.radainfobot.service.telegram.TelegramChatService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.HashSet;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.anyObject;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class SubscriptionServiceTest {

    SubscriptionService subscriptionService;

    @Mock
    SubscriberService subscriberService;

    @Mock
    TelegramChatService telegramChatService;

    @Before
    public void setUp() {
        subscriptionService = new SubscriptionServiceImpl(telegramChatService, subscriberService);
    }

    @Test
    public void testAddNewSubscriber() {
        String testChatId = "testChatId";
        SubscribeType[] subscribeTypes = {SubscribeType.VOTING_HISTORY_CHANGING};

        Subscriber mockInputSubscriber = new Subscriber();
        mockInputSubscriber.setChatType(ChatType.CHANNEL);
        mockInputSubscriber.setChatId(testChatId);
        mockInputSubscriber.setSubscribeTypes(new HashSet<>(Arrays.asList(subscribeTypes)));

        Subscriber mockOutputSubscriber = new Subscriber();
        mockOutputSubscriber.setChatType(ChatType.CHANNEL);
        mockOutputSubscriber.setChatId(testChatId);
        mockOutputSubscriber.setSubscribeTypes(new HashSet<>(Arrays.asList(subscribeTypes)));
        mockOutputSubscriber.setId(1L);

        given(telegramChatService.getChatType(eq(testChatId))).willReturn(ChatType.CHANNEL);
        given(subscriberService.save(eq(mockInputSubscriber))).willReturn(mockOutputSubscriber);

        subscriptionService.addSubscriber(testChatId, subscribeTypes);

        verify(telegramChatService).getChatType(eq(testChatId));
        verify(subscriberService).save(eq(mockInputSubscriber));

        assertThat(Subscribers.VOTING_HISTORY_CHANGING.getAllSubscribers(), hasItemInArray(testChatId));
    }

    @Before
    public void cleanGlobalStatesBeforeTest() {
        Subscribers.TEST_clearVariablesInAllInstances();
    }

    @After
    public void cleanGlobalStatesAfterTest() {
        Subscribers.TEST_clearVariablesInAllInstances();
    }
}