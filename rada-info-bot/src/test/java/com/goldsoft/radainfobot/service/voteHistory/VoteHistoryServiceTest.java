package com.goldsoft.radainfobot.service.voteHistory;

import com.goldsoft.radaOpenDataApi.model.chronology.Question;
import com.goldsoft.radaOpenDataApi.model.chronology.builder.QuestionBuilder;
import com.goldsoft.radaOpenDataApi.service.ChronologyService;
import com.goldsoft.radainfobot.domain.VoteHistory;
import com.goldsoft.radainfobot.domain.builder.VoteBuilder;
import com.goldsoft.radainfobot.domain.builder.VoteHistoryBuilder;
import com.goldsoft.radainfobot.domain.builder.VoteQuestionBuilder;
import com.goldsoft.radainfobot.domain.builder.VoteResultBuilder;
import com.goldsoft.radainfobot.mapper.ChronologyQuestionMapper;
import com.goldsoft.radainfobot.repository.VoteHistoryRepository;
import com.goldsoft.radainfobot.service.subscribe.SubscriberService;
import com.goldsoft.radainfobot.service.subscription.SubscriptionService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import static com.goldsoft.radaOpenDataApi.model.chronology.builder.DateEventBuilder.dateEvent;
import static com.goldsoft.radaOpenDataApi.model.chronology.builder.DateQuestionBuilder.dateQuestion;
import static com.goldsoft.radaOpenDataApi.model.chronology.builder.EventQuestionBuilder.eventQuestion;
import static com.goldsoft.radaOpenDataApi.model.chronology.builder.QuestionBuilder.question;
import static com.goldsoft.radaOpenDataApi.model.chronology.builder.ResultEventBuilder.resultEvent;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class VoteHistoryServiceTest {

    @Mock
    VoteHistoryRepository voteHistoryRepository;

    @Mock
    SubscriberService subscriberService;

    @Mock
    SubscriptionService subscriptionService;

    @Mock
    ChronologyService chronologyService;

    @InjectMocks
    VoteHistoryService voteHistoryService = new VoteHistoryServiceImpl();

    @Spy
    ChronologyQuestionMapper chronologyQuestionMapper= new ChronologyQuestionMapper();

    @Test
    public void getLastVoteInVoteHistory() {
        LocalDate voteHistoryDate = LocalDate.of(2019, 3, 2);
        VoteHistory voteHistory = VoteHistoryBuilder.voteHistory()
                .addVoteQuestion(VoteQuestionBuilder.voteQuestion(1L)
                        .setDate(LocalDateTime.of(2019, 2, 2, 10, 35, 45))
                        .addVote(VoteBuilder.vote()
                                .setId(1L)
                                .setName("Name1")
                                .setVoteResult(VoteResultBuilder.voteResult()
                                        .setTotal((short) 30)
                                        .setAbsent((short) 30)
                                        .setAbstain((short) 30)
                                        .setVotedAgainst((short) 30)
                                        .setNotVoted((short) 30)
                                        .setPresence((short) 30)
                                        .setVotedFor((short) 30)
                                        .build())
                                .setDate(LocalDateTime.of(2019, 2, 2, 10, 38, 45))
                                .build())
                        .build())
                .addVoteQuestion(VoteQuestionBuilder.voteQuestion(2L)
                        .setDate(LocalDateTime.of(2019, 2, 2, 11, 38, 45))
                        .resetVotes()
                        .addVote(VoteBuilder.vote().setName("Name2")
                                .setDate(LocalDateTime.of(2019, 2, 2, 11, 40, 45))
                                .setVoteResult(VoteResultBuilder.voteResult()
                                        .setTotal((short) 20)
                                        .setAbsent((short) 20)
                                        .setAbstain((short) 20)
                                        .setVotedAgainst((short) 20)
                                        .setNotVoted((short) 20)
                                        .setPresence((short) 20)
                                        .setVotedFor((short) 20)
                                        .build())
                                .build())
                        .addVote(VoteBuilder.vote().setName("Name3")
                                .setDate(LocalDateTime.of(2019, 2, 2, 11, 50, 45))
                                .setVoteResult(VoteResultBuilder.voteResult()
                                        .setTotal((short) 30)
                                        .setAbsent((short) 30)
                                        .setAbstain((short) 30)
                                        .setVotedAgainst((short) 30)
                                        .setNotVoted((short) 30)
                                        .setPresence((short) 30)
                                        .setVotedFor((short) 30)
                                        .build())
                                .build())
                        .build())
                .addVoteQuestion(VoteQuestionBuilder.voteQuestion(3L)
                        .setDate(LocalDateTime.of(2019, 2, 2, 12, 50, 45))
                        .build())
                .build();
        given(voteHistoryRepository.findByDate(eq(voteHistoryDate))).willReturn(voteHistory);
        LocalDateTime lastVoteDateInVoteHistory = voteHistoryService.getLastVoteDateInVoteHistory(voteHistoryDate);
        Assert.assertNotNull(voteHistoryDate);
        Assert.assertEquals(LocalDateTime.of(2019, 2, 2, 11, 50, 45), lastVoteDateInVoteHistory);
    }

    @Test
    public void saveVoteQuestionsToVoteHistory() {
        QuestionBuilder questionBuilder = question()
                .addEventQuestion(eventQuestion()
                        .setTypeEvent(new BigDecimal(0))
                        .setNameEvent("Test voting")
                        .setDateEvent(dateEvent()
                                .setYearEvent(new BigDecimal(2019))
                                .setMonthEvent(new BigDecimal(2))
                                .setDayEvent(new BigDecimal(20))
                                .setHourEvent(new BigDecimal(12))
                                .setMinEvent(new BigDecimal(21))
                                .setSecEvent(new BigDecimal(20))
                                .build())
                        .setResultEvent(resultEvent()
                                .setFor(new BigDecimal(20))
                                .setAbsent(new BigDecimal(20))
                                .setIdEvent(new BigDecimal(208))
                                .setTotal(new BigDecimal(200))
                                .setNotVoting(new BigDecimal(20))
                                .setPresence(new BigDecimal(20))
                                .setAgainst(new BigDecimal(20))
                                .setAbstain(new BigDecimal(20))
                                .build())
                        .build())
                .setIdQuestion(new BigDecimal(2996))
                .setDateQuestion(dateQuestion()
                        .setYearQuestion(new BigDecimal(2019))
                        .setMonthQuestion(new BigDecimal(2))
                        .setDayQuestion(new BigDecimal(20))
                        .setHourQuestion(new BigDecimal(12))
                        .setMinQuestion(new BigDecimal(21))
                        .setSecQuestion(new BigDecimal(20))
                        .build());

        List<Question> sourceQuestions = new LinkedList<>();
        sourceQuestions.add(questionBuilder.build());

        questionBuilder.setIdQuestion(new BigDecimal(3000));
        questionBuilder.setDateQuestion(dateQuestion()
                .setYearQuestion(new BigDecimal(2019))
                .setMonthQuestion(new BigDecimal(2))
                .setDayQuestion(new BigDecimal(20))
                .setHourQuestion(new BigDecimal(13))
                .setMinQuestion(new BigDecimal(21))
                .setSecQuestion(new BigDecimal(20))
                .build());
        sourceQuestions.add(questionBuilder.build());


        List<Question> newQuestions = new LinkedList<>();

        questionBuilder.addEventQuestion(eventQuestion()
                .setTypeEvent(new BigDecimal(0))
                .setNameEvent("Test new voting")
                .setDateEvent(dateEvent()
                        .setYearEvent(new BigDecimal(2019))
                        .setMonthEvent(new BigDecimal(2))
                        .setDayEvent(new BigDecimal(20))
                        .setHourEvent(new BigDecimal(16))
                        .setMinEvent(new BigDecimal(21))
                        .setSecEvent(new BigDecimal(20))
                        .build())
                .setResultEvent(resultEvent()
                        .setFor(new BigDecimal(20))
                        .setAbsent(new BigDecimal(20))
                        .setIdEvent(new BigDecimal(208))
                        .setTotal(new BigDecimal(200))
                        .setNotVoting(new BigDecimal(20))
                        .setPresence(new BigDecimal(20))
                        .setAgainst(new BigDecimal(20))
                        .setAbstain(new BigDecimal(20))
                        .build())
                .build());
        newQuestions.add(question().build());

        LocalDate chronologyDate = LocalDate.of(2019, 3, 25);

        VoteHistory voteHistory = VoteHistoryBuilder.voteHistory()
                .setId(1L)
                .setDate(chronologyDate)
                .addVoteQuestion(VoteQuestionBuilder.voteQuestion(4L)
                        .setId(44L)
                        .setDate(LocalDateTime.of(2019, Calendar.MARCH, 25, 15, 5, 5))
                        .addVote(VoteBuilder.vote()
                                .setName("Name1")
                                .setDate(LocalDateTime.now())
                                .setId(1L)
                                .setVoteResult(VoteResultBuilder.voteResult()
                                        .setId(1L)
                                        .setTotal((short) 10)
                                        .setAbsent((short) 10)
                                        .setAbstain((short) 10)
                                        .setVotedAgainst((short) 10)
                                        .setNotVoted((short) 10)
                                        .setPresence((short) 10)
                                        .setVotedFor((short) 10)
                                        .build())
                                .build())
                        .build())
                .build();
        given(voteHistoryService.findByDate(eq(chronologyDate))).willReturn(voteHistory);
        VoteHistory savedHistory = voteHistoryService.saveQuestionsToVoteHistory(chronologyDate, sourceQuestions);
        System.out.println("=======================================================================");
        System.out.println(savedHistory);
    }
}