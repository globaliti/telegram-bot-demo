package com.goldsoft.radainfobot.domain;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.ComponentScan;

@TestConfiguration
@ComponentScan(basePackages = "com.goldsoft.radainfobot.domain.*")
public class DomainTestContextConfiguration {
}
