package com.goldsoft.radaOpenDataApi.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

@Configuration
public class RadaApiProfilesConfiguration {
    @Configuration
    @Profile("test")
    @PropertySource("classpath:rada-open-data-test.properties")
    static class Test{}

    @Configuration
    @Profile("default")
    @PropertySource("classpath:rada-open-data.properties")
    static class Defaults
    { }
}
