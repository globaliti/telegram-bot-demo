package com.goldsoft.radaOpenDataApi.service;

import com.goldsoft.radaOpenDataApi.model.agenda.Agendas;
import com.goldsoft.radaOpenDataApi.model.enums.Convene;

public interface AgendaService {
    Agendas getAgendas(Convene convene);
}
