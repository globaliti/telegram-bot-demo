package com.goldsoft.radaOpenDataApi.service;

import com.goldsoft.radaOpenDataApi.model.enums.Convene;
import com.goldsoft.radaOpenDataApi.model.faction.Factions;

public interface FactionService {
    Factions getFactions(Convene convene);
}
