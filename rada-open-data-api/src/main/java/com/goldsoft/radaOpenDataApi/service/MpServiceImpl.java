package com.goldsoft.radaOpenDataApi.service;

import com.goldsoft.radaOpenDataApi.model.enums.Convene;
import com.goldsoft.radaOpenDataApi.model.mp.Mps;
import com.goldsoft.radaOpenDataApi.util.MpUriBinder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class MpServiceImpl implements MpService {

    private final RestTemplate restTemplate;

    private final MpUriBinder mpUriBinder;

    public MpServiceImpl(RestTemplate restTemplate, MpUriBinder mpUriBinder) {
        this.restTemplate = restTemplate;
        this.mpUriBinder = mpUriBinder;
    }

    @Override
    public Mps findAllMps(Convene convene) {
        return restTemplate.getForObject(mpUriBinder.getMpsUri(convene), Mps.class);
    }
}
