package com.goldsoft.radaOpenDataApi.service;

import com.goldsoft.radaOpenDataApi.model.chronology.Chronology;
import com.goldsoft.radaOpenDataApi.model.chronology.ChronologyInformation;
import com.goldsoft.radaOpenDataApi.model.chronology.EventQuestion;
import com.goldsoft.radaOpenDataApi.model.chronology.Question;
import com.goldsoft.radaOpenDataApi.model.enums.Convene;
import com.goldsoft.radaOpenDataApi.util.ChronologyUriBinder;
import com.goldsoft.radaOpenDataApi.util.TimeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ChronologyServiceImpl implements ChronologyService {

    Logger logger = LoggerFactory.getLogger(ChronologyServiceImpl.class);

    private final RestTemplate restTemplate;

    private final ChronologyUriBinder chronologyUriBinder;

    @Autowired
    public ChronologyServiceImpl(RestTemplate restTemplate, ChronologyUriBinder chronologyUriBinder) {
        this.restTemplate = restTemplate;
        this.chronologyUriBinder = chronologyUriBinder;
    }

//    @Override
//    public List<String> getChronologyDates(Convene convene) {
//        String response = restTemplate.getForObject(chronologyUriBinder.getChronologyDatesUri(convene), String.class);
//        String[] dates = response.split("\n");
//        return Arrays.asList(dates);
//    }

    @Override
    public List<Date> getChronologyDates(Convene convene) {
        String response = restTemplate.getForObject(chronologyUriBinder.getChronologyDatesUri(convene), String.class);
        String[] dates = response.split("\n");
        List<Date> chronologyDates = new LinkedList<>();
        for (String date : dates) {
            chronologyDates.add(TimeUtil.getChronologyDate(date));
        }
        return chronologyDates;
    }

    @Override
    public List<LocalDate> getChronologyLocalDates(Convene convene) {
        String response = restTemplate.getForObject(chronologyUriBinder.getChronologyDatesUri(convene), String.class);
        String[] dates = response.split("\n");
        List<LocalDate> chronologyDates = new LinkedList<>();
        for (String date : dates) {
            chronologyDates.add(TimeUtil.getChronologyLocalDate(date));
        }
        return chronologyDates;
    }

    @Override
    public Chronology getChronology(Convene convene, String date) {
        Chronology chronology = restTemplate.getForObject(chronologyUriBinder.getChronologyUri(convene, date), Chronology.class);
        System.out.println("GETTING CHROOLOG : ");
        System.out.println(chronology);
        return chronology;
    }

    @Override
    public Chronology getChronology(Convene convene, LocalDate date) {
        return getChronology(convene, TimeUtil.getChronologyDateString(date));
    }

    @Override
    public ChronologyInformation getSectionInformation(Convene convene) {
        return restTemplate.getForObject(
                chronologyUriBinder.getSectionInformationJsonUri(Convene.NINTH),
                ChronologyInformation.class);
    }

//    @Override
//    public List<Question> getQuestionsThatHasEventsAfterDate(Convene convene, Date chronologyDate, Date searchDate) {
//        Chronology chronology = this.getChronology(Convene.NINTH,chronologyDate);
//
//        return null;
//    }


    @Override
    public List<Question> findQuestionsWithNewEventsAfterDate(Chronology chronology, LocalDateTime searchDate) {
        List<Question> foundQuestions = new LinkedList<>();

        for (Question question : chronology.getQuestion()) {
            if (isHasNewEventsAfterDate(question, searchDate)) {
                foundQuestions.add(question);
            }
        }

        return foundQuestions;
    }

    @Override
    public List<EventQuestion> findEventsWithVoting(List<Question> questions) {
        List<EventQuestion> votingEvents = new LinkedList<>();
        for (Question question : questions) {
            for (EventQuestion eventQuestion : question.getEventQuestion()) {
                if (eventQuestion.getTypeEvent().intValue() == 0) {
                    votingEvents.add(eventQuestion);
                }
            }
        }
        return votingEvents;
    }

    @Override
    public List<EventQuestion> findEventsWithVotingAfterDate(List<Question> questions, LocalDateTime searchDate) {
        List<EventQuestion> votingEvents = new LinkedList<>();
        for (Question question : questions) {
            for (EventQuestion eventQuestion : question.getEventQuestion()) {
                if (eventQuestion.getTypeEvent().intValue() == 0 && TimeUtil.getLocalDateTime(eventQuestion.getDateEvent()).isAfter(searchDate)) {
                    votingEvents.add(eventQuestion);
                }
            }
        }
        return votingEvents;
    }

    @Override
    public List<EventQuestion> findEventsWithVoting(Question question) {
        List<EventQuestion> votingEvents = new LinkedList<>();
        for (EventQuestion eventQuestion : question.getEventQuestion()) {
            if (eventQuestion.getTypeEvent().intValue() == 0) {
                votingEvents.add(eventQuestion);
            }
        }
        return votingEvents;
    }

    @Override
    public List<EventQuestion> findEventsWithVotingAfterDate(Question question, LocalDateTime searchDate) {
        List<EventQuestion> votingEvents = new LinkedList<>();
        for (EventQuestion eventQuestion : question.getEventQuestion()) {
            if (eventQuestion.getTypeEvent().intValue() == 0 && TimeUtil.getLocalDateTime(eventQuestion.getDateEvent()).isAfter(searchDate)) {
                votingEvents.add(eventQuestion);
            } else {
                LocalDateTime localDateTime = TimeUtil.getLocalDateTime(eventQuestion.getDateEvent());
                System.out.println(localDateTime);
            }
        }
        return votingEvents;
    }

    @Override
    public List<Question> getQuestionsWithVotingEvents(List<Question> questions) { //TODO он упускает даты и прочие поля
        List<Question> questionsWithVoting = new LinkedList<>();
        for (Question question : questions) {
            List<EventQuestion> votingEvents = new LinkedList<>();

            for (EventQuestion eventQuestion : question.getEventQuestion()) {
                if (eventQuestion.getTypeEvent().intValue() == 0) {
                    votingEvents.add(eventQuestion);
                }
            }

            if (!votingEvents.isEmpty()) {
                Question questionWithVoting = new Question();
                questionWithVoting.setEventQuestion(votingEvents);
                questionWithVoting.setDateQuestion(question.getDateQuestion());
                questionWithVoting.setIdQuestion(question.getIdQuestion());
                questionsWithVoting.add(questionWithVoting);
            }
        }
        return questionsWithVoting;
    }

    private boolean isHasNewEventsAfterDate(Question question, LocalDateTime searchDate) {
        for (EventQuestion eventQuestion : question.getEventQuestion()) {
            LocalDateTime eventDate = TimeUtil.getLocalDateTime(eventQuestion.getDateEvent());
            if (eventDate.isAfter(searchDate)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public List<Question> findQuestionsWithNewVotesAfterDate(Chronology chronology, LocalDateTime searchDate) {
        return findQuestionsWithNewEventsAfterDate(chronology, searchDate)
                .stream()
                .peek(question -> question.setEventQuestion(findEventsWithVotingAfterDate(question, searchDate)))
                .filter(question ->
                        !question.getEventQuestion().isEmpty()
                )
                .collect(Collectors.toList());
    }

    @Override
    public boolean isChronologyPublicationDateHasUpdate(Date checkDate, Convene convene) {
        boolean isHasUpdate = false;
        if (checkDate == null) {
            isHasUpdate = true;
        } else {
            Date lastPublicationDate = getSectionInformation(convene).getPublicationDate();
            if (lastPublicationDate.after(checkDate)) {
                isHasUpdate = true;
            }
        }
        return isHasUpdate;
    }
}