package com.goldsoft.radaOpenDataApi.service;

import com.goldsoft.radaOpenDataApi.model.chronology.Chronology;
import com.goldsoft.radaOpenDataApi.model.chronology.ChronologyInformation;
import com.goldsoft.radaOpenDataApi.model.chronology.EventQuestion;
import com.goldsoft.radaOpenDataApi.model.chronology.Question;
import com.goldsoft.radaOpenDataApi.model.enums.Convene;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public interface ChronologyService {
    Chronology getChronology(Convene convene, String date);

    Chronology getChronology(Convene convene, LocalDate date);

    List<Date> getChronologyDates(Convene convene);

    List<LocalDate> getChronologyLocalDates(Convene convene);

    ChronologyInformation getSectionInformation(Convene convene);

    List<Question> findQuestionsWithNewEventsAfterDate(Chronology chronology, LocalDateTime searchDate);

    List<Question> findQuestionsWithNewVotesAfterDate(Chronology chronology, LocalDateTime searchDate);

    List<EventQuestion> findEventsWithVoting(List<Question> questions);

    List<EventQuestion> findEventsWithVoting(Question question);

    List<Question> getQuestionsWithVotingEvents(List<Question> questions);

    boolean isChronologyPublicationDateHasUpdate(Date checkDate, Convene convene);

    List<EventQuestion> findEventsWithVotingAfterDate(List<Question> questions, LocalDateTime searchDate);

    public List<EventQuestion> findEventsWithVotingAfterDate(Question question, LocalDateTime searchDate);
}