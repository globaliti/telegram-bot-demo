package com.goldsoft.radaOpenDataApi.service;

import com.goldsoft.radaOpenDataApi.model.chronology.DateQuestion;
import com.goldsoft.radaOpenDataApi.model.chronology.Question;
import com.goldsoft.radaOpenDataApi.util.TimeUtil;

import java.util.Comparator;
import java.util.Date;

public class ChronologyQuestionDateComparator implements Comparator<Question> {
    @Override
    public int compare(Question first, Question second) {
        Long firstDate = TimeUtil.getDate(first.getDateQuestion()).getTime();
        Long secondDate = TimeUtil.getDate(second.getDateQuestion()).getTime();
        return (int) (firstDate - secondDate);
    }
}
