package com.goldsoft.radaOpenDataApi.service;

import com.goldsoft.radaOpenDataApi.model.agenda.Agendas;
import com.goldsoft.radaOpenDataApi.model.enums.Convene;
import com.goldsoft.radaOpenDataApi.util.AgendaUriBinder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class AgendaServiceImpl implements AgendaService {

    private final RestTemplate restTemplate;

    private final AgendaUriBinder agendaUriBinder;

    public AgendaServiceImpl(RestTemplate restTemplate, AgendaUriBinder agendaUriBinder) {
        this.restTemplate = restTemplate;
        this.agendaUriBinder = agendaUriBinder;
    }

    @Override
    public Agendas getAgendas(Convene convene) {
        return restTemplate.getForObject(agendaUriBinder.getAgendasInXmlUri(convene), Agendas.class);
    }
}
