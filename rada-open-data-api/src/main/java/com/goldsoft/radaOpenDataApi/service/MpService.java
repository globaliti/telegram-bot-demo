package com.goldsoft.radaOpenDataApi.service;

import com.goldsoft.radaOpenDataApi.model.enums.Convene;
import com.goldsoft.radaOpenDataApi.model.mp.Mps;

public interface MpService {
    Mps findAllMps(Convene convene);
}
