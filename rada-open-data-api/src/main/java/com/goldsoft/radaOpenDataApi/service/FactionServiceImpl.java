package com.goldsoft.radaOpenDataApi.service;

import com.goldsoft.radaOpenDataApi.model.enums.Convene;
import com.goldsoft.radaOpenDataApi.model.faction.Factions;
import com.goldsoft.radaOpenDataApi.util.FactionUriBinder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class FactionServiceImpl implements FactionService {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private FactionUriBinder factionUriBinder;

    @Override
    public Factions getFactions(Convene convene) {
        return restTemplate.getForObject(factionUriBinder.getFactionsInXmlUri(convene),Factions.class);
    }
}