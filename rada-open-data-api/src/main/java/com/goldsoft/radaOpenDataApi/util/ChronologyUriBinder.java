package com.goldsoft.radaOpenDataApi.util;

import com.goldsoft.radaOpenDataApi.model.enums.Convene;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;


@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ChronologyUriBinder {

    @Value("${rada-open-data.scheme:https}")
    private String SCHEME;

    @Value("${rada-open-data.host:data.rada.gov.ua}")
    private String HOST;

    @Value("${rada-open-data.port:443}")
    private int PORT;

    private final String GET_CHRONOLOGY_PATH = "/ogd/zal/ppz/skl{convene}/xml/{date}.xml";

    private final String GET_CHRONOLOGY_DATES_PATH = "/ogd/zal/ppz/skl{convene}/dict/dates.txt";

    private final String GET_SECTION_INFORMATION_JSON = "/open/data/chron_ppz-skl{convene}.json";

    public String getChronologyDatesUri(Convene convene) {
        UriComponents uriComponents = UriComponentsBuilder.newInstance()
                .scheme(SCHEME).host(HOST).path(GET_CHRONOLOGY_DATES_PATH).port(PORT)
                .buildAndExpand(convene.getConveneNumber());
        return uriComponents.toString();
    }

    public String getChronologyDatesPath(Convene convene) {
        UriComponents uriComponents = UriComponentsBuilder.newInstance()
                .scheme(SCHEME).host(HOST).path(GET_CHRONOLOGY_DATES_PATH).port(PORT)
                .buildAndExpand(convene.getConveneNumber());
        return uriComponents.getPath();
    }

    public String getChronologyUri(Convene convene, String date) {
        date = date.replaceAll("\\.", "");
        UriComponents uriComponents = UriComponentsBuilder.newInstance()
                .scheme(SCHEME).host(HOST).path(GET_CHRONOLOGY_PATH).port(PORT)
                .buildAndExpand(convene.getConveneNumber(), date);
        return uriComponents.toString();
    }

    public String getChronologyPath(Convene convene, String date) {
        date = date.replaceAll("\\.", "");
        UriComponents uriComponents = UriComponentsBuilder.newInstance()
                .scheme(SCHEME).host(HOST).path(GET_CHRONOLOGY_PATH).port(PORT)
                .buildAndExpand(convene.getConveneNumber(), date);
        return uriComponents.getPath();
    }

    public String getSectionInformationJsonUri(Convene convene) {
        UriComponents uriComponents = UriComponentsBuilder.newInstance()
                .scheme(SCHEME).host(HOST).path(GET_SECTION_INFORMATION_JSON).port(PORT)
                .buildAndExpand(convene.getConveneNumber());
        return uriComponents.toString();
    }

    public String getSectionInformationJsonPath(Convene convene) {
        UriComponents uriComponents = UriComponentsBuilder.newInstance()
                .scheme(SCHEME).host(HOST).path(GET_SECTION_INFORMATION_JSON).port(PORT)
                .buildAndExpand(convene.getConveneNumber());
        return uriComponents.getPath();
    }
}