package com.goldsoft.radaOpenDataApi.util;

import com.goldsoft.radaOpenDataApi.model.chronology.DateAgenda;
import com.goldsoft.radaOpenDataApi.model.chronology.DateEvent;
import com.goldsoft.radaOpenDataApi.model.chronology.DateQuestion;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalField;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class TimeUtil {
    public static String getChronologyDateString(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        return format.format(date);
    }

    public static String getChronologyDateString(LocalDate date) {
        String pattern = "dd.MM.yyyy";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        return date.format(formatter);
    }

    public static Date getChronologyDate(String stringDate) {
        Date date = null;
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        try {
            date = format.parse(stringDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static LocalDate getChronologyLocalDate(String stringDate) {
        LocalDate date = null;
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        try {
            date = format.parse(stringDate).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static LocalDate getChronologyLocalDate(DateAgenda dateAgenda) {
        return LocalDate.of(dateAgenda.getYearAgenda().intValue(),
                dateAgenda.getMonthAgenda().intValue(),
                dateAgenda.getDayAgenda().intValue());
    }

    public static List<Date> getChronologyDates(List<String> stringDates) {
        List<Date> chronologyDates = new LinkedList<>();
        for (String stringDate : stringDates) {
            chronologyDates.add(getChronologyDate(stringDate));
        }
        return chronologyDates;
    }

    public static List<Date> getChronologyDates(String[] stringDates) {
        List<Date> chronologyDates = new LinkedList<>();
        for (String stringDate : stringDates) {
            chronologyDates.add(getChronologyDate(stringDate));
        }
        return chronologyDates;
    }

    public static DateQuestion getDateQuestion(Date date) {
        DateQuestion dateQuestion = new DateQuestion();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        dateQuestion.setYearQuestion(new BigDecimal(calendar.get(Calendar.YEAR)));
        dateQuestion.setMonthQuestion(new BigDecimal(calendar.get(Calendar.MONTH) + 1));
        dateQuestion.setDayQuestion(new BigDecimal(calendar.get(Calendar.DAY_OF_MONTH)));
        dateQuestion.setHourQuestion(new BigDecimal(calendar.get(Calendar.HOUR_OF_DAY)));
        dateQuestion.setMinQuestion(new BigDecimal(calendar.get(Calendar.MINUTE)));
        dateQuestion.setSecQuestion(new BigDecimal(calendar.get(Calendar.SECOND)));

        return dateQuestion;
    }

    public static DateQuestion getDateQuestion(LocalDateTime date) {
        DateQuestion dateQuestion = new DateQuestion();

        dateQuestion.setYearQuestion(new BigDecimal(date.getYear()));
        dateQuestion.setMonthQuestion(new BigDecimal(date.getMonth().getValue()));
        dateQuestion.setDayQuestion(new BigDecimal(date.getDayOfMonth()));
        dateQuestion.setHourQuestion(new BigDecimal(date.getHour()));
        dateQuestion.setMinQuestion(new BigDecimal(date.getMinute()));
        dateQuestion.setSecQuestion(new BigDecimal(date.getSecond()));

        return dateQuestion;
    }

    public static Date getDate(DateQuestion dateQuestion) {
        Date date = null;

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(dateQuestion.getYearQuestion().intValue(),
                (dateQuestion.getMonthQuestion().intValue() - 1),
                dateQuestion.getDayQuestion().intValue(),
                dateQuestion.getHourQuestion().intValue(),
                dateQuestion.getMinQuestion().intValue(),
                dateQuestion.getSecQuestion().intValue());
        date = calendar.getTime();
        return date;
    }

    public static LocalDateTime getLocalDateTime(DateQuestion dateQuestion) {
        return LocalDateTime.of(dateQuestion.getYearQuestion().intValue(),
                dateQuestion.getMonthQuestion().intValue(),
                dateQuestion.getDayQuestion().intValue(),
                dateQuestion.getHourQuestion().intValue(),
                dateQuestion.getMinQuestion().intValue(),
                dateQuestion.getSecQuestion().intValue(),
                0);
    }

    public static LocalDateTime getLocalDateTime(DateEvent dateEvent) {
        return LocalDateTime.of(dateEvent.getYearEvent().intValue(),
                dateEvent.getMonthEvent().intValue(),
                dateEvent.getDayEvent().intValue(),
                dateEvent.getHourEvent().intValue(),
                dateEvent.getMinEvent().intValue(),
                dateEvent.getSecEvent().intValue(),
                0);
    }

    public static Date getDate(DateEvent dateEvent) {
        Date date = null;

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(dateEvent.getYearEvent().intValue(),
                (dateEvent.getMonthEvent().intValue() - 1),
                dateEvent.getDayEvent().intValue(),
                dateEvent.getHourEvent().intValue(),
                dateEvent.getMinEvent().intValue(),
                dateEvent.getSecEvent().intValue());
        date = calendar.getTime();
        return date;
    }

    public static DateEvent getDateEvent(Date date) {
        DateEvent dateEvent = new DateEvent();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        dateEvent.setYearEvent(new BigDecimal(calendar.get(Calendar.YEAR)));
        dateEvent.setMonthEvent(new BigDecimal(calendar.get(Calendar.MONTH) + 1));
        dateEvent.setDayEvent(new BigDecimal(calendar.get(Calendar.DAY_OF_MONTH)));
        dateEvent.setHourEvent(new BigDecimal(calendar.get(Calendar.HOUR_OF_DAY)));
        dateEvent.setMinEvent(new BigDecimal(calendar.get(Calendar.MINUTE)));
        dateEvent.setSecEvent(new BigDecimal(calendar.get(Calendar.SECOND)));

        return dateEvent;
    }

    public static DateEvent getDateEvent(LocalDateTime date) {
        DateEvent dateEvent = new DateEvent();

        dateEvent.setYearEvent(new BigDecimal(date.getYear()));
        dateEvent.setMonthEvent(new BigDecimal(date.getMonth().getValue()));
        dateEvent.setDayEvent(new BigDecimal(date.getDayOfMonth()));
        dateEvent.setHourEvent(new BigDecimal(date.getHour()));
        dateEvent.setMinEvent(new BigDecimal(date.getMinute()));
        dateEvent.setSecEvent(new BigDecimal(date.getSecond()));

        return dateEvent;
    }

    /**
     * Compare two dates without considering milliseconds. Maximum accuracy in seconds.
     *
     * @param firstDate
     * @param secondDate
     * @return 1 if firstDate > secondDate, -1 firstDate < secondDate, 0 if dates equals
     */
    public static int compareDatesWithoutMils(Date firstDate, Date secondDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(firstDate);
        calendar.set(Calendar.MILLISECOND, 0);
        firstDate = calendar.getTime();
        calendar.setTime(secondDate);
        calendar.set(Calendar.MILLISECOND, 0);
        secondDate = calendar.getTime();

        int result = -100;

        if (firstDate.getTime() > secondDate.getTime()) {
            result = 1;
        } else {
            if (firstDate.getTime() < secondDate.getTime()) {
                result = -1;
            } else {
                if (firstDate.getTime() == secondDate.getTime()) {
                    result = 0;
                }
            }
        }
        return result;
    }
}
