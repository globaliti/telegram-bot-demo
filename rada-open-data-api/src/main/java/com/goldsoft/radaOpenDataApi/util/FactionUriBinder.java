package com.goldsoft.radaOpenDataApi.util;

import com.goldsoft.radaOpenDataApi.model.enums.Convene;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class FactionUriBinder {
    @Value("${rada-open-data.scheme:https}")
    private String SCHEME;

    @Value("${rada-open-data.host:data.rada.gov.ua}")
    private String HOST;

    @Value("${rada-open-data.port:443}")
    private int PORT;

    private final String GET_FACTIONS_IN_XML_PATH = "/ogd/zal/ppz/skl{convene}/dict/factions.xml";

    protected FactionUriBinder(){}

    public String getFactionsInXmlUri(Convene convene){
        UriComponents uriComponents = UriComponentsBuilder.newInstance()
                .scheme(SCHEME).host(HOST).path(GET_FACTIONS_IN_XML_PATH).port(PORT)
                .buildAndExpand(convene.getConveneNumber());
        return uriComponents.toString();
    }
}
