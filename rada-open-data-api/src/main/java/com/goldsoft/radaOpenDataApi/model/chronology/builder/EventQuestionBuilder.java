package com.goldsoft.radaOpenDataApi.model.chronology.builder;

import com.goldsoft.radaOpenDataApi.model.chronology.DateEvent;
import com.goldsoft.radaOpenDataApi.model.chronology.EventQuestion;
import com.goldsoft.radaOpenDataApi.model.chronology.ResultEvent;

import java.math.BigDecimal;

public class EventQuestionBuilder {

    private DateEvent dateEvent;

    private String nameEvent;

    private ResultEvent resultEvent;

    private BigDecimal typeEvent;

    public static EventQuestionBuilder eventQuestion(){
        return new EventQuestionBuilder();
    }

    public EventQuestionBuilder setDateEvent(DateEvent dateEvent) {
        this.dateEvent = dateEvent;
        return this;
    }

    public EventQuestionBuilder setNameEvent(String nameEvent) {
        this.nameEvent = nameEvent;
        return this;
    }

    public EventQuestionBuilder setResultEvent(ResultEvent resultEvent) {
        this.resultEvent = resultEvent;
        return this;
    }

    public EventQuestionBuilder setTypeEvent(BigDecimal typeEvent) {
        this.typeEvent = typeEvent;
        return this;
    }

    public EventQuestion build(){
        EventQuestion eventQuestion = new EventQuestion();
        eventQuestion.setDateEvent(dateEvent);
        eventQuestion.setResultEvent(resultEvent);
        eventQuestion.setTypeEvent(typeEvent);
        eventQuestion.setNameEvent(nameEvent);
        return eventQuestion;
    }
}