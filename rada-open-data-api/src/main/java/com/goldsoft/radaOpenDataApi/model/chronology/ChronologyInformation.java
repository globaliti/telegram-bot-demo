package com.goldsoft.radaOpenDataApi.model.chronology;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.Objects;

public class ChronologyInformation {
    @JsonProperty("id")
    private String id;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss", timezone = "Europe/Kiev")
    @JsonProperty("creationDate")
    private Date creationDate;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss", timezone = "Europe/Kiev")
    @JsonProperty("pubDate")
    private Date publicationDate;

    @JsonProperty("publisher")
    private String publisher;

    @JsonProperty("creator")
    private String creator;

    @JsonProperty("manager")
    private String manager;

    @JsonProperty("managerPhone")
    private String managerPhone;

    @JsonProperty("webMaster")
    private String webMaster;

    @JsonProperty("category")
    private String category;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }

    public String getManagerPhone() {
        return managerPhone;
    }

    public void setManagerPhone(String managerPhone) {
        this.managerPhone = managerPhone;
    }

    public String getWebMaster() {
        return webMaster;
    }

    public void setWebMaster(String webMaster) {
        this.webMaster = webMaster;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChronologyInformation that = (ChronologyInformation) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(creationDate, that.creationDate) &&
                Objects.equals(publicationDate, that.publicationDate) &&
                Objects.equals(publisher, that.publisher) &&
                Objects.equals(creator, that.creator) &&
                Objects.equals(manager, that.manager) &&
                Objects.equals(managerPhone, that.managerPhone) &&
                Objects.equals(webMaster, that.webMaster) &&
                Objects.equals(category, that.category);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, creationDate, publicationDate, publisher, creator, manager, managerPhone, webMaster, category);
    }
}
