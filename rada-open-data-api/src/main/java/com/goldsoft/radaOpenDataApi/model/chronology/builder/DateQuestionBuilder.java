package com.goldsoft.radaOpenDataApi.model.chronology.builder;

import com.goldsoft.radaOpenDataApi.model.chronology.DateQuestion;

import javax.xml.bind.annotation.XmlElement;
import java.math.BigDecimal;

public class DateQuestionBuilder {

    private BigDecimal dayQuestion;

    private BigDecimal monthQuestion;

    private BigDecimal yearQuestion;

    private BigDecimal hourQuestion;

    private BigDecimal minQuestion;

    private BigDecimal secQuestion;

    public static DateQuestionBuilder dateQuestion() {
        return new DateQuestionBuilder();
    }

    public DateQuestionBuilder setDayQuestion(BigDecimal dayQuestion) {
        this.dayQuestion = dayQuestion;
        return this;
    }

    public DateQuestionBuilder setMonthQuestion(BigDecimal monthQuestion) {
        this.monthQuestion = monthQuestion;
        return this;
    }

    public DateQuestionBuilder setYearQuestion(BigDecimal yearQuestion) {
        this.yearQuestion = yearQuestion;
        return this;
    }

    public DateQuestionBuilder setHourQuestion(BigDecimal hourQuestion) {
        this.hourQuestion = hourQuestion;
        return this;
    }

    public DateQuestionBuilder setMinQuestion(BigDecimal minQuestion) {
        this.minQuestion = minQuestion;
        return this;
    }

    public DateQuestionBuilder setSecQuestion(BigDecimal secQuestion) {
        this.secQuestion = secQuestion;
        return this;
    }

    public DateQuestion build() {
        DateQuestion dateQuestion = new DateQuestion();
        dateQuestion.setYearQuestion(yearQuestion);
        dateQuestion.setMonthQuestion(monthQuestion);
        dateQuestion.setDayQuestion(dayQuestion);
        dateQuestion.setHourQuestion(hourQuestion);
        dateQuestion.setMinQuestion(minQuestion);
        dateQuestion.setSecQuestion(secQuestion);
        return dateQuestion;
    }
}
