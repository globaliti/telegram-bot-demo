package com.goldsoft.radaOpenDataApi.model.chronology.builder;

import com.goldsoft.radaOpenDataApi.model.chronology.Mp;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.math.BigDecimal;

public class MpBuilder {

    private BigDecimal faction;

    private BigDecimal result;

    private BigDecimal komment;

    private String idMp;

    public static MpBuilder mp() {
        return new MpBuilder();
    }

    public MpBuilder setFaction(BigDecimal faction) {
        this.faction = faction;
        return this;
    }

    public MpBuilder setResult(BigDecimal result) {
        this.result = result;
        return this;
    }

    public MpBuilder setKomment(BigDecimal komment) {
        this.komment = komment;
        return this;
    }

    public MpBuilder setIdMp(String idMp) {
        this.idMp = idMp;
        return this;
    }

    public Mp build() {
        Mp mp = new Mp();
        mp.setFaction(faction);
        mp.setIdMp(idMp);
        mp.setKomment(komment);
        mp.setResult(result);
        return mp;
    }
}
