package com.goldsoft.radaOpenDataApi.model.chronology.builder;

import com.goldsoft.radaOpenDataApi.model.chronology.Mp;
import com.goldsoft.radaOpenDataApi.model.chronology.ResultByName;

import java.util.LinkedList;
import java.util.List;

public class ResultByNameBuilder {
    private List<Mp> mps = new LinkedList<>();

    public static ResultByNameBuilder resultByName(){
        return new ResultByNameBuilder();
    }

    public ResultByNameBuilder addMp(Mp mp){
        mps.add(mp);
        return this;
    }

    public ResultByName build(){
        ResultByName resultByName = new ResultByName();
        resultByName.setMp(mps);
        return resultByName;
    }
}
