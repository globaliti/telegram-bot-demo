package com.goldsoft.radaOpenDataApi.model.chronology.builder;

import com.goldsoft.radaOpenDataApi.model.chronology.ResultByName;
import com.goldsoft.radaOpenDataApi.model.chronology.ResultEvent;

import java.math.BigDecimal;
import java.util.Random;

public class ResultEventBuilder {

    private BigDecimal idEvent;

    private BigDecimal _for;

    private BigDecimal against;

    private BigDecimal abstain;

    private BigDecimal notVoting;

    private BigDecimal total;

    private BigDecimal presence;

    private BigDecimal absent;

    private ResultByName resultByName;

    public static ResultEventBuilder resultEvent() {
        return new ResultEventBuilder();
    }

    public ResultEventBuilder setIdEvent(BigDecimal idEvent) {
        this.idEvent = idEvent;
        return this;
    }

    public ResultEventBuilder setFor(BigDecimal _for) {
        this._for = _for;
        return this;
    }

    public ResultEventBuilder setAgainst(BigDecimal against) {
        this.against = against;
        return this;
    }

    public ResultEventBuilder setAbstain(BigDecimal abstain) {
        this.abstain = abstain;
        return this;
    }

    public ResultEventBuilder setNotVoting(BigDecimal notVoting) {
        this.notVoting = notVoting;
        return this;
    }

    public ResultEventBuilder setTotal(BigDecimal total) {
        this.total = total;
        return this;
    }

    public ResultEventBuilder setPresence(BigDecimal presence) {
        this.presence = presence;
        return this;
    }

    public ResultEventBuilder setAbsent(BigDecimal absent) {
        this.absent = absent;
        return this;
    }

    public ResultEventBuilder setResultByName(ResultByName resultByName) {
        this.resultByName = resultByName;
        return this;
    }

    public ResultEvent build() {
        ResultEvent resultEvent = new ResultEvent();
        resultEvent.setAbsent(absent);
        resultEvent.setFor(_for);
        resultEvent.setAbstain(abstain);
        resultEvent.setAgainst(against);
        resultEvent.setIdEvent(idEvent);
        resultEvent.setNotVoting(notVoting);
        resultEvent.setTotal(total);
        resultEvent.setPresence(presence);
        resultEvent.setResultByName(resultByName);
        return resultEvent;
    }
}
