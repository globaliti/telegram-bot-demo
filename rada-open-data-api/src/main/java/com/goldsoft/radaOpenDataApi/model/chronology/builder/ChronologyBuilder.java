package com.goldsoft.radaOpenDataApi.model.chronology.builder;

import com.goldsoft.radaOpenDataApi.model.chronology.Chronology;
import com.goldsoft.radaOpenDataApi.model.chronology.DateAgenda;
import com.goldsoft.radaOpenDataApi.model.chronology.Question;

import java.util.LinkedList;
import java.util.List;

public class ChronologyBuilder {
    private DateAgenda dateAgenda;
    private List<Question> questions = new LinkedList<>();

    public static ChronologyBuilder chronology() {
        return new ChronologyBuilder();
    }

    public ChronologyBuilder setDateAgenda(DateAgenda dateAgenda) {
        this.dateAgenda = dateAgenda;
        return this;
    }

    public ChronologyBuilder addQuestion(Question question) {
        this.questions.add(question);
        return this;
    }

    public Chronology build(){
        Chronology chronology = new Chronology();
        chronology.setQuestion(questions);
        chronology.setDateAgenda(dateAgenda);
        return chronology;
    }
}
