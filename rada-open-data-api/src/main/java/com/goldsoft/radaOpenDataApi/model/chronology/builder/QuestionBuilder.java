package com.goldsoft.radaOpenDataApi.model.chronology.builder;

import com.goldsoft.radaOpenDataApi.model.chronology.DateQuestion;
import com.goldsoft.radaOpenDataApi.model.chronology.EventQuestion;
import com.goldsoft.radaOpenDataApi.model.chronology.Question;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

public class QuestionBuilder {
    private DateQuestion dateQuestion;
    private List<EventQuestion> eventQuestion = new LinkedList<>();
    private BigDecimal idQuestion;

    public static QuestionBuilder question() {
        return new QuestionBuilder();
    }

    public QuestionBuilder setDateQuestion(DateQuestion dateQuestion) {
        this.dateQuestion = dateQuestion;
        return this;
    }

    public QuestionBuilder addEventQuestion(EventQuestion eventQuestion) {
        this.eventQuestion.add(eventQuestion);
        return this;
    }

    public QuestionBuilder setIdQuestion(BigDecimal idQuestion) {
        this.idQuestion = idQuestion;
        return this;
    }

    public Question build() {
        Question question = new Question();
        question.setIdQuestion(idQuestion);
        question.setDateQuestion(dateQuestion);
        question.setEventQuestion(eventQuestion);
        return question;
    }
}