package com.goldsoft.radaOpenDataApi.model.chronology.builder;

import com.goldsoft.radaOpenDataApi.model.chronology.DateAgenda;

import java.math.BigDecimal;

public class DateAgendaBuilder {
    private BigDecimal dayAgenda;
    private BigDecimal monthAgenda;
    private BigDecimal yearAgenda;

    public static DateAgendaBuilder dateAgenda() {
        return new DateAgendaBuilder();
    }

    public DateAgendaBuilder setDayAgenda(BigDecimal dayAgenda) {
        this.dayAgenda = dayAgenda;
        return this;
    }

    public DateAgendaBuilder setMonthAgenda(BigDecimal monthAgenda) {
        this.monthAgenda = monthAgenda;
        return this;
    }

    public DateAgendaBuilder setYearAgenda(BigDecimal yearAgenda) {
        this.yearAgenda = yearAgenda;
        return this;
    }

    public DateAgenda build() {
        DateAgenda dateAgenda = new DateAgenda();
        dateAgenda.setDayAgenda(dayAgenda);
        dateAgenda.setMonthAgenda(monthAgenda);
        dateAgenda.setYearAgenda(yearAgenda);

        return dateAgenda;
    }
}