package com.goldsoft.radaOpenDataApi.model.enums;

public enum Convene {
    FIRST(1),
    SECOND(2),
    THIRD(3),
    FOURTH(4),
    FIFTH(5),
    SIXTH(6),
    SEVENTH(7),
    EIGHTH(8),
    NINTH(9);

    private final int conveneNumber;

    Convene(int i) {
        this.conveneNumber = i;
    }
    public int getConveneNumber(){
        return this.conveneNumber;
    }
}
