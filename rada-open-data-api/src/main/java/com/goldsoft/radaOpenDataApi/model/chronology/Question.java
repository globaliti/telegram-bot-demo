//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.09.16 at 06:58:56 PM EEST 
//


package com.goldsoft.radaOpenDataApi.model.chronology;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}date_question"/>
 *         &lt;element ref="{}event_question" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id_question" use="required" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "dateQuestion",
    "eventQuestion"
})
@XmlRootElement(name = "question")
public class Question {

    @XmlElement(name = "date_question", required = true)
    protected DateQuestion dateQuestion;
    @XmlElement(name = "event_question", required = true)
    protected List<EventQuestion> eventQuestion;
    @XmlAttribute(name = "id_question", required = true)
    protected BigDecimal idQuestion;

    /**
     * Gets the value of the dateQuestion property.
     * 
     * @return
     *     possible object is
     *     {@link DateQuestion }
     *     
     */
    public DateQuestion getDateQuestion() {
        return dateQuestion;
    }

    /**
     * Sets the value of the dateQuestion property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateQuestion }
     *     
     */
    public void setDateQuestion(DateQuestion value) {
        this.dateQuestion = value;
    }

    /**
     * Gets the value of the eventQuestion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the eventQuestion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEventQuestion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EventQuestion }
     * 
     * 
     */
    public List<EventQuestion> getEventQuestion() {
        if (eventQuestion == null) {
            eventQuestion = new ArrayList<EventQuestion>();
        }
        return this.eventQuestion;
    }

    /**
     * Gets the value of the idQuestion property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getIdQuestion() {
        return idQuestion;
    }

    /**
     * Sets the value of the idQuestion property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setIdQuestion(BigDecimal value) {
        this.idQuestion = value;
    }

    public void setEventQuestion(List<EventQuestion> eventQuestion) {
        this.eventQuestion = eventQuestion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Question question = (Question) o;
        return Objects.equals(dateQuestion, question.dateQuestion) &&
                Objects.equals(eventQuestion, question.eventQuestion) &&
                Objects.equals(idQuestion, question.idQuestion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dateQuestion, eventQuestion, idQuestion);
    }

    @Override
    public String toString() {
        return "Question{" +
                "dateQuestion=" + dateQuestion +
                ", eventQuestion=" + eventQuestion +
                ", idQuestion=" + idQuestion +
                '}';
    }
}
