package com.goldsoft.radaOpenDataApi.model.chronology.builder;

import com.goldsoft.radaOpenDataApi.model.chronology.DateEvent;

import java.math.BigDecimal;

public class DateEventBuilder {

    private BigDecimal dayEvent;

    private BigDecimal monthEvent;

    private BigDecimal yearEvent;

    private BigDecimal hourEvent;

    private BigDecimal minEvent;

    private BigDecimal secEvent;

    public static DateEventBuilder dateEvent() {
        return new DateEventBuilder()
                .setYearEvent(new BigDecimal(2000))
                .setMonthEvent(new BigDecimal(1))
                .setDayEvent(new BigDecimal(1))
                .setHourEvent(new BigDecimal(1))
                .setMinEvent(new BigDecimal(1))
                .setSecEvent(new BigDecimal(1));
    }

    public static DateEventBuilder dateEvent(
            BigDecimal dayEvent,
            BigDecimal monthEvent,
            BigDecimal yearEvent,
            BigDecimal hourEvent,
            BigDecimal minEvent,
            BigDecimal secEvent) {

        return new DateEventBuilder()
                .setDayEvent(dayEvent)
                .setMonthEvent(monthEvent)
                .setYearEvent(yearEvent)
                .setHourEvent(hourEvent)
                .setMinEvent(minEvent)
                .setSecEvent(secEvent);
    }

    public DateEventBuilder setDayEvent(BigDecimal dayEvent) {
        this.dayEvent = dayEvent;
        return this;
    }

    public DateEventBuilder setMonthEvent(BigDecimal monthEvent) {
        this.monthEvent = monthEvent;
        return this;
    }

    public DateEventBuilder setYearEvent(BigDecimal yearEvent) {
        this.yearEvent = yearEvent;
        return this;
    }

    public DateEventBuilder setHourEvent(BigDecimal hourEvent) {
        this.hourEvent = hourEvent;
        return this;
    }

    public DateEventBuilder setMinEvent(BigDecimal minEvent) {
        this.minEvent = minEvent;
        return this;
    }

    public DateEventBuilder setSecEvent(BigDecimal secEvent) {
        this.secEvent = secEvent;
        return this;
    }

    public DateEvent build(){
        DateEvent dateEvent = new DateEvent();
        dateEvent.setYearEvent(yearEvent);
        dateEvent.setMonthEvent(monthEvent);
        dateEvent.setDayEvent(dayEvent);
        dateEvent.setHourEvent(hourEvent);
        dateEvent.setMinEvent(minEvent);
        dateEvent.setSecEvent(secEvent);
        return dateEvent;
    }
}
