package com.goldsoft.radaOpenDataApi;

import com.goldsoft.radaOpenDataApi.configuration.RadaApiProfilesConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.client.RestTemplate;

@Configuration
@ComponentScan(basePackages = {"com.goldsoft.radaOpenDataApi.service","com.goldsoft.radaOpenDataApi.util"})
@EnableAutoConfiguration
@Import(RadaApiProfilesConfiguration.class)
public class TestServiceConfiguration {

    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
}
