package com.goldsoft.radaOpenDataApi.service;

import com.goldsoft.radaOpenDataApi.TestServiceConfiguration;
import com.goldsoft.radaOpenDataApi.model.chronology.Chronology;
import com.goldsoft.radaOpenDataApi.model.chronology.ChronologyInformation;
import com.goldsoft.radaOpenDataApi.model.chronology.EventQuestion;
import com.goldsoft.radaOpenDataApi.model.chronology.Question;
import com.goldsoft.radaOpenDataApi.model.chronology.builder.*;
import com.goldsoft.radaOpenDataApi.model.enums.Convene;
import com.goldsoft.radaOpenDataApi.util.TimeUtil;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest()
@ContextConfiguration(classes = TestServiceConfiguration.class)
@ActiveProfiles("default")
public class ChronologyServiceTest {
    @Autowired
    ChronologyService chronologyService;

    @Test
    public void getChronologyWithDateAndStringDateTest() {
        String testDateString = "03.09.2019";
        LocalDate testDate = LocalDate.of(2019, 9, 13);
        Chronology chronology = null;

        chronology = chronologyService.getChronology(Convene.NINTH, testDate);
        Assert.notNull(chronology);
        Assert.notEmpty(chronology.getQuestion());
        Assert.notEmpty(chronology.getQuestion().get(0).getEventQuestion());

        chronology = chronologyService.getChronology(Convene.NINTH, testDateString);
        Assert.notNull(chronology);
        Assert.notEmpty(chronology.getQuestion());
        Assert.notEmpty(chronology.getQuestion().get(0).getEventQuestion());
    }

    @Test
    public void getChronologyDatesTest() {
        List<Date> dates = chronologyService.getChronologyDates(Convene.NINTH);

        Assert.notEmpty(dates);
        Date convertedDate = TimeUtil.getChronologyDate("29.08.2019");
        // org.junit.Assert.assertEquals("29.08.2019",dates.get(0));
        org.junit.Assert.assertEquals(convertedDate, dates.get(0));
    }

    @Test
    public void getChronologyInformationTest() {
        ChronologyInformation chronologySectionInformation = chronologyService.getSectionInformation(
                Convene.NINTH);
        Assert.notNull(chronologySectionInformation.getId());
        assertThat(chronologySectionInformation.getCreationDate(), IsInstanceOf.instanceOf(Date.class));
        assertThat(chronologySectionInformation.getPublicationDate(), IsInstanceOf.instanceOf(Date.class));
    }

    @Test
    public void findQuestionsWithNewVotesAfterDate() {
        ChronologyBuilder chronologyBuilder = ChronologyBuilder.chronology()
                .addQuestion(QuestionBuilder.question()
                        .setDateQuestion(DateQuestionBuilder.dateQuestion()
                                .setYearQuestion(new BigDecimal(2019))
                                .setMonthQuestion(new BigDecimal(1))
                                .setDayQuestion(new BigDecimal(25))
                                .setHourQuestion(new BigDecimal(12))
                                .setMinQuestion(new BigDecimal(20))
                                .setSecQuestion(new BigDecimal(30))
                                .build())
                        .setIdQuestion(new BigDecimal(15))
                        .addEventQuestion(EventQuestionBuilder.eventQuestion()
                                .setDateEvent(DateEventBuilder.dateEvent()
                                        .setYearEvent(new BigDecimal(2019))
                                        .setMonthEvent(new BigDecimal(1))
                                        .setDayEvent(new BigDecimal(25))
                                        .setHourEvent(new BigDecimal(12))
                                        .setMinEvent(new BigDecimal(21))
                                        .setSecEvent(new BigDecimal(30))
                                        .build())
                                .setTypeEvent(new BigDecimal(0))
                                .setNameEvent("Test event 1")
                                .setResultEvent(
                                        ResultEventBuilder.resultEvent()
                                                .setFor(new BigDecimal(20))
                                                .setAbsent(new BigDecimal(20))
                                                .setIdEvent(new BigDecimal(208))
                                                .setTotal(new BigDecimal(200))
                                                .setNotVoting(new BigDecimal(20))
                                                .setPresence(new BigDecimal(20))
                                                .setAgainst(new BigDecimal(20))
                                                .build()
                                )
                                .build())
                        .build())
                .addQuestion(QuestionBuilder.question()
                        .setDateQuestion(DateQuestionBuilder.dateQuestion()
                                .setYearQuestion(new BigDecimal(2019))
                                .setMonthQuestion(new BigDecimal(1))
                                .setDayQuestion(new BigDecimal(25))
                                .setHourQuestion(new BigDecimal(13))
                                .setMinQuestion(new BigDecimal(20))
                                .setSecQuestion(new BigDecimal(30))
                                .build())
                        .setIdQuestion(new BigDecimal(16))
                        .addEventQuestion(EventQuestionBuilder.eventQuestion()
                                .setDateEvent(DateEventBuilder.dateEvent()
                                        .setYearEvent(new BigDecimal(2019))
                                        .setMonthEvent(new BigDecimal(1))
                                        .setDayEvent(new BigDecimal(25))
                                        .setHourEvent(new BigDecimal(13))
                                        .setMinEvent(new BigDecimal(21))
                                        .setSecEvent(new BigDecimal(30))
                                        .build())
                                .setTypeEvent(new BigDecimal(0))
                                .setNameEvent("Test event 2")
                                .setResultEvent(
                                        ResultEventBuilder.resultEvent()
                                                .setFor(new BigDecimal(20))
                                                .setAbsent(new BigDecimal(20))
                                                .setIdEvent(new BigDecimal(208))
                                                .setTotal(new BigDecimal(200))
                                                .setNotVoting(new BigDecimal(20))
                                                .setPresence(new BigDecimal(20))
                                                .setAgainst(new BigDecimal(20))
                                                .build()
                                )
                                .build())
                        .addEventQuestion(EventQuestionBuilder.eventQuestion()
                                .setDateEvent(DateEventBuilder.dateEvent()
                                        .setYearEvent(new BigDecimal(2019))
                                        .setMonthEvent(new BigDecimal(1))
                                        .setDayEvent(new BigDecimal(25))
                                        .setHourEvent(new BigDecimal(13))
                                        .setMinEvent(new BigDecimal(25))
                                        .setSecEvent(new BigDecimal(30))
                                        .build())
                                .setTypeEvent(new BigDecimal(0))
                                .setNameEvent("Test event 3")
                                .setResultEvent(
                                        ResultEventBuilder.resultEvent()
                                                .setFor(new BigDecimal(20))
                                                .setAbsent(new BigDecimal(20))
                                                .setIdEvent(new BigDecimal(208))
                                                .setTotal(new BigDecimal(200))
                                                .setNotVoting(new BigDecimal(20))
                                                .setPresence(new BigDecimal(20))
                                                .setAgainst(new BigDecimal(20))
                                                .build()
                                )
                                .build())
                        .addEventQuestion(EventQuestionBuilder.eventQuestion()
                                .setDateEvent(DateEventBuilder.dateEvent()
                                        .setYearEvent(new BigDecimal(2019))
                                        .setMonthEvent(new BigDecimal(1))
                                        .setDayEvent(new BigDecimal(25))
                                        .setHourEvent(new BigDecimal(13))
                                        .setMinEvent(new BigDecimal(30))
                                        .setSecEvent(new BigDecimal(30))
                                        .build())
                                .setTypeEvent(new BigDecimal(0))
                                .setNameEvent("Test event 4")
                                .setResultEvent(
                                        ResultEventBuilder.resultEvent()
                                                .setFor(new BigDecimal(20))
                                                .setAbsent(new BigDecimal(20))
                                                .setIdEvent(new BigDecimal(208))
                                                .setTotal(new BigDecimal(200))
                                                .setNotVoting(new BigDecimal(20))
                                                .setPresence(new BigDecimal(20))
                                                .setAgainst(new BigDecimal(20))
                                                .build()
                                )
                                .build())
                        .build());

        LocalDateTime searchDate = LocalDateTime.of(2019, 1, 25, 13, 0, 0);
        List<Question> questions = chronologyService.findQuestionsWithNewVotesAfterDate(chronologyBuilder.build(), searchDate);
        Assert.isTrue(questions.size() == 1, "Must be only one question");
        Assert.isTrue(questions.get(0).getEventQuestion().size() == 3, "Must be only one question");

        searchDate = LocalDateTime.of(2019, 1, 25, 13, 24, 0);
        questions = chronologyService.findQuestionsWithNewVotesAfterDate(chronologyBuilder.build(), searchDate);

        Assert.isTrue(questions.size() == 1, "Must be only one question");
        Assert.isTrue(questions.get(0).getEventQuestion().size() == 2, "Must be only one question");

        searchDate = LocalDateTime.of(2019, 1, 25, 14, 0, 0);
        questions = chronologyService.findQuestionsWithNewVotesAfterDate(chronologyBuilder.build(), searchDate);

        Assert.isTrue(questions.isEmpty(), "Must be empty");

        chronologyBuilder.addQuestion(QuestionBuilder.question()
                .setDateQuestion(DateQuestionBuilder.dateQuestion()
                        .setYearQuestion(new BigDecimal(2019))
                        .setMonthQuestion(new BigDecimal(1))
                        .setDayQuestion(new BigDecimal(25))
                        .setHourQuestion(new BigDecimal(15))
                        .setMinQuestion(new BigDecimal(0))
                        .setSecQuestion(new BigDecimal(0))
                        .build())
                .setIdQuestion(new BigDecimal(16))
                .addEventQuestion(EventQuestionBuilder.eventQuestion()
                        .setDateEvent(DateEventBuilder.dateEvent()
                                .setYearEvent(new BigDecimal(2019))
                                .setMonthEvent(new BigDecimal(1))
                                .setDayEvent(new BigDecimal(25))
                                .setHourEvent(new BigDecimal(15))
                                .setMinEvent(new BigDecimal(1))
                                .setSecEvent(new BigDecimal(0))
                                .build())
                        .setTypeEvent(new BigDecimal(8))
                        .setNameEvent("Test event with speech")
                        .build())
                .build());

        searchDate = LocalDateTime.of(2019, 1, 25, 14, 0, 0);
        questions = chronologyService.findQuestionsWithNewVotesAfterDate(chronologyBuilder.build(), searchDate);

        Assert.isTrue(questions.isEmpty(), "Must be empty");
    }
}