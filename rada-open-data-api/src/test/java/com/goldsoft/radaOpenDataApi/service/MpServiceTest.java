package com.goldsoft.radaOpenDataApi.service;

import com.goldsoft.radaOpenDataApi.TestServiceConfiguration;
import com.goldsoft.radaOpenDataApi.model.enums.Convene;
import com.goldsoft.radaOpenDataApi.model.mp.Mps;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = TestServiceConfiguration.class)
public class MpServiceTest {

    @Autowired
    MpService mpService;

    @Test
    public void findAllMps() {
        Mps mps = mpService.findAllMps(Convene.NINTH);
        Assert.notNull(mps);
        Assert.notEmpty(mps.getMp());
        Assert.notNull(mps.getMp().get(0).getName());
    }
}
