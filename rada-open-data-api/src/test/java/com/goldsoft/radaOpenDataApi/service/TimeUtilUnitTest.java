package com.goldsoft.radaOpenDataApi.service;

import com.goldsoft.radaOpenDataApi.model.chronology.DateEvent;
import com.goldsoft.radaOpenDataApi.model.chronology.DateQuestion;
import com.goldsoft.radaOpenDataApi.model.chronology.builder.DateAgendaBuilder;
import com.goldsoft.radaOpenDataApi.model.chronology.builder.DateEventBuilder;
import com.goldsoft.radaOpenDataApi.util.TimeUtil;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class TimeUtilUnitTest {

    @Test
    public void timeConvertingToStringAndStringToDateTest() {
        String testStringDate = "13.09.2019";
        Date testDate = new GregorianCalendar(2019, 8, 13).getTime();

        String stringDate = TimeUtil.getChronologyDateString(testDate);

        Assert.assertEquals(stringDate, testStringDate);

        Date date = TimeUtil.getChronologyDate(testStringDate);

        Assert.assertEquals(date, testDate);
    }

    @Test
    public void getQuestionDateTest() {
        DateQuestion dateQuestion = new DateQuestion();
        dateQuestion.setDayQuestion(new BigDecimal(5));
        dateQuestion.setMonthQuestion(new BigDecimal(12));
        dateQuestion.setYearQuestion(new BigDecimal(2019));
        dateQuestion.setHourQuestion(new BigDecimal(12));
        dateQuestion.setMinQuestion(new BigDecimal(50));
        dateQuestion.setSecQuestion(new BigDecimal(59));

        Calendar calendar = Calendar.getInstance();
        calendar.set(2019, Calendar.DECEMBER, 5, 12, 50, 59);
        Date calendarDate = calendar.getTime();

        DateQuestion convertedDate = TimeUtil.getDateQuestion(calendarDate);
        Assert.assertEquals(dateQuestion, convertedDate);
    }

    @Test
    public void getEventDateTest() {
        DateEvent dateEvent = new DateEvent();
        dateEvent.setDayEvent(new BigDecimal(5));
        dateEvent.setMonthEvent(new BigDecimal(12));
        dateEvent.setYearEvent(new BigDecimal(2019));
        dateEvent.setHourEvent(new BigDecimal(12));
        dateEvent.setMinEvent(new BigDecimal(50));
        dateEvent.setSecEvent(new BigDecimal(59));

        Calendar calendar = Calendar.getInstance();
        calendar.set(2019, Calendar.DECEMBER, 5, 12, 50, 59);
        Date calendarDate = calendar.getTime();

        DateEvent convertedDate = TimeUtil.getDateEvent(calendarDate);
        Assert.assertEquals(dateEvent, convertedDate);
    }

    @Test
    public void getDateByQuestionDateTest() {
        DateQuestion dateQuestion = new DateQuestion();
        dateQuestion.setDayQuestion(new BigDecimal(5));
        dateQuestion.setMonthQuestion(new BigDecimal(12));
        dateQuestion.setYearQuestion(new BigDecimal(2019));
        dateQuestion.setHourQuestion(new BigDecimal(12));
        dateQuestion.setMinQuestion(new BigDecimal(50));
        dateQuestion.setSecQuestion(new BigDecimal(59));

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(2019, Calendar.DECEMBER, 5, 12, 50, 59);
        Date calendarDate = calendar.getTime();

        Date convertedDate = TimeUtil.getDate(dateQuestion);
        Assert.assertEquals(calendarDate.getTime(), convertedDate.getTime());
    }

    @Test
    public void getLocalDateTime() {
        LocalDate chronologyLocalDate = TimeUtil.getChronologyLocalDate(DateAgendaBuilder.dateAgenda()
                .setDayAgenda(new BigDecimal(5))
                .setMonthAgenda(new BigDecimal(1))
                .setYearAgenda(new BigDecimal(2019))
                .build());

        System.out.println(chronologyLocalDate);
    }

    @Test
    public void getDateByEventDateTest() {
        DateEvent dateEvent = new DateEvent();
        dateEvent.setDayEvent(new BigDecimal(5));
        dateEvent.setMonthEvent(new BigDecimal(12));
        dateEvent.setYearEvent(new BigDecimal(2019));
        dateEvent.setHourEvent(new BigDecimal(12));
        dateEvent.setMinEvent(new BigDecimal(50));
        dateEvent.setSecEvent(new BigDecimal(59));

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(2019, Calendar.DECEMBER, 5, 12, 50, 59);
        Date calendarDate = calendar.getTime();

        Date convertedDate = TimeUtil.getDate(dateEvent);
        Assert.assertEquals(calendarDate.getTime(), convertedDate.getTime());
    }

    @Test
    public void testTimeCompare() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2019, Calendar.DECEMBER, 5, 12, 50, 59);
        Date firstDate = calendar.getTime();
        calendar.set(2019, Calendar.DECEMBER, 5, 12, 51, 59);
        Date secondDate = calendar.getTime();

        Assert.assertEquals(-1, TimeUtil.compareDatesWithoutMils(firstDate, secondDate));
        Assert.assertEquals(1, TimeUtil.compareDatesWithoutMils(secondDate, firstDate));
        Assert.assertEquals(0, TimeUtil.compareDatesWithoutMils(firstDate, firstDate));
    }

    @Test
    public void getLocalDateTimeWhen() {
        LocalDateTime concertedDate = TimeUtil.getLocalDateTime(DateEventBuilder
                .dateEvent()
                .setYearEvent(new BigDecimal(2019))
                .setMonthEvent(new BigDecimal(2))
                .setDayEvent(new BigDecimal(10))
                .setHourEvent(new BigDecimal(13))
                .setMinEvent(new BigDecimal(25))
                .setSecEvent(new BigDecimal(15))
                .build());

       Assert.assertTrue(LocalDateTime.of(2019,2,10,13,25,16).isAfter(concertedDate));
       Assert.assertTrue(LocalDateTime.of(2019,2,10,13,25,15).isEqual(concertedDate));
    }
}