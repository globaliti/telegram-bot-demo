package com.goldsoft.radaOpenDataApi.service;

import com.goldsoft.radaOpenDataApi.TestServiceConfiguration;
import com.goldsoft.radaOpenDataApi.model.agenda.Agendas;
import com.goldsoft.radaOpenDataApi.model.enums.Convene;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = TestServiceConfiguration.class)
public class AgendaServiceTest {
    @Autowired
    private AgendaService agendaService;
    @Test
    public void getAgendasTest(){
        Agendas agendas = agendaService.getAgendas(Convene.NINTH);
        Assert.notEmpty(agendas.getAgenda());
    }
}
