package com.goldsoft.radaOpenDataApi.service;

import com.goldsoft.radaOpenDataApi.TestServiceConfiguration;
import com.goldsoft.radaOpenDataApi.model.enums.Convene;
import com.goldsoft.radaOpenDataApi.model.faction.Factions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = TestServiceConfiguration.class)
public class FactionServiceTest {

    @Autowired
    private FactionService factionService;

    @Test
    public void getFactionsTest(){
        Factions factions = factionService.getFactions(Convene.NINTH);
        Assert.notNull(factions);
        Assert.notEmpty(factions.getFaction());
    }
}
