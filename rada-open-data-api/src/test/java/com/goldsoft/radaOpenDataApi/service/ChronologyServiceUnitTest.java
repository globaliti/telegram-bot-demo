package com.goldsoft.radaOpenDataApi.service;

import com.goldsoft.radaOpenDataApi.model.chronology.*;
import com.goldsoft.radaOpenDataApi.model.enums.Convene;
import com.goldsoft.radaOpenDataApi.service.ChronologyService;
import com.goldsoft.radaOpenDataApi.service.ChronologyServiceImpl;
import com.goldsoft.radaOpenDataApi.util.ChronologyUriBinder;
import com.goldsoft.radaOpenDataApi.util.TimeUtil;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.util.Assert;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ChronologyServiceUnitTest {

    private ChronologyService chronologyService;

    @Mock
    RestTemplate restTemplate;

    static ChronologyUriBinder chronologyUriBinder;

    @BeforeClass
    public static void initFinalComponents(){
        chronologyUriBinder = new ChronologyUriBinder();
    }

    @Before
    public void init() {
        chronologyService = new ChronologyServiceImpl(restTemplate, chronologyUriBinder);
    }

    @Test
    public void findQuestionsWithNewEventsAfterDateTest() {//TODO bad tets
        LocalDateTime testSearchDate = LocalDateTime.of(2019,12,5,12,50,53);

        Chronology testChronology = new Chronology();
        List<Question> questionList = new LinkedList<>();

        //create testQuestion without events inside
        Question question = createQuestion(null, LocalDateTime.of(2019,12,5,12,50,59));
        questionList.add(question);
        testChronology.setQuestion(questionList);

        List<Question> questionsWithoutEvents = chronologyService.findQuestionsWithNewEventsAfterDate(
                testChronology,
                testSearchDate);

        Assert.notNull(questionsWithoutEvents, "This question list without events must be not null");
        Assert.isTrue(questionsWithoutEvents.isEmpty(), "This question list without events  must be empty");

        //create event for testQuestion
        EventQuestion eventQuestion = createEventQuestion(LocalDateTime.of(2019,12,5,12,51,59));
        List<EventQuestion> eventQuestionList = new LinkedList<>();
        eventQuestionList.add(eventQuestion);
        question.setEventQuestion(eventQuestionList);

        List<Question> questionsWithEvents = chronologyService.findQuestionsWithNewEventsAfterDate(
                testChronology,
                testSearchDate);
        Assert.notNull(questionsWithEvents, "This question list with new events must be not null");
        Assert.notEmpty(questionsWithEvents, "This question list with new events must be not empty");

        //create testQuestion and event that have old date. This question must not return in findQuestionList
        Question oldQuestion = createQuestion(null, LocalDateTime.of(2019,12,4,12,50,59));

        List<EventQuestion> oldEventQuestions = new LinkedList<>();
        EventQuestion oldEventQuestion = createEventQuestion(LocalDateTime.of(2019,12,4,12,41,59));
        oldEventQuestions.add(oldEventQuestion);
        oldQuestion.setEventQuestion(oldEventQuestions);
        questionList.add(oldQuestion);
        testChronology.setQuestion(questionList);

        List<Question> questionsWhenMustBeOnlyOneQuestion = chronologyService.findQuestionsWithNewEventsAfterDate(
                testChronology,
                testSearchDate);
        Assert.notNull(
                questionsWhenMustBeOnlyOneQuestion,
                "This question list with new events must be not null");
        Assert.notEmpty(
                questionsWhenMustBeOnlyOneQuestion,
                "This question list with new events must be not empty");
        Assert.isTrue(
                questionsWhenMustBeOnlyOneQuestion.size() == 1,
                "Must be only ine question");


        Question secondQuestion = createQuestion(null, LocalDateTime.of(2019,12,6,12,50,59));
        List<EventQuestion> secondQuestionEvents = new LinkedList<>();

        EventQuestion secondEventQuestion1 = createEventQuestion(LocalDateTime.of(2019,12,6,12,51,59));
        EventQuestion secondEventQuestion2 = createEventQuestion(LocalDateTime.of(2019,12,7,12,51,59));
        secondQuestionEvents.add(secondEventQuestion1);
        secondQuestionEvents.add(secondEventQuestion2);
        secondQuestion.setEventQuestion(secondQuestionEvents);

        questionList.add(secondQuestion);
        testChronology.setQuestion(questionList);

        List<Question> questionsWhenMustBeTwoQuestions = chronologyService.findQuestionsWithNewEventsAfterDate(
                testChronology,
                testSearchDate);

        Assert.isTrue(questionsWhenMustBeTwoQuestions.size() == 2, "Must be two questions");

    }

    @Test
    public void findEventsWithVotingTest() {
        Assert.isTrue(chronologyService.findEventsWithVoting(
                new LinkedList<Question>()).size() == 0,
                "Must be zero events");

        List<Question> listWithEmptyQuestion = new LinkedList<>();
        listWithEmptyQuestion.add(new Question());
        Assert.isTrue(chronologyService.findEventsWithVoting(
                new LinkedList<Question>()).size() == 0,
                "Must be zero events");

        List<Question> testQuestionList = new LinkedList<>();

        Question question1 = new Question();
        EventQuestion question1Event = new EventQuestion();
        question1Event.setTypeEvent(new BigDecimal(0));
        List<EventQuestion> question1Events = new LinkedList<>();
        question1Events.add(question1Event);
        question1.setEventQuestion(question1Events);

        testQuestionList.add(question1);

        Assert.isTrue(chronologyService.findEventsWithVoting(
                testQuestionList).size() == 1,
                "Must be only one events");

        Question question2 = new Question();
        EventQuestion question2Event = new EventQuestion();
        question2Event.setTypeEvent(new BigDecimal(8));
        List<EventQuestion> question2Events = new LinkedList<>();
        question2Events.add(question2Event);
        question2.setEventQuestion(question2Events);

        testQuestionList.add(question2);

        Assert.isTrue(chronologyService.findEventsWithVoting(
                testQuestionList).size() == 1,
                "Must be only one events");

        Question question3 = new Question();
        EventQuestion question3Event1 = new EventQuestion();
        question3Event1.setTypeEvent(new BigDecimal(8));
        EventQuestion question3Event2 = new EventQuestion();
        question3Event2.setTypeEvent(new BigDecimal(0));
        List<EventQuestion> question3Events = new LinkedList<>();
        question3Events.add(question3Event1);
        question3Events.add(question3Event2);
        question3.setEventQuestion(question3Events);

        testQuestionList.add(question3);

        Assert.isTrue(
                chronologyService.findEventsWithVoting(testQuestionList).size() == 2,
                "Must be only 2 events");

    }

    @Test
    public void findEventsWithVotingWhenParamOneQuestionTest() {

        Question question1 = new Question();
        EventQuestion question1Event = new EventQuestion();
        question1Event.setTypeEvent(new BigDecimal(0));
        List<EventQuestion> question1Events = new LinkedList<>();
        question1Events.add(question1Event);
        question1.setEventQuestion(question1Events);

        Assert.isTrue(chronologyService.findEventsWithVoting(
                question1).size() == 1,
                "Must be only one events");

        Question question2 = new Question();
        EventQuestion question2Event = new EventQuestion();
        question2Event.setTypeEvent(new BigDecimal(8));
        List<EventQuestion> question2Events = new LinkedList<>();
        question2Events.add(question2Event);
        question2.setEventQuestion(question2Events);

        Assert.isTrue(chronologyService.findEventsWithVoting(
                question2).isEmpty(),
                "Must have nothing in list");

        Question question3 = new Question();
        EventQuestion question3Event1 = new EventQuestion();
        question3Event1.setTypeEvent(new BigDecimal(8));
        EventQuestion question3Event2 = new EventQuestion();
        question3Event2.setTypeEvent(new BigDecimal(0));
        EventQuestion question3Event3 = new EventQuestion();
        question3Event3.setTypeEvent(new BigDecimal(0));
        List<EventQuestion> question3Events = new LinkedList<>();
        question3Events.add(question3Event1);
        question3Events.add(question3Event2);
        question3Events.add(question3Event3);
        question3.setEventQuestion(question3Events);

        Assert.isTrue(
                chronologyService.findEventsWithVoting(question3).size() == 2,
                "Must be only 2 events");

    }

    @Test
    public void getQuestionsWithVotingEventsTest() {
        Assert.isTrue(chronologyService.getQuestionsWithVotingEvents(
                new LinkedList<Question>()).size() == 0,
                "Must be zero events");

        List<Question> listWithEmptyQuestion = new LinkedList<>();
        listWithEmptyQuestion.add(new Question());
        Assert.isTrue(chronologyService.getQuestionsWithVotingEvents(
                new LinkedList<Question>()).size() == 0,
                "Must be zero events");

        List<Question> testQuestionList = new LinkedList<>();

        Question question1 = new Question();
        EventQuestion question1Event = new EventQuestion();
        question1Event.setTypeEvent(new BigDecimal(0));
        List<EventQuestion> question1Events = new LinkedList<>();
        question1Events.add(question1Event);
        question1.setEventQuestion(question1Events);

        testQuestionList.add(question1);

        Assert.isTrue(chronologyService.getQuestionsWithVotingEvents(
                testQuestionList).size() == 1,
                "Must be only one events");

        Question question2 = new Question();
        EventQuestion question2Event = new EventQuestion();
        question2Event.setTypeEvent(new BigDecimal(8));
        List<EventQuestion> question2Events = new LinkedList<>();
        question2Events.add(question2Event);
        question2.setEventQuestion(question2Events);

        testQuestionList.add(question2);

        Assert.isTrue(chronologyService.getQuestionsWithVotingEvents(
                testQuestionList).size() == 1,
                "Must be only one events");

        Question question3 = new Question();
        EventQuestion question3Event1 = new EventQuestion();
        question3Event1.setTypeEvent(new BigDecimal(8));
        EventQuestion question3Event2 = new EventQuestion();
        question3Event2.setTypeEvent(new BigDecimal(0));
        List<EventQuestion> question3Events = new LinkedList<>();
        question3Events.add(question3Event1);
        question3Events.add(question3Event2);
        question3.setEventQuestion(question3Events);

        testQuestionList.add(question3);

        Assert.isTrue(
                chronologyService.getQuestionsWithVotingEvents(testQuestionList).size() == 2,
                "Must be only 2 events");
    }

    private Question createQuestion(List<EventQuestion> eventQuestions, LocalDateTime date) {
        Question question = new Question();
        DateQuestion dateQuestion = TimeUtil.getDateQuestion(date);
        question.setDateQuestion(dateQuestion);
        question.setEventQuestion(eventQuestions);
        return question;
    }

    private EventQuestion createEventQuestion(LocalDateTime date) {
        EventQuestion eventQuestion = new EventQuestion();
        DateEvent dateEvent = TimeUtil.getDateEvent(date);
        eventQuestion.setDateEvent(dateEvent);
        return eventQuestion;
    }

    @Test
    public void isChronologyHasUpdateTest() {
        Assert.isTrue(
                chronologyService.isChronologyPublicationDateHasUpdate(null, Convene.NINTH),
                "When check data null, must return true");

        Date checkDate = new Date();
        Date testPublicationDate = new Date(checkDate.getTime() + 5000);
        ChronologyInformation chronologyInformation = new ChronologyInformation();
        chronologyInformation.setPublicationDate(testPublicationDate);

        ChronologyService chronologyServiceSpy = Mockito.spy(this.chronologyService);
        when(chronologyServiceSpy.getSectionInformation(Convene.NINTH)).thenReturn(chronologyInformation);

        Assert.isTrue(
                this.chronologyService.isChronologyPublicationDateHasUpdate(checkDate, Convene.NINTH),
                "When publication date after check date, must return true");

        Assert.isTrue(
                !this.chronologyService.isChronologyPublicationDateHasUpdate(new Date(testPublicationDate.getTime() + 5000), Convene.NINTH),
                "When publication date before check date, must return false");
    }
}